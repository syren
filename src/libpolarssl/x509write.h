/**
 * \file x509write.h
 *
 * \brief X509 buffer writing functionality
 *
 *  Copyright (C) 2006-2012, Brainspark B.V.
 *
 *  This file is part of PolarSSL (http://www.polarssl.org)
 *  Lead Maintainer: Paul Bakker <polarssl_maintainer at polarssl.org>
 *
 *  All rights reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
#ifndef POLARSSL_X509_WRITE_H
#define POLARSSL_X509_WRITE_H

#include "config.h"

#include "oid.h"
#include "rsa.h"
#include "asn1.h"
#include "x509.h"
#include "x509_crt.h"


#define OID_X520                "\x55\x04"


/*
#define POLARSSL_ERR_ASN1_OUT_OF_DATA                      -0x0014
#define POLARSSL_ERR_ASN1_UNEXPECTED_TAG                   -0x0016
#define POLARSSL_ERR_ASN1_INVALID_LENGTH                   -0x0018
#define POLARSSL_ERR_ASN1_LENGTH_MISMATCH                  -0x001A
#define POLARSSL_ERR_ASN1_INVALID_DATA                     -0x001C
*/

//Does not appear in 1.2.7 Polar x509.h file
#define POLARSSL_ERR_X509_KEY_INVALID_ENC_IV               -0x2A01
#define POLARSSL_ERR_X509_KEY_UNKNOWN_ENC_ALG              -0x2A03
#define POLARSSL_ERR_X509_KEY_PASSWORD_REQUIRED            -0x2A05
#define POLARSSL_ERR_X509_KEY_PASSWORD_MISMATCH            -0x2A07
#define POLARSSL_ERR_X509_POINT_ERROR                      -0x2A09
#define POLARSSL_ERR_X509_VALUE_TO_LENGTH                  -0x2A11


/*
 * DER constants
 */
/*
#define ASN1_BOOLEAN                 0x01
#define ASN1_INTEGER                 0x02
#define ASN1_BIT_STRING              0x03
#define ASN1_OCTET_STRING            0x04
#define ASN1_NULL                    0x05
#define ASN1_OID                     0x06
#define ASN1_UTF8_STRING             0x0C
#define ASN1_SEQUENCE                0x10
#define ASN1_SET                     0x11
#define ASN1_PRINTABLE_STRING        0x13
#define ASN1_T61_STRING              0x14
#define ASN1_IA5_STRING              0x16
#define ASN1_UTC_TIME                0x17
#define ASN1_UNIVERSAL_STRING        0x1C
#define ASN1_BMP_STRING              0x1E
#define ASN1_PRIMITIVE               0x00
#define ASN1_CONSTRUCTED             0x20
#define ASN1_CONTEXT_SPECIFIC        0x80
*/

#define OID_PKCS1_RSA_SHA       "\x2A\x86\x48\x86\xF7\x0D\x01\x01\x05"


#define X509_OUTPUT_DER              0x01
#define X509_OUTPUT_PEM              0x02
#define PEM_LINE_LENGTH                72

#define X509_ISSUER                  0x01
#define X509_SUBJECT                 0x02

#define X520_COMMON_NAME                3
#define X520_COUNTRY                    6
#define X520_LOCALITY                   7
#define X520_STATE                      8
#define X520_ORGANIZATION              10
#define X520_ORG_UNIT                  11


/**
 * \name Structures for writing X.509 certificates.
 * XvP: commented out as they are not used.
 * - <tt>typedef struct _x509_node x509_node;</tt>
 * - <tt>typedef struct _x509_raw x509_raw;</tt>
 */
typedef struct _x509_node
{
    unsigned char *data;
    unsigned char *p;
    unsigned char *end;

    size_t len;
}
x509_node;

typedef struct _x509_raw
{
    x509_node raw;
    x509_node tbs;

    x509_node version;
    x509_node serial;
    x509_node tbs_signalg;
    x509_node issuer;
    x509_node validity;
    x509_node subject;
    x509_node subpubkey;

    x509_node signalg;
    x509_node sign;
}
x509_raw;


#ifdef __cplusplus
extern "C" {
#endif

/**
 * \brief          Add a public key to certificate
 *
 * \param chain    points to the raw certificate data
 * \param pubkey   points to an RSA key
 *
 * \return         0 if successful, or a specific X509 error code
 */
int x509write_add_pubkey ( x509_raw *chain, rsa_context *pubkey );

/**
 * \brief          Create x509 subject/issuer field to raw certificate
 *                 from string or CA cert. Make string NULL if you will
 *                 use the CA copy function or make CA NULL then used
 *                 the string parse.
 *
 * \param chain    points to the raw certificate data
 * \param names    a string that can hold (separete with ";"):
 *                     CN=CommonName
 *                 --   O=Organization
 *                 --  OU=OrgUnit
 *                 --  ST=State
 *                 --   L=Locality
 *                 --   R=Email
 *                 --   C=Country
 *                 . Make that NULL if you didn't need that.
 * \param flag     flag is X509_ISSUER or X509_SUBJECT that defined
 *                 where change
 * \param ca       the certificate for copy data. Make that NULL if you
 *                 didn't need that.
 * \param ca_flag  set the ca field from copy to crt
 *
 * \return         0 if successful, or a specific X509 error code
 */
/*
int x509write_add_customize ( x509_raw *crt,
                          unsigned char *names,
                          int flag,
                          x509_crt *ca,
                          int ca_flag );
*/

/**
* \brief          Add x509 issuer field
*
* \param chain    points to the raw certificate data
* \param issuer   a string holding (separete with ";"):
*                     CN=CommonName
*                 --   O=Organization
*                 --  OU=OrgUnit
*                 --  ST=State
*                 --   L=Locality
*                 --   R=Email
*                 --   C=Country
*                 . Set this to NULL if not needed.
* \return         0 if successful, or a specific X509 error code
*/
int x509write_add_issuer ( x509_raw *crt, unsigned char *issuer);

/**
 * \brief          Add x509 subject field
 *
 * \param chain    points to the raw certificate data
 * \param subject  a string holding (separete with ";"):
 *                     CN=CommonName
 *                 --   O=Organization
 *                 --  OU=OrgUnit
 *                 --  ST=State
 *                 --   L=Locality
 *                 --   R=Email
 *                 --   C=Country
 *                 . Set this to NULL if not needed.
 * \return         0 if successful, or a specific X509 error code
 */
int x509write_add_subject ( x509_raw *crt, unsigned char *subject);

/**
* \brief          Copy x509 issuer field from another certificate
*
* \param chain    points to the raw certificate data
* \param from_crt the certificate whose issuer is to be copied.
* \return         0 if successful, or a specific X509 error code
*/
int x509write_copy_issuer (x509_raw *crt, x509_crt *from_crt);

/**
* \brief          Copy x509 subject field from another certificate
*
* \param chain    points to the raw certificate data
* \param from_crt the certificate whose subject is to be copied.
* \return         0 if successful, or a specific X509 error code
*/
int x509write_copy_subject (x509_raw *crt, x509_crt *from_crt);

/**
* \brief          Copy x509 issuer field from the subject of another certificate
*
* \param chain    points to the raw certificate data
* \param from_crt the certificate whose subject is to be copied.
* \return         0 if successful, or a specific X509 error code
*/
int x509write_copy_issuer_from_subject (x509_raw *crt, x509_crt *from_crt);

/**
* \brief          Copy x509 subject field from the issuer of another certificate
*
* \param chain    points to the raw certificate data
* \param from_crt the certificate whose issuer is to be copied.
* \return         0 if successful, or a specific X509 error code
*/
int x509write_copy_subject_from_issuer (x509_raw *crt, x509_crt *from_crt);

/**
 * \brief          Create x509 validity time in UTC
 *
 * \param chain    points to the raw certificate data
 * \param before   valid not before in format YYYY-MM-DD hh:mm:ss
 * \param after    valid not after  in format YYYY-MM-DD hh:mm:ss
 *
 * \return         0 if successful, or a specific X509 error code
 */
int x509write_add_validity ( x509_raw *crt,
                               unsigned char *before,
                               unsigned char *after );

/**
 * \brief          Create a self-signed certificate
 *
 * \param chain    points to the raw certificate data
 * \param rsa      a private key to sign the certificate
 *
 * \return         0 if successful, or a specific X509 error code
 */
int x509write_create_selfsign ( x509_raw *crt, rsa_context *raw );

/**
 * \brief          Create a certificate
 *
 * \param chain    points to the raw certificate data
 * \param rsa      a private key to sign the certificate
 *
 * \return         0 if successful, or a specific X509 error code
 */
int x509write_create_sign ( x509_raw *crt, rsa_context *raw );

/**
 * \brief           Serialize an rsa key into DER
 *
 * \param rsa       a rsa key for output
 * \param node      a x509 node for write into
 *
 * \return          0 if successful, or a specific X509 error code
 */
int x509write_serialize_key ( rsa_context *rsa, x509_node *node );

/**
 * \brief          Unallocate all raw certificate data
 */
void x509write_free_raw ( x509_raw *crt );

/**
 * \brief          Allocate all raw certificate data
 */
void x509write_init_raw ( x509_raw *crt );

/**
 * \brief          Unallocate all node certificate data
 */
void x509write_free_node ( x509_node *crt_node );

/**
 * \brief          Allocate all node certificate data
 */
void x509write_init_node ( x509_node *crt_node );


/**
 * \brief          Write a certificate info file
 *
 * \param chain    points to the raw certificate data
 * \param path     filename to write the certificate to
 * \param format   X509_OUTPUT_DER or X509_OUTPUT_PEM
 *
 * \return         0 if successful, or a specific X509 error code
 */
int x509write_crtfile ( x509_raw *chain,
                       char *path,
                       int format );

/**
 * \brief          Write a certificate signing request message format file
 *
 * \param chain    points to the raw certificate (with x509write_create_csr) data
 * \param path     filename to write the certificate to
 * \param format   X509_OUTPUT_DER or X509_OUTPUT_PEM
 *
 * \return         0 if successful, or a specific X509 error code
 */
int x509write_csrfile ( x509_raw *chain,
                       unsigned char *path,
                       int format );

/*
 * \brief          Write a private RSA key into a file
 *
 * \param rsa      points to an RSA key
 * \param path     filename to write the key to
 * \param format   X509_OUTPUT_DER or X509_OUTPUT_PEM
 *
 * \return         0 if successful, or a specific X509 error code
 */
int x509write_keyfile ( rsa_context *rsa,
                       char *path,
                       int format );

/**
 * \brief          Create a certificate signing request
 *
 * \param chain    points to the raw certificate data. Didn't use the
 *                 same chain that u have use for certificate.
 * \param privkey  a rsa private key
 *
 * \return         0 if successful, or a specific X509 error code
 */
int x509write_create_csr ( x509_raw *chain, rsa_context *privkey );

#endif /* POLARSSL_X509_WRITE_H */
