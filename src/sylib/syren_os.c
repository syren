/*
  Syren -- a lightweight downloader for Linux/BSD/MacOSX
  inspired by Axel Copyright 2001-2002 Wilmer van der Gaast
  version 0.0.6 (atomic alien)
  coded by Ketmar // Vampire Avalon

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License with
  the Debian GNU/Linux distribution in file /usr/doc/copyright/GPL;
  if not, write to the Free Software Foundation, Inc., 59 Temple Place,
  Suite 330, Boston, MA  02111-1307  USA
*/
/*
  Syren os-specific functions
 */
#ifndef _SYREN_OS_C
#define _SYREN_OS_C

#include "syren_os.h"



double SyGetTimeD (void) {
  struct timeval time;
  gettimeofday(&time, 0);
  return ((double)time.tv_sec+(double)time.tv_usec/1000000);
}


#ifdef SY_INCLUDE_FILE_IO
TSyResult SyDeleteFile (const char *fname) {
  if (!unlink(fname)) return SY_OK;
  return SY_ERROR;
}


int SyOpenFile (const char *fname, TSyFileMode mode, TSyBool mustCreate) {
  int mm = (mode==SY_FMODE_WRITE)?O_WRONLY:O_RDONLY;

  if (mustCreate != SY_FALSE) mm = mm | O_CREAT;
  return open(fname, mm, SY_FILE_DEFMODE);
}


TSyResult SyWriteFile (int fd, const void *buf, int count) {
  int wr = write(fd, buf, count);
  if (wr != count) return SY_ERROR;
  return SY_OK;
}


TSyResult SyReadFile (int fd, void *buf, int count) {
  int rd = read(fd, buf, count);
  if (rd != count) return SY_ERROR;
  return SY_OK;
}


TSyResult SySeekFile (int fd, int64_t offset) {
  //!!
  offset = offset<0?SY_LSEEK(fd, offset, SEEK_END):SY_LSEEK(fd, offset, SEEK_SET);
  if (offset < 0) return SY_ERROR;
  return SY_OK;
}


int64_t SyFileSize (int fd) {
  int64_t size;
  int64_t ppos = SY_LSEEK(fd, 0, SEEK_CUR);
  if (ppos < 0) return -1;
  size = SY_LSEEK(fd, 0, SEEK_END);
  if (SY_LSEEK(fd, ppos, SEEK_SET) < 0) return -1;
  return size;
}
#endif


TSyResult SySocketInit (void) {
  return SY_OK;
}

void SySocketShutdown (void) {
}


#endif
