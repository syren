/*
  Syren -- a lightweight downloader for Linux/BSD/MacOSX
  inspired by Axel Copyright 2001-2002 Wilmer van der Gaast
  version 0.0.6 (atomic alien)
  coded by Ketmar // Vampire Avalon

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License with
  the Debian GNU/Linux distribution in file /usr/doc/copyright/GPL;
  if not, write to the Free Software Foundation, Inc., 59 Temple Place,
  Suite 330, Boston, MA  02111-1307  USA
*/
/*
  Syren os-specific functions
 */
#ifndef _SYREN_OS_H
#define _SYREN_OS_H

#ifdef __cplusplus
extern "C" {
#endif


#include "syren_common.h"


/* *alloc() */
#include <stdlib.h>

/* close() */
#include <unistd.h>

/* socket support */
#include <sys/types.h>
#include <sys/socket.h>

/* inet_aton(), etc */
#include <netinet/in.h>
#include <arpa/inet.h>

/* gethostbyname() */
#include <netdb.h>


/* SyGetIFIP() support */
/* ioctl() %-) */
#include <sys/ioctl.h>

/* IFNAMSIZ, struct ifreq */
#include <net/if.h>

/* gettimeofday() */
#include <sys/time.h>

/* open(), etc */
/*#include <sys/types.h>*/
#include <sys/stat.h>
#include <fcntl.h>

/* signal() */
/*#include <signal.h>*/

/* not used, but fine to have %-) */
#include <limits.h>
#include <errno.h>


/*
seems to be unused
#include <netinet/in_systm.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
*/


double SyGetTimeD (void);
#define SySleep(n) sleep(n)


#ifdef SY_INCLUDE_FILE_IO
#define SY_FILE_DEFMODE 0644
typedef enum {
  SY_FMODE_READ,
  SY_FMODE_WRITE
} TSyFileMode;


/*
#ifdef __linux
#define SY_LSEEK lseek64
#else
#define SY_LSEEK lseek
#endif
*/
#define SY_LSEEK lseek


#define SyCloseFile(fd) close(fd)

TSyResult SyDeleteFile (const char *fname);
int SyOpenFile (const char *fname, TSyFileMode mode, TSyBool mustCreate);
TSyResult SyWriteFile (int fd, const void *buf, int count);
TSyResult SyReadFile (int fd, void *buf, int count);
TSyResult SySeekFile (int fd, int64_t offset);
int64_t SyFileSize (int fd);
#endif


#define SySocketClose close

TSyResult SySocketInit (void);
void SySocketShutdown (void);


#ifdef __cplusplus
}
#endif

#endif
