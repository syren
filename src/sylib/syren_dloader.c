/*
  Syren -- a lightweight downloader for Linux/BSD/MacOSX
  inspired by Axel Copyright 2001-2002 Wilmer van der Gaast
  version 0.0.6 (atomic alien)
  coded by Ketmar // Vampire Avalon

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License with
  the Debian GNU/Linux distribution in file /usr/doc/copyright/GPL;
  if not, write to the Free Software Foundation, Inc., 59 Temple Place,
  Suite 330, Boston, MA  02111-1307  USA
*/
/*
  Syren download driver
 */
#ifndef _SYREN_DLOADER_C
#define _SYREN_DLOADER_C


#include "syren_dloader.h"


/* old algo seems to be faster on fast networks */
#define SYDL_USE_OLD_ALGO


static void SyClearURL (TSyURL **url) {
  if (!url || !(*url)) return;
  SyURLClear(*url); free(*url); *url = NULL;
}


static void SyClearStr (char **s) {
  if (!s || !(*s)) return;
  SyStrFree(*s); *s = NULL;
}


static void SyClearInternal (TSyState *state, int keepSizesAndPData) {
  if (!state) return;
  SyTCPCloseSocket(&state->datafd); SyTCPCloseSocket(&state->fd);
  SyClearURL(&state->url); SyClearURL(&state->httpproxy); SyClearURL(&state->ftpproxy);
  SyClearStr(&state->iface); SyClearStr(&state->httpFName);
  SyClearStr(&state->userAgent);
  state->status = SY_STATUS_UNPREPARED;
  state->breakNow = state->interrupted = SY_FALSE;
  if (!keepSizesAndPData) {
    SyClearStr(&state->postData);
    state->fileSize = state->lastByte = -1;
    state->firstByte = state->currentByte = 0;
  }
}



void SyInitStats (TSyState *state, TSyStats *si) {
  si->i[0].active = 0;
  si->i[1].active = 0;
  si->interval = 30; /* seconds */
  si->bps = 0.0;
  si->eta = 0.0;
}


void SyUpdateStats (TSyState *state, TSyStats *si, int64_t bytesDone, int64_t bytesTotal) {
  TSyStatInfo *i0, *i1;
  double ctime, ptime, t;
  double w0, w1; /* weights */
  double bps0, bps1;

  i0 = &(si->i[0]);
  i1 = &(si->i[1]);

  ctime = SyGetTimeD();

  if (!i1->active) {
    /* nothing started */
    i0->active = 0;
    i0->bytesFrom = 0;
    i0->bytesDone = 0;
    i0->time = 0.0;
    i1->active = 1;
    i1->bytesFrom = 0;
    i1->bytesDone = bytesDone;
    i1->time = ctime;
    si->sttime = ctime;
    si->bps = 0.0;
    si->eta = 0.0;
    return;
  }

  /* update "bytes done" */
  i1->bytesDone = bytesDone-(i1->bytesFrom);

  ptime = ctime-(i1->time);
  if (ptime >= si->interval) {
    /* interval passed, move state */
    *i0 = *i1;
    i1->bytesFrom = bytesDone;
    i1->bytesDone = 0;
    i1->time = ctime;
    i0->time = ptime; /* i0->time: time, taken by this stat */
  }

  /* calculate weights */
  t = ctime-(i1->time);
  w1 = i0->active?t/si->interval:1.0;
  if (w1 > 1.0) w1 = 1.0;
  if (t < 0.5) w1 = 0.0;
  w0 = 1.0-w1;

  /* calculate bps */
  bps0 = i0->active?(double)(i0->bytesDone)/i0->time:0.0;
  bps1 = t>=0.5?(double)(i1->bytesDone)/t:0.0;
  si->bps = (bps0*w0)+(bps1*w1);

  /* calculate eta */
  if (bytesTotal > 0 && si->bps >= 1) {
    si->eta = (double)(bytesTotal-bytesDone)/(si->bps);
  } else si->eta = 0.0;
}



TSyState *SyNew (void) {
  TSyState *state = calloc(1, sizeof(TSyState));
  if (state) {
    if (SyTCPInitSocket(&state->fd) != SY_OK || SyTCPInitSocket(&state->datafd) != SY_OK) {
      free(state);
      return NULL;
    }
    state->maxBufferSize = 1024*1024; /* 1mb */
    state->ioTimeout = 60;
    state->maxRedirects = 6;
    state->allowOnly2XX = SY_TRUE;
    SyClearInternal(state, 0);
  }
  return state;
}


void SyFree (TSyState *state) {
  if (!state) return;
  SyClear(state); free(state);
}


void SyClear (TSyState *state) {
  if (!state) return;
  SyClearInternal(state, 0);
}



TSyResult SyPrepare (TSyState *state, const char *urlstr, const char *httpproxyurl, const char *ftpproxyurl,
    const char *iface) {
  if (!state) return SY_ERROR;
  if (state->status != SY_STATUS_UNPREPARED) return SY_ERROR;
  SyInitStats(state, &state->stats);
  SyClearInternal(state, 1); /* keep sizes and positions */
  state->url = SyURLNew(); if (!state->url) return SY_ERROR;
  if (iface) {
    state->iface = SyStrDup(iface);
    if (!state->iface) return SY_ERROR;
    SyStrTrim(state->iface);
  }
  if (SyURLParse(state->url, urlstr) != SY_OK) {
    SyMessage(state->pfn, SY_MSG_ERROR, "invalid URL");
    return SY_ERROR;
  }
  if (state->url->proto == SY_PROTO_UNKNOWN) {
    SyClearInternal(state, 1);
    SyMessage(state->pfn, SY_MSG_ERROR, "unknown protocol");
    return SY_ERROR;
  }
#ifndef SYOPT_ALLOW_HTTPS
  if (state->url->proto == SY_PROTO_HTTPS) {
    SyClearInternal(state, 1);
    SyMessage(state->pfn, SY_MSG_ERROR, "HTTPS not supported");
    return SY_ERROR;
  }
#endif
  if (httpproxyurl && *httpproxyurl) {
    state->httpproxy = SyURLNew(); if (!state->httpproxy) return SY_ERROR;
    if (SyURLParse(state->httpproxy, httpproxyurl) != SY_OK) {
      SyClearInternal(state, 1);
      SyMessage(state->pfn, SY_MSG_ERROR, "invalid HTTP proxy URL");
      return SY_ERROR;
    }
  }
  if (ftpproxyurl && *ftpproxyurl) {
    state->ftpproxy = SyURLNew(); if (!state->ftpproxy) return SY_ERROR;
    if (SyURLParse(state->ftpproxy, ftpproxyurl) != SY_OK) {
      SyClearInternal(state, 1);
      SyMessage(state->pfn, SY_MSG_ERROR, "invalid FTP proxy URL");
      return SY_ERROR;
    }
  }
  state->status = SY_STATUS_PREPARED;
  return SY_OK;
}


TSyResult SyEnd (TSyState *state) {
  SyClear(state);
  return SY_OK;
}


void SyReset (TSyState *state) {
  if (!state) return;
  if (state->status == SY_STATUS_UNPREPARED) return;
  SyTCPCloseSocket(&state->datafd); SyTCPCloseSocket(&state->fd);
  state->status = SY_STATUS_PREPARED;
  state->breakNow = state->interrupted = SY_FALSE;
}



#define SY_CHECK_BREAK0 if (state->breakNow != SY_FALSE) { state->interrupted = SY_TRUE; break; }


TSyResult SyBegin (TSyState *state) {
  TSyResult res = SY_ERROR;
  TSyProxy *proxy = NULL;
  TSyHdrs *hdrs; TSyURL *url, *purl, *lurl;
  char *s, *t, *buf, tmp[32];
  int rdcnt, code;

  if (!state || (state->status != SY_STATUS_PREPARED && state->status != SY_STATUS_DOWNLOADING)) return SY_ERROR;
  rdcnt = state->maxRedirects;
  state->breakNow = state->interrupted = state->error = SY_FALSE; state->currentByte = 0;
  state->chunked = SY_FALSE;
  if (state->firstByte < 0) state->firstByte = 0;
  if (state->lastByte >= 0 && state->firstByte > state->lastByte) return SY_ERROR;
  hdrs = SyHdrNew(); if (!hdrs) return SY_ERROR;
  while (1) {
    SyProxyFree(proxy); proxy = NULL;
    SyClearStr(&state->httpFName);
    SyTCPCloseSocket(&state->datafd); SyTCPCloseSocket(&state->fd);
    state->status = SY_STATUS_CONNECTING;
    SY_CHECK_BREAK0
    url = state->url;
    SyMessage(state->pfn, SY_MSG_MSG, "downloading: %s://%s:%i%s%s%s%s",
      url->protostr, url->host, url->port, url->dir, url->file, url->query, url->anchor);
    /* check for proxy */
    if (url->proto == SY_PROTO_FTP && state->ftpproxy) purl = state->ftpproxy;
#ifdef SYOPT_ALLOW_HTTPS
    else if (url->proto == SY_PROTO_HTTP && state->httpproxy) purl = state->httpproxy;
    else if (url->proto == SY_PROTO_HTTPS && state->httpproxy) purl = state->httpproxy;
#else
    else if (url->proto == SY_PROTO_HTTP && state->httpproxy) purl = state->httpproxy;
#endif
    else purl = url;
    state->fd.errCode = 0;
    if (url->proto == SY_PROTO_FTP && (!state->ftpproxy || state->ftpUseConnect)) {
      /* FTP */
      /* connecting */
      if (SyTCPConnect(&state->fd, purl->host, purl->port, state->iface, state->ioTimeout, state->pfn) != SY_OK) break;
      SY_CHECK_BREAK0
      /* handshaking */
      state->status = SY_STATUS_HANDSHAKING;
      if (state->ftpUseConnect && state->ftpproxy) {
        proxy = SyProxyNew(state->ftpproxy, SY_PROXY_HTTP_CONNECT, state->userAgent);
        if (!proxy) break;
        if (SyProxyConnect(&state->fd, proxy, url->host, url->port, state->pfn) != SY_OK) break;
        SY_CHECK_BREAK0
      }
      if (SyFTPStart(&state->fd, url->user, url->pass, state->pfn) != SY_OK) break;
      SY_CHECK_BREAK0
      if (SyFTPCwd(&state->fd, url->dir, state->pfn) != SY_OK) break;
      SY_CHECK_BREAK0
      state->fileSize = SyFTPGetSize(&state->fd, state->iface, url->file, rdcnt, state->ioTimeout, state->pfn, proxy);
      if (state->firstByte > state->fileSize) break;
      SY_CHECK_BREAK0
      if (state->firstByte > 0) {
        /* do REST */
        if (SyLong2Str(tmp, state->firstByte) != SY_OK) break;
        if (SyTCPSendLine(state->pfn, 1, &state->fd, "REST %s", tmp) != SY_OK) break;
        SY_CHECK_BREAK0
        /*if (SyFTPWaitFor2(&state->fd, 3, 2, state->pfn) <= 0) break;*/
        if ((s = SyFTPWait(&code, &state->fd, state->pfn)) == NULL) break;
        if (code <= 0) break;
        code /= 100;
        if (code != 3 && code != 2) {
          SyMessage(state->pfn, SY_MSG_WARNING, "can't resume");
          if (state->fnnoresume && state->fnnoresume(state) != SY_OK) break;
          state->firstByte = 0;
        }
        SY_CHECK_BREAK0
      }
      if (SyFTPOpenDataPassv(&state->fd, &state->datafd, state->iface, state->ioTimeout, state->pfn, proxy) != SY_OK) break;
      SY_CHECK_BREAK0
      if (SyTCPSendLine(state->pfn, 1, &state->fd, "RETR %s", url->file) != SY_OK) break;
      SY_CHECK_BREAK0
      if (SyFTPWaitFor2(&state->fd, 1, -1, state->pfn) <= 0) break;
      res = SY_OK; break;
    } else {
      /* HTTP/HTTPS */
      int nodir;
      int postLen = strlen(state->postData?state->postData:"");
#ifdef SYOPT_ALLOW_HTTPS
      if (SyHTTPBuildQuery(hdrs, postLen?"POST":"GET", url,
           (url->proto==SY_PROTO_FTP)?state->ftpproxy:
           (url->proto==SY_PROTO_HTTP)?state->httpproxy:NULL) != SY_OK) {
        SyMessage(state->pfn, SY_MSG_ERROR, "can't build request headers");
        break;
      }
#else
      if (SyHTTPBuildQuery(hdrs, postLen?"POST":"GET", url,
           (url->proto==SY_PROTO_FTP)?state->ftpproxy:state->httpproxy) != SY_OK) {
        SyMessage(state->pfn, SY_MSG_ERROR, "can't build request headers");
        break;
      }
#endif
      if (state->userAgent && state->userAgent[0]) {
        s = SySPrintf("User-Agent: %s", state->userAgent);
        if (!s) {
          SyMessage(state->pfn, SY_MSG_ERROR, "can't build request headers (out of memory)");
          break;
        }
        if (SyHdrAddLine(hdrs, s) != SY_OK) {
          SyStrFree(s);
          SyMessage(state->pfn, SY_MSG_ERROR, "can't build request headers");
          break;
        }
        SyStrFree(s);
      }
      if (postLen) {
        if (!SyHdrHasField(hdrs, "Content-Type")) {
          if (SyHdrAddLine(hdrs, "Content-Type: application/x-www-form-urlencoded") != SY_OK) {
            SyMessage(state->pfn, SY_MSG_ERROR, "can't build request headers");
            break;
          }
        }
        s = SySPrintf("Content-Length: %i", postLen); /* FIXME: change when we'll be able to post files */
        if (!s) { SyMessage(state->pfn, SY_MSG_ERROR, "memory error"); break; }
        if (SyHdrAddLine(hdrs, s) != SY_OK) {
          SyStrFree(s);
          SyMessage(state->pfn, SY_MSG_ERROR, "can't build request headers");
          break;
        }
        SyStrFree(s);
      }
      if (state->firstByte > 0) {
        if (SyHTTPAddRange(hdrs, state->firstByte, -1) != SY_OK) {
          SyMessage(state->pfn, SY_MSG_ERROR, "can't build request headers");
          break;
        }
      }
      if (state->fnprephdrs && state->fnprephdrs(state, hdrs) != SY_OK) break;

      /* connecting */
      if (SyTCPConnect(&state->fd, purl->host, purl->port, state->iface, state->ioTimeout, state->pfn) != SY_OK) break;
      SY_CHECK_BREAK0
      /* handshaking */
      state->status = SY_STATUS_HANDSHAKING;
#ifdef SYOPT_ALLOW_HTTPS
      if (url->proto == SY_PROTO_HTTPS && state->httpproxy) {
        proxy = SyProxyNew(state->ftpproxy, SY_PROXY_HTTP_CONNECT, state->userAgent);
        if (!proxy) break;
        if (SyProxyConnect(&state->fd, proxy, url->host, url->port, state->pfn) != SY_OK) break;
        SY_CHECK_BREAK0
      }
      if (url->proto == SY_PROTO_HTTPS) {
        if (SyTCPInitSSL(&state->fd, url->host, state->ioTimeout, state->pfn) != SY_OK) break;
      }
#endif

      if (SyHTTPSendQuery(&state->fd, hdrs, state->pfn) != SY_OK) break;
      SY_CHECK_BREAK0
      /* send post data, if any */
      if (postLen) {
        SyMessage(state->pfn, SY_MSG_NOTICE, " sending %i bytes of POST data", postLen);
        if (SyTCPSend(&state->fd, state->postData, postLen) != SY_OK) {
          SyMessage(state->pfn, SY_MSG_ERROR, "can't send POST data");
          break;
        }
      }
      /* read reply headers */
      if (SyHTTPReadHeaders(hdrs, &state->fd, state->pfn) != SY_OK) break;
      if (state->fngothdrs && state->fngothdrs(state, hdrs) != SY_OK) break;
      SY_CHECK_BREAK0
      code = hdrs->code/100;
      state->replyCode = hdrs->code;
      if (code == 3) {
        /* redirect */
        state->status = SY_STATUS_REDIRECTING;
        SyTCPCloseSocket(&state->fd);
        if (--rdcnt < 1) { SyMessage(state->pfn, SY_MSG_ERROR, "too many redirects"); break; }
        s = SyHdrGetFieldValue(hdrs, "Location");
        if (!s) { SyMessage(state->pfn, SY_MSG_ERROR, "redirect to nowhere"); break; }
        if (!s[0]) { SyStrFree(s); SyMessage(state->pfn, SY_MSG_ERROR, "invalid redirect"); break; }
        /* check for 'news.php' or 'news.php?dir=/abc' or 'news.php?n=1#/abc */
        /*BUG: possible bug! why i'm prepending '/'? */
        nodir = 0;
        if (!strstr(s, "://")) {
          if (*s != '/') {
            /* seems to be a relative path */
            char *x = SySPrintf("%s%s", url->dir, s); SyStrFree(s);
            if (!x) { SyMessage(state->pfn, SY_MSG_ERROR, "memory error"); break; }
            s = x; nodir = 0;
/*
            t = SySPrintf("/%s", s); SyStrFree(s);
            if (!t) { SyMessage(state->pfn, SY_MSG_ERROR, "memory error"); break; }
            s = t; nodir = 1;
*/
          }
        }
        /* check for spaces in url */
        if (strncmp(s, "ftp://", 6) && strchr(s, ' ')) {
          int cnt = 0; char *xp = s, *tp;
          while (*xp) { if (*xp == ' ') cnt++; xp++; }
          t = SyStrAlloc(strlen(s)+cnt*4); //actually *3
          if (!t) { SyStrFree(s); SyMessage(state->pfn, SY_MSG_ERROR, "memory error"); break; }
          for (xp = s, tp = t; *xp; xp++) {
            if (*xp != ' ') *tp++ = *xp;
            else { *tp++ = '%'; *tp++ = '2'; *tp++ = '0'; }
          }
          *tp = '\0';
          SyStrFree(s); s = t;
        }
        lurl = SyURLNew(); if (!lurl) { SyStrFree(s); SyMessage(state->pfn, SY_MSG_ERROR, "memory error"); break; }
        if (SyURLParse(lurl, s) != SY_OK) {
          SyStrFree(s); SyClearURL(&lurl);
          SyMessage(state->pfn, SY_MSG_ERROR, "invalid redirect");
          break;
        }
        SyStrFree(s);
        if (nodir) {
          /* relative */
          if (strcmp(lurl->dir, "/")) {
            /* has other dir parts */
            s = SySPrintf("%s%s", url->dir, (lurl->dir)+1);
            if (!s) { SyClearURL(&lurl); SyMessage(state->pfn, SY_MSG_ERROR, "memory error"); break; }
            SyClearStr(&lurl->dir); lurl->dir = s;
          }
        }
        SyStrFree(url->dir); url->dir = lurl->dir;
        if (lurl->host[0]) {
          SyClearStr(&url->protostr); SyClearStr(&url->host);
          SyClearStr(&url->user); SyClearStr(&url->pass);
          url->port = lurl->port;
          url->defaultPort = lurl->defaultPort;
          url->proto = lurl->proto;
          url->protostr = lurl->protostr;
          url->host = lurl->host;
          url->user = lurl->user;
          url->pass = lurl->pass;
          lurl->protostr = lurl->host = lurl->user = lurl->pass = NULL;
        } else {
          SyClearStr(&lurl->protostr); SyClearStr(&lurl->host);
          SyClearStr(&lurl->user); SyClearStr(&lurl->pass);
        }
        SyClearStr(&url->file); url->file = lurl->file;
        SyClearStr(&url->query); url->query = lurl->query;
        SyClearStr(&url->anchor); url->anchor = lurl->anchor;
        free(lurl); /* DO NOT CLEAR lurl! */
        /* again */
      } else {
        /* no redirect */
        if (code != 2 && state->allowOnly2XX != SY_FALSE) { SyMessage(state->pfn, SY_MSG_ERROR, "%s", hdrs->firstLine); break; }
        if (state->firstByte && hdrs->code != 206) {
          SyMessage(state->pfn, SY_MSG_WARNING, "can't resume");
          if (state->fnnoresume && state->fnnoresume(state) != SY_OK) break;
          state->firstByte = 0;
        }
        state->fileSize = SyHTTPGetSize(hdrs);
        if (state->fileSize >= 0) {
          state->fileSize += state->firstByte;
          if (state->firstByte > state->fileSize) break;
        }
        if ((s = SyHdrGetFieldValue(hdrs, "Transfer-Encoding")) != NULL) {
          if (strstr(s, "chunked")) {
            state->chunked = SY_TRUE;
            state->fileSize = -1;
          }
          free(s);
        }
        if ((s = SyHdrGetFieldValue(hdrs, "Content-Disposition")) != NULL) {
          /* extract file name, if any */
          buf = s;
          while (*s) {
            t = strcasestr(s, "filename");
            if (!t) break;
            if (t != buf && isalnum(*(t-1))) { s = t+1; continue; }
            t += 8; if (*t && isalnum(*t)) { s = t; continue; }
            while (*t && *t <= ' ') t++;
            /* strange format: filename*=UTF-8''FurShaderExample.rar */
            if (*t == '*' && t[1] == '=') {
              t += 2; while (*t && *t != '\x27' && *t != ';') t++;
              if (*t == ';') { s = t; continue; }
              while (*t && *t == '\x27') t++;
            } else {
              if (*t != '=') { s = t; continue; }
              t++; while (*t && *t <= ' ') t++;
            }
            if (!(*t)) break;
            if (*t == '"') {
              t++;
              s = t;
              while (*s && *s != '"') s++;
            } else {
              s = t;
              while (*s && (*s != ' ' && *s != ';')) s++;
              if (*s == ' ') {
                // check for idiotic servers that doesn't know about quoting
                char *tx = s;
                while (*tx && *tx != ';') ++tx;
                if (!tx[0]) s = tx;
              }
            }
            *s = '\0';
            state->httpFName = SyStrNew(t, -1);
            SyStrTrim(state->httpFName);
            if (!state->httpFName[0]) SyClearStr(&state->httpFName);
            break;
          }
          free(buf);
        }
        res = SY_OK; break;
      }
    }
  }
  SyHdrFree(hdrs);
  SyProxyFree(proxy);
  if (res == SY_OK) {
    if (state->lastByte < 0 && state->fileSize >= 0) state->lastByte = state->fileSize;
    if ((state->lastByte >= 0 && state->firstByte > state->lastByte) ||
        (state->fileSize >= 0 && state->lastByte > state->fileSize)) {
      SyMessage(state->pfn, SY_MSG_ERROR, "invalid file range");
      res = SY_ERROR;
    }
  } else if (state->fd.errCode) {
    s = SyTCPGetLastErrorMsg (&state->fd);
    if (s) {
      SyMessage(state->pfn, SY_MSG_ERROR, "socket error: %s", s);
      SyStrFree(s);
    } else SyMessage(state->pfn, SY_MSG_ERROR, "socket error: %i", state->fd.errCode);
  }
  if (res == SY_OK) state->status = SY_STATUS_CONNECTED;
  SyInitStats(state, &state->stats);
  return res;
}


TSyResult SyRun (TSyState *state) {
  TSyResult res = SY_ERROR;
  char *buf, *bufPtr; int bufSize, bufLeft;
  int64_t total, done, left;
  double lastprogtime, tm, ctime;
  TSySocket *rsock;

  if (!state || state->status != SY_STATUS_CONNECTED) return SY_ERROR;
  state->currentByte = 0; state->error = SY_FALSE;
  if (state->fileSize >= 0) {
    if ((state->lastByte >= 0 && state->lastByte == state->firstByte) || state->firstByte == state->fileSize) {
      state->status = SY_STATUS_COMPLETE;
      return SY_OK;
    }
  }
  if (!state->fnopen) {
    /* no 'open' function, assume simple check */
    state->status = SY_STATUS_COMPLETE;
    return SY_OK;
  }
  /* prepare buffer */
  bufSize = state->initBufferSize; if (bufSize < 1) bufSize = 1;
  buf = malloc(bufSize);
  if (!buf) {
    SyMessage(state->pfn, SY_MSG_ERROR, "memory error");
    state->error = SY_TRUE;
    return SY_ERROR;
  }
  /* open file */
  if (state->fnopen(state) != SY_OK) {
    free(buf);
    state->error = SY_TRUE;
    return SY_ERROR;
  }
  /* init vars */
  total = state->fileSize;
  if (total > 0) {
    total -= state->firstByte;
    if (state->lastByte > 0) total -= (state->fileSize-state->lastByte);
  }
  left = total; done = 0;
  SyInitStats(state, &state->stats);
  SyUpdateStats(state, &state->stats, 0, total);
  SyMessage(state->pfn, SY_MSG_MSG, "starting data transfer");
  if (state->fnprog) state->fnprog(state, 0, total, SY_FALSE);
  lastprogtime = SyGetTimeD();
  rsock = (state->datafd.fd>=0)?&state->datafd:&state->fd;
  /* start downloading */
  state->status = SY_STATUS_DOWNLOADING;
  int chunkdone = 1;
  while (state->chunked || left < 0 || left > 0) {
    int tord, rd, flg;
    SY_CHECK_BREAK0
    /*fprintf(stderr, "left: %i\n", (int)left);*/
    if (state->chunked && chunkdone) {
      char cls[128];
      if (left != -1) {
        /* not the first chunk; should receive chunk end ("\r\n") */
        if (SyTCPReceive(rsock, cls, 2) != 2) { res = SY_ERROR; break; }
        if (cls[0] != '\r' || cls[1] != '\n') { res = SY_ERROR; break; }
      }
      /* chunked, get chunk length */
      if (!SyTCPReceiveStrEx(rsock, 120, cls)) { res = SY_ERROR; break; }
      //fprintf(stderr, " [%s]\n", cls);
      SyStrTrim(cls);
      int64_t csz = SyHexStr2Long(cls);
      if (csz < 1) { res = SY_ERROR; break; }
      if (csz == 0) { res = SY_OK; left = 0; break; } // done
      //fprintf(stderr, "chunk size: %d\n", (int)csz);
      left = csz;
      chunkdone = 0;
    }
    /* how many bytes we should receive? */
    tord = bufSize;
    if (left >= (int64_t)0) {
      flg = (int64_t)tord > left;
      if (flg) tord = (int)left;
    }
    rsock->errCode = 0;
    ctime = tm = SyGetTimeD();
#ifdef SYDL_USE_OLD_ALGO
    /*rd = SyTCPReceiveEx(rsock, buf, tord, SY_TCP_DONT_ALLOWPARTIAL);
    bufLeft = bufSize-(rd>0?rd:0);*/
    bufPtr = buf; bufLeft = tord; rd = 0;
    //fprintf(stderr, "TOREAD: %d (left=%d)\n", (int)bufLeft, (int)left);
    do {
      int xrd = SyTCPReceiveEx(rsock, bufPtr, bufLeft, SY_TCP_ALLOWPARTIAL);
      //fprintf(stderr, "...xrd=%d (bufleft=%d)\n", xrd, (int)bufLeft);
      ctime = SyGetTimeD();
      if (!xrd) break;
      if (xrd < 0) { rd = xrd; break; }
      rd += xrd; bufPtr += xrd; bufLeft -= xrd;
    } while (!bufLeft || rd >= bufSize/3);
    if (state->chunked && rd >= 0) {
      if (rd > left) {
        SyMessage(state->pfn, SY_MSG_ERROR, "internal error in chunk receiver!");
        rd = -1;
        state->breakNow = SY_TRUE;
      }
    }
#else
    bufPtr = buf; bufLeft = tord; rd = 0;
    do {
      int xrd = SyTCPReceiveEx(rsock, bufPtr, bufLeft, SY_TCP_ALLOWPARTIAL);
      ctime = SyGetTimeD();
      if (!xrd) break;
      if (xrd < 0) { rd = xrd; break; }
      rd += xrd; bufPtr += xrd; bufLeft -= xrd;
    } while (!bufLeft || ctime-tm >= 4.0);
#endif
    if (rd > 0) {
      /* write everything %-) */
      if (state->fnwrite && state->fnwrite(state, buf, rd) != SY_OK) break;
      if (left > 0) {
        left -= rd;
        done += rd;
        if (state->chunked) chunkdone = (left == 0);
        //fprintf(stderr, "...left=%d; chunkdone=%d\n", (int)left, (int)chunkdone);
      } else {
        if (state->chunked) chunkdone = 1;
      }
      state->currentByte = done;
      /* update stats */
      SyUpdateStats(state, &state->stats, done, total);
      /* and draw progress */
      if (tm-lastprogtime >= 1.0) {
        if (state->fnprog) state->fnprog(state, done, total, SY_FALSE);
        lastprogtime = ctime;
      }
      /* grow buffer if necessary */
      if (!bufLeft && ctime-tm < 2.0 && bufSize < state->maxBufferSize) {
        bufLeft = bufSize*2;
        if (bufLeft > state->maxBufferSize) bufLeft = state->maxBufferSize;
        bufPtr = realloc(buf, bufLeft);
        if (bufPtr) {
          /*fprintf(stderr, "\nbuffer grows from %i to %i\n", bufSize, bufLeft);*/
          bufSize = bufLeft; buf = bufPtr;
        }
      }
      /* shrink buffer if necessary */
      /*if (bufLeft && ctime-tm > 2.0 && bufSize > 8192) {
        bufLeft = bufSize/2;
      }*/
    }
    if (rd <= 0 || rsock->errCode) {
      /* read error or connection closed */
      if (rd >= 0 && left < 0) res = SY_OK; /* size unknown? all done */
      else if (state->breakNow == SY_TRUE) state->interrupted = SY_TRUE;
      break;
    }
  }
  /* check if downloading ok */
  if (!left) res = SY_OK;
  state->error = (res==SY_OK)?SY_FALSE:SY_TRUE;
  SyUpdateStats(state, &state->stats, done, total);
  if (state->fnprog) state->fnprog(state, done, total, SY_TRUE);
  if (state->fnclose && state->fnclose(state) != SY_OK) res = SY_ERROR; /* don't set state->error here! */
  /* free memory, close sockets */
  if (buf) free(buf);
  SyTCPCloseSocket(&state->datafd); SyTCPCloseSocket(&state->fd);
  if (res == SY_OK) state->status = SY_STATUS_COMPLETE;
  if (rsock->errCode && (left > 0 || res != SY_OK)) {
    char *s = SyTCPGetLastErrorMsg(rsock);
    if (s) {
      SyMessage(state->pfn, SY_MSG_ERROR, "socket error: %s", s);
      SyStrFree(s);
    } else SyMessage(state->pfn, SY_MSG_ERROR, "socket error: %i", rsock->errCode);
  }
  return res;
}


#endif
