/*
  Syren -- a lightweight downloader for Linux/BSD/MacOSX
  inspired by Axel Copyright 2001-2002 Wilmer van der Gaast
  version 0.0.6 (atomic alien)
  coded by Ketmar // Vampire Avalon

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License with
  the Debian GNU/Linux distribution in file /usr/doc/copyright/GPL;
  if not, write to the Free Software Foundation, Inc., 59 Temple Place,
  Suite 330, Boston, MA  02111-1307  USA
*/
/*
  Syren ftp utilities
 */
#ifndef _SYREN_FTP_C
#define _SYREN_FTP_C

#include "syren_ftp.h"


char *SyFTPWait (int *code, TSySocket *fd, const TSyPrintStr *pfn) {
  char *s, *buf, *lbuf, *rline = NULL;
  int multi = 0, rep = -1;

  if (code) *code = -1;
  lbuf = calloc(1, 8200); if (!lbuf) return NULL;
  while (1) {
    buf = SyTCPReceiveStrEx(fd, 8192, lbuf);
    if (!buf) { free(lbuf); SyMessage(pfn, SY_MSG_ERROR, "read error"); return NULL; }
    SyMessage(pfn, SY_MSG_NOTICE, " %s", buf);
    /* extract code */
    s = buf;
    if (isdigit(s[0]) && isdigit(s[1]) & isdigit(s[2])) rep = (s[0]-'0')*100+(s[1]-'0')*10+(s[2]-'0'); else rep = 0;
    /* check code */
    if (!multi) {
      /* first line of reply */
      if (!rep) {
        free(lbuf);
        SyMessage(pfn, SY_MSG_ERROR, "invalid reply code");
        return NULL;
      }
      if (s[3] == '-') multi = rep;
      else if (s[3] != ' ') {
        free(lbuf);
        SyMessage(pfn, SY_MSG_ERROR, "invalid reply");
        return NULL;
      } else {
        /* first line */
        rline = SyStrDup(lbuf);
        if (!rline) {
          free(lbuf);
          SyMessage(pfn, SY_MSG_ERROR, "memory error");
          return NULL;
        }
        break;
      }
    }
    /* multiline continues */
    if (multi == rep && s[3] == ' ') break;
    /*free(buf);*/
  }
  if (code) *code = rep;
  return buf;
}


int SyFTPWaitFor2 (TSySocket *fd, int code0, int code1, const TSyPrintStr *pfn) {
  int c;
  char *rep = SyFTPWait(&c, fd, pfn);
  if (rep) free(rep);
  if (c < 1) return -1;
  if (code0 < 0) {
    if (code1 < 0) return c;
    code0 = code1; code1 = -1;
  }
  if ((code0 > 0 && c/100 == code0) || (code1 > 0 && c/100 == code1)) return c;
  if (code1 > 0)
    SyMessage(pfn, SY_MSG_ERROR, "invalid reply code (got: %i; expected: %ixx or %ixx)", c, code0, code1);
  else
    SyMessage(pfn, SY_MSG_ERROR, "invalid reply code (got: %i; expected: %ixx)", c, code0);
  return -1;
}


/* perform ftp 'handshake': send USER/PASS */
TSyResult SyFTPStart (TSySocket *fd, const char *user, const char* pass, const TSyPrintStr *pfn) {
  int code;
  char *s;
  if (SyFTPWaitFor2(fd, 2, -1, pfn) < 0) return SY_ERROR;
  if (SyTCPSendLine(pfn, 1, fd, "USER %s", (user&&*user)?user:"anonymous") != SY_OK) return SY_ERROR;
  code = SyFTPWaitFor2(fd, 2, 3, pfn);
  if (code < 0) return SY_ERROR;
  if (code/100 == 3) {
    s = SySPrintf("PASS %s\r\n", (pass&&*pass)?pass:"syren@nowhere.org");
    if (!s) { SyMessage(pfn, SY_MSG_ERROR, "memory error"); return SY_ERROR; }
    SyMessage(pfn, SY_MSG_NOTICE, " PASS ********");
    if (SyTCPSendStr(fd, s) != SY_OK) { free(s); SyMessage(pfn, SY_MSG_ERROR, "socket write error"); return SY_ERROR; }
    free(s);
    if (SyFTPWaitFor2(fd, 2, -1, pfn) < 0) return SY_ERROR;
  }
  /* set binary mode */
  if (SyTCPSendLine(pfn, 1, fd, "TYPE I") != SY_OK) return SY_ERROR;
  return (SyFTPWaitFor2(fd, 2, -1, pfn)<0?SY_ERROR:SY_OK);
}


/* change current working directory */
TSyResult SyFTPCwd (TSySocket *fd, const char *cwd, const TSyPrintStr *pfn) {
  char *s, *t;
  TSyResult res;

  if (!cwd) return SY_OK;
  s = SyStrNew(cwd, -1); if (!s) { SyMessage(pfn, SY_MSG_ERROR, "memory error"); return SY_ERROR; }
  t = s+strlen(s)-1; if (t > s && *t == '/') *t = '\0';
  res = SyTCPSendLine(pfn, 1, fd, "CWD %s", s); free(s);
  if (res != SY_OK) return SY_ERROR;
  return (SyFTPWaitFor2(fd, 2, -1, pfn)<0?SY_ERROR:SY_OK);
}


/* open a data connection */
TSyResult SyFTPOpenDataPassv (TSySocket *fd, TSySocket *datafd, const char *iface, int timeout,
  const TSyPrintStr *pfn, TSyProxy *proxy) {

  int f, info[6];
  char *host = NULL;
  int code; char *rep;
  TSyResult res;

  if (SyTCPSendLine(pfn, 1, fd, "PASV") != SY_OK) return SY_ERROR;
  rep = SyFTPWait(&code, fd, pfn);
  if (!rep) return SY_ERROR;
  if (code/100 != 2) {
    free(rep);
    SyMessage(pfn, SY_MSG_ERROR, "invalid reply code (got: %i; expected: 2xx)", code);
    return SY_ERROR;
  }
  for (f = 0; rep[f]; f++) {
    if (sscanf(&(rep[f]), "%i,%i,%i,%i,%i,%i",
               &info[0], &info[1], &info[2], &info[3], &info[4], &info[5]) == 6) {
      host = SySPrintf("%i.%i.%i.%i", info[0], info[1], info[2], info[3]);
      break;
    }
  }
  free(rep);
  if (!host) { SyMessage(pfn, SY_MSG_ERROR, "invalid PASV reply"); return SY_ERROR; }
  if (proxy) {
    res = SyTCPConnect(datafd, proxy->url->host, proxy->url->port, iface, timeout, pfn);
    if (res == SY_OK) res = SyProxyConnect(datafd, proxy, host, info[4]*256+info[5], pfn);
  } else {
    res = SyTCPConnect(datafd, host, info[4]*256+info[5], iface, timeout, pfn);
  }
  free(host);
  return res;
}


/* get file size; <0: error */
/* FIXME: remove recursion! */
int64_t SyFTPGetSize (TSySocket *fd, const char *iface, const char *filename, int maxredir, int timeout,
  const TSyPrintStr *pfn, TSyProxy *proxy) {

  int f, c, size = 256;
  char *reply, *rr, *s, *t, *fn;
  int code; char *rep;
  TSySocket datafd;
  int64_t sz;

  /* try the SIZE command first, if possible */
  if (SyTCPSendLine(pfn, 1, fd, "SIZE %s", filename) != SY_OK) return SY_ERROR;
  rep = SyFTPWait(&code, fd, pfn);
  if (!rep) return SY_ERROR;
  if (code/100 == 2) {
    s = rep; while (*s && *s > ' ') s++;
    int send = 0;
    sz = SyStr2LongEx(s, &send);
    if (sz < 0 || (unsigned char)(s[send]) > ' ') {
      free(rep);
      SyMessage(pfn, SY_MSG_ERROR, "invalid SIZE reply");
      return -1;
    }
    free(rep);
    return sz;
  } else if (code/10 != 50) {
    free(rep);
    SyMessage(pfn, SY_MSG_ERROR, "file not found");
    return -1;
  }

  if (maxredir <= 0) {
    SyMessage(pfn, SY_MSG_ERROR, "too many redirects");
    return -1;
  }

  if (SyFTPOpenDataPassv(fd, &datafd, iface, timeout, pfn, proxy) != SY_OK) return -1;
  if (SyTCPSendLine(pfn, 1, fd, "LIST %s", filename) != SY_OK) { SyTCPCloseSocket(&datafd); return -1; }
  if (SyFTPWaitFor2(fd, 1, -1, pfn) < 0) { SyTCPCloseSocket(&datafd); return -1; }

  /* read reply from the server */
  reply = calloc(1, size+4);
  if (!reply) {
    SyTCPCloseSocket(&datafd);
    SyMessage(pfn, SY_MSG_ERROR, "memory error");
    return -1;
  }
  f = 1;
  /*while ((c = recv(datafd, reply+f, size-f-3, 0)) > 0) {*/
  while ((c = SyTCPReceiveEx(&datafd, reply+f, size-f-3, SY_TCP_ALLOWPARTIAL)) > 0) {
    f += c; reply[f] = '\0';
    if (size-f <= 10) {
      if (size >= 1024*1024) { SyTCPCloseSocket(&datafd); free(reply); return -1; }
      size *= 2; rr = realloc(reply, size+4);
      if (!rr) { SyTCPCloseSocket(&datafd); free(reply); return -1; }
    }
  }
  SyTCPCloseSocket(&datafd);
  if (SyFTPWaitFor2(fd, 1, -1, pfn) < 0) { free(reply); return -1; }
  strcat(reply, "\n");

  /* count the number of probably legal matches: Files&Links only */
  c = 0;
  for (f = 1; reply[f] && reply[f+1]; f++) {
    if (reply[f] == '-' || reply[f] == 'l') c++;
    else while (reply[f] != '\n' && reply[f]) f++;
  }

  /* no match or more than one match */
  if (c != 1) {
    free(reply);
    if (c == 0) SyMessage(pfn, SY_MSG_ERROR, "file not found");
    else SyMessage(pfn, SY_MSG_ERROR, "multiple matches for file");
    return -1;
  }

  /* symlink handling */
  s = strstr(reply, "\nl");
  if (s) {
    /* get the real filename */
    fn = calloc(1, 1024); if (!fn) {
      free(reply);
      SyMessage(pfn, SY_MSG_ERROR, "memory error");
      return -1;
    }
    sscanf(s, "%*s %*i %*s %*s %*i %*s %*i %*s %1023s", fn);
    t = strstr(s, "->");
    if (!t) {
      free(fn);
      free(reply);
      SyMessage(pfn, SY_MSG_ERROR, "invalid LIST reply");
      return -1;
    }
    /* get size of the file linked to */
    strcpy(fn, t+3);
    free(reply);
    if ((reply = strchr(fn, '\r')) != NULL) *reply = '\0';
    if ((reply = strchr(fn, '\n')) != NULL) *reply = '\0';
    sz = SyFTPGetSize(fd, iface, fn, maxredir-1, timeout, pfn, proxy);
    free(fn);
    return sz;
  } else {
    /* normal file, so read the size */
    s = strstr(reply, "\n-");
    if (!s) {
      free(reply);
      SyMessage(pfn, SY_MSG_ERROR, "invalid LIST reply");
      return -1;
    }
    /* fuck m$! msvcrt doesn't undersand %lli (and %Li too) */
    /*f = sscanf(s, "%*s %*i %*s %*s %lli %*s %*i %*s %*s", &sz);
    if (f < 2) {
      f = sscanf(s, "%*s %*i %lli %*i %*s %*i %*i %*s", &sz);
      if (f < 2) {
        free(reply);
        SyMessage(pfn, SY_MSG_ERROR, "invalid LIST reply");
        return -1;
      }
    }*/
    sz = -1;
    for (f = 2; f; f--) {
      for (c = 2; c; c--) { while (*s && *s > ' ') s++; while (*s && *s <= ' ') s++; }
      if (!s[0]) break;
      t = s; while (isdigit(*t)) t++;
      if (t > s && *t && *t <= ' ') {
        *t = '\0'; sz = SyStr2Long(s);
        break;
      }
    }
    free(reply);
    return sz;
  }
}


#endif
