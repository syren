/*
  Syren -- a lightweight downloader for Linux/BSD/MacOSX
  inspired by Axel Copyright 2001-2002 Wilmer van der Gaast
  version 0.0.6 (atomic alien)
  coded by Ketmar // Vampire Avalon

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License with
  the Debian GNU/Linux distribution in file /usr/doc/copyright/GPL;
  if not, write to the Free Software Foundation, Inc., 59 Temple Place,
  Suite 330, Boston, MA  02111-1307  USA
*/
/*
  Syren http utilities
 */
#ifndef _SYREN_HTTP_C
#define _SYREN_HTTP_C

#include "syren_http.h"


/* return ptr to next line */
static char *SyHTTPFixLine (char *curstr) {
  char *nl = curstr, *res;

  while (*nl && *nl != '\n') nl++;
  res = nl+(*nl?1:0);
  *nl = '\0';
  if (nl > curstr && *(nl-1) == '\r') *(nl-1) = '\0';

  return res;
}


TSyResult SyHTTPReadHeaders (TSyHdrs *hdrs, TSySocket *fd, const TSyPrintStr *pfn) {
  char *hdrbuf, *curstr, *nextstr;
  TSyResult res;

  SyMessage(pfn, SY_MSG_NOTICE, "reading reply headers");
  if (!hdrs) return SY_ERROR;
  SyHdrClear(hdrs, SY_HDR_REPLY);
  if (!fd || fd->fd < 0) return SY_ERROR;
  hdrbuf = SyTCPReceiveHdrs(fd, 65536);
  if (!hdrbuf) {
    SyMessage(pfn, SY_MSG_ERROR, "error receiving reply headers");
    return SY_ERROR;
  }
  curstr = hdrbuf;
  while (*curstr) {
    nextstr = SyHTTPFixLine(curstr);
    if (!nextstr[0]) break;
    SyMessage(pfn, SY_MSG_NOTICE, " %s", curstr);
    res = SyHdrAddLine(hdrs, curstr);
    if (res != SY_OK) {
      free(hdrbuf);
      SyMessage(pfn, SY_MSG_ERROR, "can't parse reply header string");
      return SY_ERROR;
    }
    curstr = nextstr;
  }
  free(hdrbuf);
  if (hdrs->code < 0) {
    SyMessage(pfn, SY_MSG_ERROR, "empty reply headers");
    return SY_ERROR;
  }
  SyMessage(pfn, SY_MSG_NOTICE, " reply headers received; code: %i", hdrs->code);
  return SY_OK;
}


/* -1: none or error */
int64_t SyHTTPGetSize (const TSyHdrs *hdrs) {
  int64_t res;
  char *s, *t;
  char *buf = SyHdrGetFieldValue(hdrs, "content-length");
  res = SyStr2Long(buf); if (buf) free(buf);
  if (res >= 0) return res;
  /*Content-Disposition: attachment; filename="textpattern-4.0.6.tar.gz"; size = "304354"*/
  buf = SyHdrGetFieldValue(hdrs, "Content-Disposition");
  if (buf) {
    s = buf;
    while (*s) {
      t = strcasestr(s, "size");
      if (!t) break;
      if (t != buf && isalnum(*(t-1))) { s = t+1; continue; }
      t += 4; if (*t && isalnum(*t)) { s = t; continue; }
      while (*t && *t <= ' ') t++;
      if (*t != '=') { s = t; continue; }
      t++; while (*t && *t <= ' ') t++;
      if (!(*t)) break;
      if (*t == '"') { t++; s = t; while (*s && *s != '"') s++; }
      else { s = t; while (*s && (*s != ' ' && *s != ';')) s++; }
      *s = '\0';
      res = SyStr2Long(t);
      break;
    }
    free(buf);
    return res;
  }
  //Content-Range: bytes 2759580-21263647/21263648
  buf = SyHdrGetFieldValue(hdrs, "Content-Range");
  if (!buf) return -1;
  t = strchr(buf, '/');
  if (!strcasestr(buf, "bytes") || !t || !t[1]) { free(buf); return -1; }
  for (s = ++t; *s; s++) if (!isdigit(*s)) { free(buf); return -1; }
  res = SyStr2Long(t);
  free(buf);
  return res;
}


static TSyResult SyHTTPAddAuth (TSyHdrs *hdrs, const char *astr, const TSyURL *url) {
  TSyResult res;
  const char *user, *pass;
  char *tmp, *s;

  user = url->user; pass = url->pass;
  if ((user && *user) || (pass && *pass)) {
    tmp = SyBuildAuthStr(user, pass); if (!tmp) return SY_ERROR;
    s = SySPrintf(astr, tmp);
    free(tmp); if (!s) return SY_ERROR;
    res = SyHdrAddLine(hdrs, s); free(s);
    if (res != SY_OK) return SY_ERROR;
  }

  return SY_OK;
}


TSyResult SyHTTPBuildQuery (TSyHdrs *hdrs, const char *method, const TSyURL *url, const TSyURL *proxy) {
  char port[32], *tmp, *s;
  const char *mt;
  TSyResult res;
  if (!hdrs) return SY_ERROR;

  SyHdrClear(hdrs, SY_HDR_REQUEST);
  if (!url) return SY_ERROR;
  /* query string */
  if (url->port != 80) sprintf(port, ":%i", url->port); else *port = '\0';
  mt = (method && *method)?method:"GET";
  if (proxy) {
    /*if (*url->user || *url->pass)
      s = SySPrintf("%s %s://%s:%s@%s%s", mt, url->protostr, url->user, url->pass, url->host, port);
    else*/
      s = SySPrintf("%s %s://%s%s", mt, url->protostr, url->host, port);
  } else s = SySPrintf("%s ", mt);
  if (!s) return SY_ERROR;
  tmp = SySPrintf("%s%s%s%s%s HTTP/1.1", s, url->dir, url->file, url->query, url->anchor);
  free(s); if (!tmp) return SY_ERROR;
  res = SyHdrAddLine(hdrs, tmp); free(tmp);
  if (res != SY_OK) return SY_ERROR;
  /* auth */
  if (proxy) {
    if (SyHTTPAddAuth(hdrs, "Proxy-Authorization: Basic %s", proxy) != SY_OK) return SY_ERROR;
  }
  if (SyHTTPAddAuth(hdrs, "Authorization: Basic %s", url) != SY_OK) return SY_ERROR;
  /* host */
  if ((url->proto == SY_PROTO_HTTPS && url->port != 443) ||
      (url->proto != SY_PROTO_HTTPS && url->port != 80))
    s = SySPrintf("Host: %s:%i", url->host, url->port); /* don't add 443 for HTTPS too */
  else
    s = SySPrintf("Host: %s", url->host);
  if (!s) return SY_ERROR;
  res = SyHdrAddLine(hdrs, s); free(s);
#ifdef SY_DNT
  SyHdrAddLine(hdrs, "X-Tracking-Choice: do-not-track");
  SyHdrAddLine(hdrs, "DNT: 1");
  SyHdrAddLine(hdrs, "X-Do-Not-Track: 1");
#endif
  return res;
}


TSyResult SyHTTPAddRange (TSyHdrs *hdrs, int from, int to) {
  TSyResult res;
  char *s;
  if (!hdrs) return SY_ERROR;
  if (from <= 0) {
    if (to < 0) return SY_OK;
    from = 0;
  }
  if (to >= 0 && to < from) return SY_ERROR;
  if (to >= 0) {
    if (from > 0) s = SySPrintf("Range: bytes=%i-%i", from, to);
    else s = SySPrintf("Range: bytes=-%i", to);
  } else {
    if (from > 0) s = SySPrintf("Range: bytes=%i-", from); else return SY_OK;
  }
  res = SyHdrAddLine(hdrs, s); free(s);
  return res;
}


/* FIXME: hide password! */
TSyResult SyHTTPSendQuery (TSySocket *fd, const TSyHdrs *hdrs, const TSyPrintStr *pfn) {
  TSyKVListItem *item, *ci;
  TSyKVList *cc;

  if (!hdrs || !hdrs->fields || hdrs->type == SY_HDR_INVALID) return SY_ERROR;
  if (!hdrs->firstLine) { SyMessage(pfn, SY_MSG_ERROR, "incomplete header"); return SY_ERROR; }
  if (SyTCPSendLine(pfn, 1, fd, "%s", hdrs->firstLine) != SY_OK) return SY_ERROR;
  /* auth */
  item = hdrs->fields->first;
  while (item) {
    cc = item->udata;
    if (cc && cc->count) {
      ci = cc->first;
      while (ci) {
        if (SyTCPSendLine(pfn, 1, fd, "%s: %s", item->key, ci->value) != SY_OK) return SY_ERROR;
        ci = ci->next;
      }
    } else {
      if (SyTCPSendLine(pfn, 1, fd, "%s: %s", item->key, item->value) != SY_OK) return SY_ERROR;
    }
    item = item->next;
  }
  return SyTCPSendLine(pfn, 0, fd, "");
}


#endif
