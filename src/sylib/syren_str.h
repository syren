/*
  Syren -- a lightweight downloader for Linux/BSD/MacOSX
  inspired by Axel Copyright 2001-2002 Wilmer van der Gaast
  version 0.0.6 (atomic alien)
  coded by Ketmar // Vampire Avalon

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License with
  the Debian GNU/Linux distribution in file /usr/doc/copyright/GPL;
  if not, write to the Free Software Foundation, Inc., 59 Temple Place,
  Suite 330, Boston, MA  02111-1307  USA
*/
/*
  Syren string manipulation package
 */
#ifndef _SYREN_STR_H
#define _SYREN_STR_H

#ifdef __cplusplus
extern "C" {
#endif


#include "syren_os.h"
#include "syren_common.h"


char *SySPrintf (const char *fmt, ...) __attribute__ ((format (printf, 1, 2)));


/* src can be NULL; maxlen < 0: maxlen = strlen(src?src:"") */
char *SyStrNew (const char *src, int maxlen);
char *SyStrAlloc (int len);
#define SyStrDup(str) SyStrNew(str, -1)
#define SyStrNewEmpty() SyStrNew(NULL, -1)
void SyStrFree (char *str);


/* key/value lists */
typedef struct _TSyKVListItem TSyKVListItem;
struct _TSyKVListItem {
  TSyKVListItem *next;
  char *key;
  char *value;
  char *ustr; /* for configs (descr); autofree */
  void *udata; /* for configs/cookies */
  int uidata; /* for configs/cookies */
};

typedef struct {
  TSyKVListItem *first;
  TSyKVListItem *last;
  int count;
  int casesens;
} TSyKVList;


TSyKVList *SyKVListNew (void);
void SyKVListFree (TSyKVList *lst);
void SyKVListClear (TSyKVList *lst);
void SyKVListClearItem (TSyKVListItem *item);
void SyKVListFreeItem (TSyKVListItem *item);
TSyKVListItem *SyKVListFind (const TSyKVList *lst, const char *key);
TSyResult SyKVListDelete (TSyKVList *lst, const char *key);
TSyKVListItem *SyKVListSet (TSyKVList *lst, const char *key, const char *value, int *newKey);


/* trim [dynamic] string "in-place" */
void SyStrTrim (char *s);

/* return NULL or new string */
char *SyURLDecode (const char *s);
/* return NULL or new string */
char *SyURLEncode (const char *s);

/* return NULL or new string */
char *SyBuildAuthStr (const char *user, const char *pass);


typedef enum {
  SY_PROTO_UNKNOWN=-1,
  SY_PROTO_HTTP=0,
  SY_PROTO_FTP=1,
  SY_PROTO_HTTPS=2,

  SY_PROTO_DEFAULT=SY_PROTO_HTTP
} TSyProto;
#define SY_PROTO_DEFAULT_STR  "http"


typedef struct {
  TSyProto proto;
  char *protostr;
  char *user, *pass;
  char *host;
  int port; TSyBool defaultPort;
  char *dir, *file, *query, *anchor;
} TSyURL;


TSyURL *SyURLNew (void);
void SyURLFree (TSyURL *url);
void SyURLClear (TSyURL *url);
TSyResult SyURLParse (TSyURL *dest, const char *urlstr);
TSyURL *SyURLClone (const TSyURL *src);

#ifdef SY_STR_URL2STR
/* return NULL or new string */
char *SyURL2StrEx (const TSyURL *url, TSyBool userpass, TSyBool hidepass);
/* return NULL or new string */
char *SyURL2Str (const TSyURL *url);
#endif


int64_t SyStr2LongEx (const char *s, int *ep);
/* -1: error */
int64_t SyStr2Long (const char *s);
/* -1: error */
int64_t SyHexStr2Long (const char *s);
/* -1: error */
int SyStr2Int (const char *s);

TSyResult SyLong2Str (char *dest, int64_t num);


#ifdef SY_STR_ADDON
/* dest: at least 26 chars */
/* returns len */
int SyLong2StrComma (char *dest, int64_t num);

/* dest: at least 10 bytes */
void SySize2Str (char *dest, int64_t size);

/* dest: at least 10 bytes */
void SyTime2Str (char *dest, int value);
#endif


#ifdef __cplusplus
}
#endif

#endif
