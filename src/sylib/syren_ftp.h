/*
  Syren -- a lightweight downloader for Linux/BSD/MacOSX
  inspired by Axel Copyright 2001-2002 Wilmer van der Gaast
  version 0.0.6 (atomic alien)
  coded by Ketmar // Vampire Avalon

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License with
  the Debian GNU/Linux distribution in file /usr/doc/copyright/GPL;
  if not, write to the Free Software Foundation, Inc., 59 Temple Place,
  Suite 330, Boston, MA  02111-1307  USA
*/
/*
  Syren ftp utilities
 */
#ifndef _SYREN_FTP_H
#define _SYREN_FTP_H

#ifdef __cplusplus
extern "C" {
#endif


#include "syren_common.h"
#include "syren_str.h"
#include "syren_msg.h"
#include "syren_tcp.h"
#include "syren_proxy.h"


/* <=0:err; else code; return result must be freed if not NULL */
char *SyFTPWait (int *code, TSySocket *fd, const TSyPrintStr *pfn);
/* code? < 0: ignore this one; returns received code */
int SyFTPWaitFor2 (TSySocket *fd, int code0, int code1, const TSyPrintStr *pfn);
/* perform ftp 'handshake': send USER/PASS */
TSyResult SyFTPStart (TSySocket *fd, const char *user, const char *pass, const TSyPrintStr *pfn);
/* change current working directory */
TSyResult SyFTPCwd (TSySocket *fd, const char *cwd, const TSyPrintStr *pfn);
/* open a data connection; initializes datafd */
TSyResult SyFTPOpenDataPassv (TSySocket *fd, TSySocket *datafd, const char *iface, int timeout,
  const TSyPrintStr *pfn, TSyProxy *proxy);
/* get file size; <0: error */
int64_t SyFTPGetSize (TSySocket *fd, const char *iface, const char *filename, int maxredir, int timeout,
  const TSyPrintStr *pfn, TSyProxy *proxy);


#ifdef __cplusplus
}
#endif

#endif
