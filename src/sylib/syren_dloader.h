/*
  Syren -- a lightweight downloader for Linux/BSD/MacOSX
  inspired by Axel Copyright 2001-2002 Wilmer van der Gaast
  version 0.0.6 (atomic alien)
  coded by Ketmar // Vampire Avalon

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License with
  the Debian GNU/Linux distribution in file /usr/doc/copyright/GPL;
  if not, write to the Free Software Foundation, Inc., 59 Temple Place,
  Suite 330, Boston, MA  02111-1307  USA
*/
/*
  Syren download driver
 */
#ifndef _SYREN_DLOADER_H
#define _SYREN_DLOADER_H

#ifdef __cplusplus
extern "C" {
#endif


#include "syren_os.h"
#include "syren_common.h"
#include "syren_str.h"
#include "syren_msg.h"
#include "syren_cfg.h"
#include "syren_tcp.h"
#include "syren_hdrs.h"
#include "syren_http.h"
#include "syren_proxy.h"
#include "syren_ftp.h"


/*typedef struct _TSyState TSyState;*/
typedef struct _TSyState TSyState;

/* timePassed: from the start */
/* bytesDone: # of bytes downloaded so far */
/* bytesTotal: # of bytes to download (-1: unknown); can be lesser than actual file size */
/* to obtain a real position one must do this:
  state->firstByte+bytesDone */
typedef void (*PSyFnProgress) (TSyState *state, int64_t bytesDone, int64_t bytesTotal, TSyBool done);

/* return SY_ERROR to stop */
/* one can add/change headers here */
typedef TSyResult (*PSyFnPrepareHeaders) (TSyState *state, TSyHdrs *hdrs);

/* return SY_ERROR to stop */
/* called after all headers read */
typedef TSyResult (*PSyFnGotHeaders) (TSyState *state, TSyHdrs *hdrs);

/* return SY_ERROR to stop */
typedef TSyResult (*PSyFnCantResume) (TSyState *state);

/* return SY_ERROR to stop */
/* called just before the downloading will be started; one can open file and set state->outfd */
/* check state->httpFName to get possible name from http headers */
typedef TSyResult (*PSyFnOpenFile) (TSyState *state);

/* return SY_ERROR to stop */
/* called just before after the downloading stopped; one can close file here */
/* check state->error to know if downloaded was ok */
typedef TSyResult (*PSyFnCloseFile) (TSyState *state);

/* return SY_ERROR to stop */
/* called when outfd <0 */
typedef TSyResult (*PSyFnWrite) (TSyState *state, void *buf, int bufSize);


typedef enum {
  SY_STATUS_UNPREPARED=0,
  /* afrer successfull SyPrepare() */
  SY_STATUS_PREPARED,
  /* in SyBegin() */
  SY_STATUS_CONNECTING,
  SY_STATUS_HANDSHAKING,
  SY_STATUS_REDIRECTING,
  /* after SyBegin() */
  SY_STATUS_CONNECTED,
  /* in SyRun() */
  SY_STATUS_DOWNLOADING,
  /* after SyRun(), if no errors */
  SY_STATUS_COMPLETE
  /* and now call SyEnd() to be UNPREPARED */
} TSyStatus;


typedef struct {
  int active;
  double time;
  int64_t bytesFrom;
  int64_t bytesDone;
} TSyStatInfo;


typedef struct {
  TSyStatInfo i[2];
  double interval;
  double sttime;
  double bps;
  double eta;
} TSyStats;


struct _TSyState {
  void *udata;
  int udatai;

  int maxBufferSize;
  int initBufferSize;
  int ioTimeout;
  int maxRedirects;
  TSyURL *url, *ftpproxy, *httpproxy;
  /* 'url' can be changed due to redirects */
  char *iface;
  TSyBool ftpUseConnect; /* use CONNECT method for ftp? */
  TSyBool allowOnly2XX; /* allow only 2XX codes */
  int replyCode; /* set by SyBegin() */

  int64_t firstByte, lastByte; /* lastByte == -1: to end */
  int64_t fileSize;

  char *postData; /* this pointer will be freed by SyDLClear()! */

  char *httpFName; /* not null if SyBegin() found 'Content-Disposition' */

  char *userAgent; /* can be NULL */

  TSyPrintStr *pfn;

  PSyFnProgress fnprog;
  PSyFnPrepareHeaders fnprephdrs;
  PSyFnGotHeaders fngothdrs;
  PSyFnCantResume fnnoresume;

  PSyFnOpenFile fnopen;
  PSyFnCloseFile fnclose;
  PSyFnWrite fnwrite;

  int64_t currentByte; /* currentByte+firstByte == filepos */

  TSyStatus status;
  TSyBool breakNow; /* set to SY_TRUE and SyDLRun will break */
  TSyBool interrupted; /* will be set to SY_TRUE if SyDLRun was breaked */
  TSyBool error; /* indicates error in SyRun() */

  TSySocket fd; /* socket fd */
  TSySocket datafd; /* ftp data socked fd (if any) */

  TSyStats stats; /* statistics: BPS, ETA...  SyRun() updates this */

  TSyBool chunked; /* HTTP chunked encoding */
};


double SyGetTimeD (void);

TSyState *SyNew (void);
void SyFree (TSyState *state);
void SyClear (TSyState *state);
TSyResult SyPrepare (TSyState *state, const char *urlstr, const char *httpproxyurl, const char *ftpproxyurl,
  const char *iface);
/*
 * call SyReset after download fails if you want to resume it
 * then you can call again SyBegin() and SyRun()
 * WARNING: DO NOT CALL SyReset() while SyRun() in progress!
 *          use state->breakNow to interrupt SyRun() first!
 */
void SyReset (TSyState *state);
TSyResult SyBegin (TSyState *state);
TSyResult SyRun (TSyState *state);
/* SyEnd() must be called after SyRun() complete (with any result)
   WARNING: it will clear the state! */
TSyResult SyEnd (TSyState *state);

void SyInitStats (TSyState *state, TSyStats *si);
void SyUpdateStats (TSyState *state, TSyStats *si, int64_t bytesDone, int64_t bytesTotal);


#ifdef __cplusplus
}
#endif

#endif
