/*
  Syren -- a lightweight downloader for Linux/BSD/MacOSX
  inspired by Axel Copyright 2001-2002 Wilmer van der Gaast
  version 0.0.6 (atomic alien)
  coded by Ketmar // Vampire Avalon

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License with
  the Debian GNU/Linux distribution in file /usr/doc/copyright/GPL;
  if not, write to the Free Software Foundation, Inc., 59 Temple Place,
  Suite 330, Boston, MA  02111-1307  USA
*/
/*
  Syren proxy connections (HTTP CONNECT, SOCKS)
 */
#ifndef _SYREN_PROXY_C
#define _SYREN_PROXY_C

#include "syren_proxy.h"


void SyProxyFree (TSyProxy *proxy) {
  if (!proxy) return;
  SyURLFree(proxy->url);
  SyStrFree(proxy->userAgent);
  free(proxy);
}


TSyProxy *SyProxyNew (const TSyURL *url, TSyProxyType ptype, const char *userAgent) {
  TSyProxy *res;

  if (!url) return NULL;
  if (ptype == SY_PROXY_NONE) return NULL;
  if (ptype < 0 || ptype > SY_PROXY_MAX) return NULL;
  res = calloc(1, sizeof(TSyProxy));
  if (!res) return NULL;
  res->ptype = ptype; res->retCode = -1;
  res->url = SyURLClone(url);
  if (res->url) {
    res->userAgent = SyStrDup(userAgent);
    if (res->userAgent) return res;
  }
  SyProxyFree(res);
  return NULL;
}


/* connect with CONNECT method; uag (user agent) and retCode can be NULL */
static TSyResult SyHTTPConnect (TSySocket *fd, TSyProxy *proxy, const char *host, int port, const TSyPrintStr *pfn) {
  TSyHdrs *hdrs;
  TSyResult res;
  const char *user, *pass;
  char *tmp;

  if (!proxy || !host || !host[0] || port < 1 || port > 65535) return SY_ERROR;
  SyMessage(pfn, SY_MSG_NOTICE, "CONNECTing to %s:%i", host, port);
  proxy->retCode = -1; /* unknown yet */
  if (SyTCPSendLine(pfn, 1, fd, "CONNECT %s:%i HTTP/1.1", host, port) != SY_OK) return SY_ERROR;
  if (SyTCPSendLine(pfn, 1, fd, "Host: %s:%i", host, port) != SY_OK) return SY_ERROR;
  if (proxy->userAgent && *proxy->userAgent) {
    if (SyTCPSendLine(pfn, 1, fd, "User-Agent: %s", proxy->userAgent) != SY_OK) return SY_ERROR;
  }
  user = proxy->url->user; pass = proxy->url->pass;
  if ((user && *user) || (pass && *pass)) {
    tmp = SyBuildAuthStr(user, pass); if (!tmp) return SY_ERROR;
    res = SyTCPSendLine(pfn, 1, fd, "Proxy-Authorization: Basic %s", tmp);
    free(tmp);
    if (res != SY_OK) return SY_ERROR;
  }
  if (SyTCPSendLine(pfn, 0, fd, "") != SY_OK) return SY_ERROR;

  hdrs = SyHdrNew();
  if (!hdrs) { SyMessage(pfn, SY_MSG_ERROR, "out of memory"); return SY_ERROR; }
  if (SyHTTPReadHeaders(hdrs, fd, pfn) != SY_OK) { SyHdrFree(hdrs); return SY_ERROR; }
  proxy->retCode = hdrs->code;
  res = (hdrs->code >= 200 && hdrs->code <= 299)?SY_OK:SY_ERROR;
  SyHdrFree(hdrs);
  return res;
}


/*
static TSyResult SySocks4Connect (TSySocket *fd, TSyProxy *proxy, const char *host, int port, const TSyPrintStr *pfn) {
  SyMessage(pfn, SY_MSG_ERROR, "SOCKS4 proxy support is not implemented yet");
  return SY_ERROR;
}


static TSyResult SySocks5Connect (TSySocket *fd, TSyProxy *proxy, const char *host, int port, const TSyPrintStr *pfn) {
  SyMessage(pfn, SY_MSG_ERROR, "SOCKS5 proxy support is not implemented yet");
  return SY_ERROR;
}
*/


static TSyResult SySocksConnect (TSySocket *fd, TSyProxy *proxy, const char *host, int port, const TSyPrintStr *pfn) {
  SyMessage(pfn, SY_MSG_ERROR, "SOCKS proxy support is not implemented yet");
  return SY_ERROR;
}


TSyResult SyProxyConnect (TSySocket *fd, TSyProxy *proxy, const char *host, int port, const TSyPrintStr *pfn) {
  if (!proxy) return SY_ERROR;
  switch (proxy->ptype) {
    case SY_PROXY_HTTP: return SY_OK;
    case SY_PROXY_HTTP_CONNECT: return SyHTTPConnect(fd, proxy, host, port, pfn);
    case SY_PROXY_SOCKS: return SySocksConnect(fd, proxy, host, port, pfn);
    default: ;
  }
  SyMessage(pfn, SY_MSG_ERROR, "unknown proxy type");
  return SY_ERROR;
}


#endif
