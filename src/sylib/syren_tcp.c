/*
  Syren -- a lightweight downloader for Linux/BSD/MacOSX
  inspired by Axel Copyright 2001-2002 Wilmer van der Gaast
  version 0.0.6 (atomic alien)
  coded by Ketmar // Vampire Avalon

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License with
  the Debian GNU/Linux distribution in file /usr/doc/copyright/GPL;
  if not, write to the Free Software Foundation, Inc., 59 Temple Place,
  Suite 330, Boston, MA  02111-1307  USA
*/
/*
  Syren tcp utilities
 */
#ifndef _SYREN_TCP_C
#define _SYREN_TCP_C

#include "syren_tcp.h"


#ifdef __APPLE__
#define SY_SOCK_MSG_NO_SIGNAL  SO_NOSIGPIPE
#else
#define SY_SOCK_MSG_NO_SIGNAL  MSG_NOSIGNAL
#endif


#include <errno.h>
#include <string.h>


#ifdef SYOPT_ALLOW_HTTPS

#ifdef SY_USE_GNUTLS
# include <gnutls/gnutls.h>
# include <gnutls/x509.h>

#define SSL_MAX_CONTENT_LEN  (32768)
static int gnutlsInited = 0;


typedef struct {
  gnutls_certificate_credentials_t xcred;
  gnutls_session_t session;
  //int handshakeComplete;
} TSySSLInfo;

static int SyTCP_SSLSend (TSySSLInfo *si, const void *buf, size_t len) {
  if (len < 1) return 0;
  for (;;) {
    int res = (int)gnutls_record_send(si->session, buf, len);
    //if (res < 0) fprintf(stderr, "GnuTLS: error sending %d bytes (%d) :%s\n", (int)len, res, gnutls_strerror(res));
    //if (res != GNUTLS_E_INTERRUPTED && res != GNUTLS_E_AGAIN) return res;
    if (res >= 0 || gnutls_error_is_fatal(res)) return res;
  }
}

static int SyTCP_SSLRecv (TSySSLInfo *si, void *buf, size_t len) {
  if (len < 1) return 0;
  for (;;) {
    int res = (int)gnutls_record_recv(si->session, buf, len);
    //if (res < 0) fprintf(stderr, "GnuTLS: error receiving %d bytes (%d) (fatal=%d) :%s\n", (int)len, res, gnutls_error_is_fatal(res), gnutls_strerror(res));
    //if (res != GNUTLS_E_INTERRUPTED && res != GNUTLS_E_AGAIN) return res;
    if (res >= 0 || gnutls_error_is_fatal(res)) return res;
  }
}

#else
# include "libpolarssl/ssl.h"
# include "libpolarssl/entropy.h"
# include "libpolarssl/ctr_drbg.h"

typedef struct {
  //havege_state hs;
  ssl_context ssl;
  ssl_session ssn;

  entropy_context ssl_entropy_cli;
  ctr_drbg_context ssl_ctr_drbg_cli;
  //entropy_context ssl_entropy_cli, ssl_entropy_srv;
  //ctr_drbg_context ssl_ctr_drbg_cli, ssl_ctr_drbg_srv;
  //pk_context ssl_pkey;
  //rsa_context ssl_rsa;
} TSySSLInfo;


static int SyTCP_SSLSend (void *sock, const unsigned char *buf, size_t len) {
  if (len < 1) return 0;
  int res = send(((TSySocket *)sock)->fd, (void *)buf, len, SY_SOCK_MSG_NO_SIGNAL);
  if (!res) res = -666;
  return res;
}


static int SyTCP_SSLRecv (void *sock, unsigned char *buf, size_t len) {
  if (len < 1) return 0;
  /*fprintf(stderr, "\r*** SyTCP_SSLRecv: %i \n", len);*/
  int res = recv(((TSySocket *)sock)->fd, (void *)buf, len, 0);
  /*fprintf(stderr, "\r*** SyTCP_SSLRecv result: %i \n", res);*/
  if (!res) res = -666;
  return res;
}
#endif /* polarssl */

#endif


static int sockRecv (TSySocket *fd, void *buf, size_t len) {
  if (len == 0) return 0;
  if (len > 65536) len = 65536;
  int rd;
#ifdef SYOPT_ALLOW_HTTPS
  if (fd->usessl) {
    const int toread = (int)(len > SSL_MAX_CONTENT_LEN ? SSL_MAX_CONTENT_LEN : len);
    #ifdef SY_USE_GNUTLS
    rd = (int)SyTCP_SSLRecv((TSySSLInfo *)fd->sslinfo, buf, toread);
    #else
    rd = (int)ssl_read(&(((TSySSLInfo *)fd->sslinfo)->ssl), buf, toread);
    if (rd == -666) rd = 0;
    if (rd < 0) {
      if (rd != POLARSSL_ERR_NET_WANT_READ) {
        //if (rd >= 0) fd->usessl = 2;
        errno = EINVAL;
      } else {
        errno = EINTR;
      }
    } else {
      rd = -1;
      errno = EINVAL;
    }
    #endif
    return rd;
  }
#endif
  for (;;) {
    rd = (int)recv(fd->fd, buf, (unsigned)len, 0);
    if (rd < 0) {
      const int err = errno;
      if (err == EINTR) continue;
    }
    break;
  }
  return rd;
}


static int sockSend (TSySocket *fd, const void *buf, size_t len) {
  if (len == 0) return 0;
  if (len > 65536) len = 65536;
  int wr;
#ifdef SYOPT_ALLOW_HTTPS
  if (fd->usessl) {
    const int tosend = (int)(len > SSL_MAX_CONTENT_LEN ? SSL_MAX_CONTENT_LEN : len);
    #ifdef SY_USE_GNUTLS
    wr = (int)SyTCP_SSLSend((TSySSLInfo *)fd->sslinfo, buf, tosend);
    #else
    wr = (int)ssl_write(&(((TSySSLInfo *)fd->sslinfo)->ssl), (void *)buf, tosend);
    if (wr == -666) wr = 0;
    if (wr < 0) {
      if (wr != POLARSSL_ERR_NET_WANT_WRITE) {
        //if (wr >= 0) fd->usessl = 2;
        errno = EINVAL;
      } else {
        //wr = -1;
        errno = EINTR;
      }
    } else if (wr == 0) {
      wr = -1;
      errno = ECONNRESET;
    } else {
      wr = -1;
      errno = EINVAL;
    }
    #endif
    return wr;
  }
#endif
  for (;;) {
    wr = (int)send(fd->fd, buf, len, SY_SOCK_MSG_NO_SIGNAL);
    if (wr < 0) {
      const int err = errno;
      if (err == EINTR) continue;
    }
    if (wr == 0) {
      wr = -1;
      errno = ECONNRESET;
    }
    break;
  }
  return wr;
}


static int SyTCPLastError (void) {
  return errno;
}


/* returns allocated string or NULL */
char *SyTCPGetLastErrorMsg (TSySocket *fd) {
  char mbuf[1024], *estr;

#ifdef SY_XYZ_TMP
#undef SY_XYZ_TMP
#endif

#ifdef _GNU_SOURCE
#define SY_XYZ_TMP
#else
#if (_POSIX_C_SOURCE >= 200112L || _XOPEN_SOURCE >= 600)
#else
#define SY_XYZ_TMP
#endif
#endif /* _GNU_SOURCE */

  if (!fd || !fd->errCode) return NULL;
  memset(mbuf, 0, sizeof(mbuf));
  estr = strerror_r(fd->errCode, mbuf, sizeof(mbuf));
#ifdef SY_XYZ_TMP
  /* GNU */
  return SyStrNew(estr, -1);
#else
  /* XSI-compliant */
  if (!estr) return SySPrintf("(%i) %s", fd->errCode, mbuf);
  return NULL;
#endif /* XSI test */

#ifdef SY_XYZ_TMP
#undef SY_XYZ_TMP
#endif
}


TSyResult SyGetIFIP (char *ip, const char *iface) {
  int res, fd;
  struct ifreq ifr;
  struct in_addr inp;
  if (!ip) return SY_ERROR;
  *ip = '\0';
  if (!iface || !(*iface)) return SY_ERROR;

  if (inet_aton(iface, &inp)) {
    strcpy(ip, inet_ntoa(inp));
    return SY_OK;
  }

  if (strlen(iface) > IFNAMSIZ) return SY_ERROR;
  fd = socket(PF_INET, SOCK_DGRAM, IPPROTO_IP);
  memset(&ifr, 0, sizeof(struct ifreq));
  strcpy(ifr.ifr_name, iface);
  ifr.ifr_addr.sa_family = AF_INET;
  res = ioctl(fd, SIOCGIFADDR, &ifr);
  SySocketClose(fd);
  if (!res) {
    struct sockaddr_in *x = (struct sockaddr_in *)&ifr.ifr_addr;
    strcpy(ip, inet_ntoa(x->sin_addr));
    return SY_OK;
  }
  return SY_ERROR;
}


TSyResult SyTCPInitSocket (TSySocket *fd) {
#ifdef SYOPT_ALLOW_HTTPS
  memset(fd, 0, sizeof(TSySocket));
  /*fd->usessl = 0;*/
#endif
  fd->errCode = 0;
  fd->fd = -1;
  return SY_OK;
}


#ifdef SYOPT_ALLOW_HTTPS
TSyResult SyTCPInitSSL (TSySocket *fd, const char *hostname, int timeout, const TSyPrintStr *pfn) {
  TSySSLInfo *si;

  fd->errCode = 0;
  if (fd->fd < 0) return SY_ERROR;
  if (fd->usessl) return SY_ERROR;

  if (!fd->sslinfo) {
    fd->sslinfo = calloc(1, sizeof(TSySSLInfo));
    if (!fd->sslinfo) return SY_ERROR;
  }
  si = fd->sslinfo;

#ifdef SY_USE_GNUTLS
  if (!gnutlsInited) {
    if (gnutls_global_init() < 0) return SY_ERROR;
    gnutlsInited = 1;
  }

  if (gnutls_certificate_allocate_credentials(&si->xcred) < 0) return SY_ERROR;

  // initialize TLS session
  if (gnutls_init(&si->session, GNUTLS_CLIENT) < 0) {
    gnutls_certificate_free_credentials(si->xcred);
    return SY_ERROR;
  }

  fd->usessl = 1; // so the library will free everything on failure

  #if 0
  const char *err = NULL;
  //if (gnutls_priority_set_direct(si->session, "PERFORMANCE", &err) < 0) return SY_ERROR;
  if (gnutls_priority_set_direct(si->session, "SECURE256", &err) < 0) return SY_ERROR;
  #else
  if (gnutls_set_default_priority(si->session) < 0) return SY_ERROR;
  gnutls_session_enable_compatibility_mode(si->session);
  #endif

  if (hostname) {
    int res = gnutls_server_name_set(si->session, GNUTLS_NAME_DNS, hostname, strlen(hostname));
    if (res < 0) {
      SyMessage(pfn, SY_MSG_ERROR, " GnuTLS error (while setting hostname '%s') %d: %s", res, hostname, gnutls_strerror(res));
      return SY_ERROR;
    }
  }

  // put the x509 credentials to the current session
  if (gnutls_credentials_set(si->session, GNUTLS_CRD_CERTIFICATE, si->xcred) < 0) return SY_ERROR;

  // pass the socket handle off to gnutls
  gnutls_transport_set_int(si->session, fd->fd);

  gnutls_handshake_set_timeout(si->session, (timeout < 0 ? GNUTLS_DEFAULT_HANDSHAKE_TIMEOUT : timeout*1000));

  // perform the TLS handshake
  for (;;) {
    int res = gnutls_handshake(si->session);
    //fprintf(stderr, "...handshacking: res=%d (%d) (to=%d)\n", res, gnutls_error_is_fatal(res), timeout);
    if (res >= 0) break;
    if (gnutls_error_is_fatal(res)) {
      SyMessage(pfn, SY_MSG_ERROR, " GnuTLS error %d: %s", res, gnutls_strerror(res));
      if (res == GNUTLS_E_WARNING_ALERT_RECEIVED || res == GNUTLS_E_FATAL_ALERT_RECEIVED) {
        gnutls_alert_description_t alt = gnutls_alert_get(si->session);
        const char *astr = gnutls_alert_get_name(alt);
        SyMessage(pfn, SY_MSG_ERROR, " GnuTLS alert: %s", astr);
      }
      return SY_ERROR;
    }
  }

  {
    char *desc = gnutls_session_get_desc(si->session);
    SyMessage(pfn, SY_MSG_NOTICE, " GnuTLS session info: %s", desc);
    gnutls_free(desc);
  }
#else
  const char *pers0 = "fuckme";
  //static const char *pers1 = "fuckhim";

  //havege_init(&(si->hs));
  memset(&(si->ssn), 0, sizeof(ssl_session));

  entropy_init(&si->ssl_entropy_cli);
  if (ctr_drbg_init(&si->ssl_ctr_drbg_cli, entropy_func, &si->ssl_entropy_cli, (const void *)pers0, strlen(pers0)) != 0) {
    //fprintf(stderr, "FUCKED: ctr_drbg_init(cli) returned %d\n", ret);
    free(fd->sslinfo);
    fd->sslinfo = NULL;
    return SY_ERROR;
  }

  if (ssl_init(&(si->ssl))) return SY_ERROR;
  /*ssl_set_debuglvl(&(si->ssl), 0);*/
  ssl_set_endpoint(&(si->ssl), SSL_IS_CLIENT);
  ssl_set_authmode(&(si->ssl), SSL_VERIFY_NONE); /*!!!*/
  ssl_set_rng(&(si->ssl), ctr_drbg_random, &(si->ssl_ctr_drbg_cli));
  ssl_set_bio(&(si->ssl), SyTCP_SSLRecv, (void *)fd, SyTCP_SSLSend, (void *)fd);
  //ssl_set_ciphers(&(si->ssl), ssl_default_ciphers);

  //ssl_set_session(&(si->ssl), 1, timeout, &(si->ssn));
  fd->usessl = 1;
#endif

  return SY_OK;
}
#endif


TSyResult SyTCPCloseSocket (TSySocket *fd) {
  if (fd->fd >= 0) {
#ifdef SYOPT_ALLOW_HTTPS
    if (fd->usessl && fd->sslinfo) {
      TSySSLInfo *si = fd->sslinfo;
      #ifdef SY_USE_GNUTLS
      if (fd->usessl) {
        gnutls_deinit(si->session);
        gnutls_certificate_free_credentials(si->xcred);
      }
      #else
      if (fd->usessl > 1) ssl_close_notify(&(si->ssl));
      ssl_free(&(si->ssl));
      memset(&(si->ssl), 0, sizeof(si->ssl));
      #endif
    }
    fd->usessl = 0;
    if (fd->sslinfo) {
      memset(fd->sslinfo, 0, sizeof(TSySSLInfo));
      free(fd->sslinfo);
      fd->sslinfo = NULL;
    }
#endif
    SySocketClose(fd->fd);
    fd->fd = -1;
  }
  return SY_OK;
}


TSyResult SyTCPConnect (TSySocket *fd, char *hostname, int port, const char *iface, int timeout, const TSyPrintStr *pfn) {
  struct hostent *host = NULL;
  struct sockaddr_in addr;
  struct sockaddr_in local;
  char ifip[64];
  int val, size;
  struct timeval tv;

  SyMessage(pfn, SY_MSG_NOTICE, "connecting to %s:%i", hostname, port);

  SyTCPInitSocket(fd);
  if (!hostname || !(*hostname)) return SY_ERROR;
  if (iface && *iface && strcmp(iface, "-")) {
    SyMessage(pfn, SY_MSG_NOTICE, " resolving local intefrace %s...\n", iface);
    if (SyGetIFIP(ifip, iface) != SY_OK) {
      SyMessage(pfn, SY_MSG_ERROR, "can't determine interface IP for %s", iface);
      return SY_ERROR;
    }
  } else *ifip = '\0';

  SyMessage(pfn, SY_MSG_NOTICE, " resolving %s", hostname);
  host = gethostbyname(hostname);
  if (!host || !host->h_name || !*host->h_name) {
    fd->errCode = SyTCPLastError();
    SyMessage(pfn, SY_MSG_ERROR, "can't resolve %s", hostname);
    return SY_ERROR;
  }

  fd->fd = socket(AF_INET, SOCK_STREAM, 0);
  if (fd->fd == -1) {
    fd->errCode = SyTCPLastError();
    SyMessage(pfn, SY_MSG_ERROR, "can't create socket");
    return SY_ERROR;
  }
  if (*ifip) {
    SyMessage(pfn, SY_MSG_NOTICE, " binding to local intefrace [%s]...\n", ifip);
    local.sin_family = AF_INET;
    local.sin_port = 0;
    local.sin_addr.s_addr = inet_addr(ifip);
    if (bind(fd->fd, (struct sockaddr *)&local, sizeof(struct sockaddr_in)) == -1) {
      fd->errCode = SyTCPLastError();
      SyTCPCloseSocket(fd);
      SyMessage(pfn, SY_MSG_ERROR, "can't bind to local interface %s", ifip);
      return SY_ERROR;
    }
  }

  addr.sin_family = AF_INET;
  addr.sin_port = htons(port);
  addr.sin_addr = *((struct in_addr *)host->h_addr);
  if (*ifip) SyMessage(pfn, SY_MSG_NOTICE, " connecting to %s:%i (%s)", inet_ntoa(addr.sin_addr), port, ifip);
  else SyMessage(pfn, SY_MSG_NOTICE, " connecting to %s:%i", inet_ntoa(addr.sin_addr), port);

  if (timeout > 0) {
    tv.tv_sec = timeout; tv.tv_usec = 0; size = sizeof(tv);
    if (setsockopt(fd->fd, SOL_SOCKET, SO_RCVTIMEO, &tv, size)) {
      SyMessage(pfn, SY_MSG_WARNING, " can't set receive timeout");
    }
    tv.tv_sec = timeout; tv.tv_usec = 0; size = sizeof(tv);
    if (setsockopt(fd->fd, SOL_SOCKET, SO_SNDTIMEO, &tv, size)) {
      SyMessage(pfn, SY_MSG_WARNING, " can't set send timeout");
    }
    val = 1; size = sizeof(val);
    setsockopt(fd->fd, SOL_SOCKET, SO_KEEPALIVE, &val, size);
  }

  if (connect(fd->fd, (struct sockaddr *)&addr, sizeof(struct sockaddr_in)) < 0) {
    fd->errCode = SyTCPLastError();
    SyTCPCloseSocket(fd);
    SyMessage(pfn, SY_MSG_ERROR, "can't connect to %s:%i", inet_ntoa(addr.sin_addr), port);
    return SY_ERROR;
  }
  SyMessage(pfn, SY_MSG_NOTICE, " connected to %s:%i", inet_ntoa(addr.sin_addr), port);

  return SY_OK;
}



TSyResult SyTCPSend (TSySocket *fd, const void *buf, int bufSize) {
  const char *c = (char *)buf;
  fd->errCode = 0;
  if (!buf) return SY_OK;
  while (bufSize > 0) {
    int wr = sockSend(fd, c, bufSize);
    if (wr <= 0) {
      fd->errCode = SyTCPLastError();
      return SY_ERROR;
    }
    c += wr;
    bufSize -= wr;
  }
  return SY_OK;
}


TSyResult SyTCPSendStr (TSySocket *fd, const char *str) {
  if (!fd || fd->fd < 0) return SY_ERROR;
  if (!str || !(*str)) return SY_OK;
  return SyTCPSend(fd, str, strlen(str));
}


/* <0: received (-res) bytes; read error */
int SyTCPReceiveEx (TSySocket *fd, void *buf, int bufSize, int allowPartial) {
  char *c = (char *)buf;
  int total = 0;
  /*fprintf(stderr, "\r*** SyTCPReceiveEx: %i (%i) \n", bufSize, allowPartial);*/
  fd->errCode = 0;
  if (fd->fd < 0) return 0;
  while (bufSize > 0) {
    int rd = sockRecv(fd, c, bufSize);
    if (rd <= 0) fd->errCode = SyTCPLastError();
    if (!rd) return total;
    if (rd < 0) return -total;
    c += rd;
    total += rd;
    bufSize -= rd;
    if (allowPartial) break;
  }
  return total;
}


int SyTCPReceive (TSySocket *fd, void *buf, int bufSize) {
  return SyTCPReceiveEx(fd, buf, bufSize, SY_TCP_DONT_ALLOWPARTIAL);
}


TSyResult SyTCPSendLine (const TSyPrintStr *pfn, int printLine, TSySocket *fd, const char *fmt, ...) {
  TSyResult res;
  int n, size = 100;
  char *p, *np;
  va_list ap;

  if ((p = calloc(1, size+4)) == NULL) { SyMessage(pfn, SY_MSG_ERROR, "memory error"); return SY_ERROR; }
  for (;;) {
    va_start(ap, fmt);
    n = vsnprintf(p, size, fmt, ap);
    va_end(ap);
    if (n > -1 && n < size) break;
    if (n > -1) size = n+1; else size *= 2;
    if ((np = realloc(p, size+4)) == NULL) {
      free(p);
      SyMessage(pfn, SY_MSG_ERROR, "memory error");
      return SY_ERROR;
    }
    p = np;
  }
  if (printLine) SyMessage(pfn, SY_MSG_NOTICE, " %s", p);
  strcat(p, "\r\n");
  res = SyTCPSendStr(fd, p);
  free(p);
  if (res != SY_OK) { SyMessage(pfn, SY_MSG_ERROR, "socket write error"); return SY_ERROR; }
  return SY_OK;
}


char *SyTCPReceiveStrEx (TSySocket *fd, int maxSize, char *dest) {
  char *d;

  fd->errCode = 0;
  if (maxSize <= 0 || fd->fd < 0 || !dest) return NULL;
  memset(dest, 0, maxSize+1);
  d = dest;
  while (maxSize) {
    int rd = sockRecv(fd, d, 1);
    if (rd <= 0) fd->errCode = SyTCPLastError();
    if (rd < 0) return NULL;
    if (rd == 0) break; /* connection closed */
    /* check for end of headers */
    if (d[0] == '\n') {
      if (d > dest && d[-1] == '\r') --d;
      *d = 0;
      return dest;
    }
    maxSize -= rd;
    d += rd;
  }
  /* headers too big */
  return NULL;
}


char *SyTCPReceiveStr (TSySocket *fd, int maxSize) {
  char *dest, *res;

  fd->errCode = 0;
  if (maxSize <= 0 || fd->fd < 0) return NULL;
  dest = calloc(1, maxSize+8);
  if (!dest) return NULL;
  res = SyTCPReceiveStrEx(fd, maxSize, dest);
  if (!res) { free(dest); return NULL; }
  return dest;
}


/* return NULL if string was too big or on error;
   tries to receive all headers
 */
char *SyTCPReceiveHdrs (TSySocket *fd, int maxSize) {
  char *dest;
  char *d;

  fd->errCode = 0;
  if (maxSize <= 0 || fd->fd < 0) return NULL;
  dest = calloc(1, maxSize+8);
  d = dest;
  while (maxSize) {
    int rd = sockRecv(fd, d, 1);
    if (rd <= 0) fd->errCode = SyTCPLastError();
    if (rd < 0) {
      free(dest);
      return NULL;
    }
    if (rd == 0) break; /* connection closed */
    if (*d == '\n') {
      const char *ck = d;
      if (ck > dest && ck[-1] == '\r') --ck;
      if (ck > dest && ck[-1] == '\n') {
        d[1] = 0;
        return dest;
      }
    }
    maxSize -= rd;
    d += rd;
  }
  /* headers too big */
  free(dest);
  return NULL;
}


#endif
