/*
  Syren -- a lightweight downloader for Linux/BSD/MacOSX
  inspired by Axel Copyright 2001-2002 Wilmer van der Gaast
  version 0.0.6 (atomic alien)
  coded by Ketmar // Vampire Avalon

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License with
  the Debian GNU/Linux distribution in file /usr/doc/copyright/GPL;
  if not, write to the Free Software Foundation, Inc., 59 Temple Place,
  Suite 330, Boston, MA  02111-1307  USA
*/
/*
  Syren proxy connections (HTTP CONNECT, SOCKS)
 */
#ifndef _SYREN_PROXY_H
#define _SYREN_PROXY_H

#ifdef __cplusplus
extern "C" {
#endif


#include "syren_os.h"
#include "syren_common.h"
#include "syren_msg.h"
#include "syren_str.h"
#include "syren_tcp.h"
#include "syren_hdrs.h"
#include "syren_http.h"


typedef enum {
  SY_PROXY_NONE=-1,
  SY_PROXY_HTTP=0,
  SY_PROXY_HTTP_CONNECT,
  SY_PROXY_SOCKS,

  SY_PROXY_MAX = SY_PROXY_SOCKS
} TSyProxyType;


typedef struct {
  TSyURL *url;
  TSyProxyType ptype;
  char *userAgent; /* for HTTP CONNECT */

  int retCode; /* for HTTP CONNECT or SOCKS */
} TSyProxy;


TSyProxy *SyProxyNew (const TSyURL *url, TSyProxyType ptype, const char *userAgent);
void SyProxyFree (TSyProxy *proxy);

TSyResult SyProxyConnect (TSySocket *fd, TSyProxy *proxy, const char *host, int port, const TSyPrintStr *pfn);


#ifdef __cplusplus
}
#endif

#endif
