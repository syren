/*
  Syren -- a lightweight downloader for Linux/BSD/MacOSX
  inspired by Axel Copyright 2001-2002 Wilmer van der Gaast
  version 0.0.6 (atomic alien)
  coded by Ketmar // Vampire Avalon

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License with
  the Debian GNU/Linux distribution in file /usr/doc/copyright/GPL;
  if not, write to the Free Software Foundation, Inc., 59 Temple Place,
  Suite 330, Boston, MA  02111-1307  USA
*/
/*
  Syren http utilities
 */
#ifndef _SYREN_HTTP_H
#define _SYREN_HTTP_H

#ifdef __cplusplus
extern "C" {
#endif


#include "syren_common.h"
#include "syren_str.h"
#include "syren_msg.h"
#include "syren_hdrs.h"
#include "syren_tcp.h"


/* read headers */
TSyResult SyHTTPReadHeaders (TSyHdrs *hdrs, TSySocket *fd, const TSyPrintStr *pfn);

/* -1: none or error */
int64_t SyHTTPGetSize (const TSyHdrs *hdrs);

TSyResult SyHTTPBuildQuery (TSyHdrs *hdrs, const char *method, const TSyURL *url, const TSyURL *proxy);
/* from or to <1: do not send this part */
TSyResult SyHTTPAddRange (TSyHdrs *hdrs, int from, int to);
TSyResult SyHTTPSendQuery (TSySocket *fd, const TSyHdrs *hdrs, const TSyPrintStr *pfn);


#ifdef __cplusplus
}
#endif

#endif
