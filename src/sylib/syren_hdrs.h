/*
  Syren -- a lightweight downloader for Linux/BSD/MacOSX
  inspired by Axel Copyright 2001-2002 Wilmer van der Gaast
  version 0.0.6 (atomic alien)
  coded by Ketmar // Vampire Avalon

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License with
  the Debian GNU/Linux distribution in file /usr/doc/copyright/GPL;
  if not, write to the Free Software Foundation, Inc., 59 Temple Place,
  Suite 330, Boston, MA  02111-1307  USA
*/
/*
  Syren HTTP headers list
 */
#ifndef _SYREN_HDRS_H
#define _SYREN_HDRS_H

#ifdef __cplusplus
extern "C" {
#endif


#include "syren_os.h"
#include "syren_common.h"
#include "syren_str.h"


typedef enum {
  SY_HDR_INVALID=-1,
  SY_HDR_REQUEST=0,
  SY_HDR_REPLY
} TSyHdrType;


typedef struct {
  TSyKVList *fields;
  TSyHdrType type;
  char *firstLine;
  int code;  /* parsed status code for HTTP reply; -1 for request */
} TSyHdrs;



TSyHdrs *SyHdrNew (void);
void SyHdrFree (TSyHdrs *hdr);
void SyHdrClear (TSyHdrs *hdr, TSyHdrType type);

/* add line, parse it, set "status" if necessary */
TSyResult SyHdrAddLine (TSyHdrs *hdr, const char *line);

char *SyHdrGetFieldValue (const TSyHdrs *hdr, const char *name);
TSyBool SyHdrHasField (const TSyHdrs *hdr, const char *name);
TSyResult SyHdrDeleteField (TSyHdrs *hdr, const char *name);
/* set field; correctly processes "Set-Cookie" and "Cookie" */
TSyResult SyHdrSetFieldValue (TSyHdrs *hdr, const char *name, const char *value);

TSyKVList *SyHdrFindCookieList (const TSyHdrs *hdr);

/* return NULL or new string */
char *SyHdrGetCookie (const TSyHdrs *hdr, const char *name);
void SyHdrDeleteCookie (TSyHdrs *hdr, const char *name);

TSyResult SyHdrSetCookie (TSyHdrs *hdr, const char *value);


#ifdef __cplusplus
}
#endif

#endif
