/*
  Syren -- a lightweight downloader for Linux/BSD/MacOSX
  inspired by Axel Copyright 2001-2002 Wilmer van der Gaast
  version 0.0.6 (atomic alien)
  coded by Ketmar // Vampire Avalon

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License with
  the Debian GNU/Linux distribution in file /usr/doc/copyright/GPL;
  if not, write to the Free Software Foundation, Inc., 59 Temple Place,
  Suite 330, Boston, MA  02111-1307  USA
*/
/*
  Syren UI message service
 */
#ifndef _SYREN_MSG_H
#define _SYREN_MSG_H

#ifdef __cplusplus
extern "C" {
#endif


#include "syren_str.h"


typedef enum {
  SY_MSG_NOTICE=0,
  SY_MSG_MSG,
  SY_MSG_WARNING,
  SY_MSG_ERROR
} TSyMsgType;


/* message printer; message will not end with '\n', but it can contain '\n's */
typedef void (*PSyFnPrintStr) (void *udata, TSyMsgType msgtype, const char *msg);


typedef struct {
  PSyFnPrintStr print;
  void *udata; /* TSyState* */
} TSyPrintStr;


void SyMessage (const TSyPrintStr *pfn, TSyMsgType msgtype, const char *fmt, ...);


#ifdef __cplusplus
}
#endif

#endif
