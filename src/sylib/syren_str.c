/*
  Syren -- a lightweight downloader for Linux/BSD/MacOSX
  inspired by Axel Copyright 2001-2002 Wilmer van der Gaast
  version 0.0.6 (atomic alien)
  coded by Ketmar // Vampire Avalon

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License with
  the Debian GNU/Linux distribution in file /usr/doc/copyright/GPL;
  if not, write to the Free Software Foundation, Inc., 59 Temple Place,
  Suite 330, Boston, MA  02111-1307  USA
*/
/*
  Syren string manipulation package
 */
#ifndef _SYREN_STR_C
#define _SYREN_STR_C

#include "syren_str.h"


#define chAlloc(size)  malloc((size)*sizeof(char))
#define chCAlloc(n, size)  calloc(n, (size)*sizeof(char))


char *SySPrintf (const char *fmt, ...) {
  int n, size = 256;
  va_list ap;
  char *p, *np;
  int addonBytes = 8;

  /*if (addonBytes < 0) addonBytes = 0; addonBytes +=8;*/
  if ((p = chAlloc(size+addonBytes)) == NULL) return NULL;
  while (1) {
    memset(p, 0, size+addonBytes);
    va_start(ap, fmt);
    n = vsnprintf(p, size, fmt?fmt:"", ap);
    va_end(ap);
    if (n > -1 && n < size) break;
    if (n > -1) size = n+1; else size *= 2;
    if ((np = realloc(p, size+addonBytes)) == NULL) { free(p); return NULL; }
    p = np;
  }
  return p;
}


/* src can be NULL; maxlen < 0: maxlen = strlen(src?src:"") */
char *SyStrNew (const char *src, int maxlen) {
  char *res;
  if (maxlen < 0) { if (src) maxlen = strlen(src); else maxlen = 0; }
  res = chAlloc(maxlen+1); if (!res) return NULL;
  memset(res, 0, maxlen+1);
  if (src && maxlen) strncpy(res, src, maxlen);
  return res;
}


char *SyStrAlloc (int len) {
  if (len < 0) return NULL;
  return (char *)chAlloc(len+1);
}


void SyStrFree (char *str) {
  if (str) free(str);
}


/* key/value lists */
TSyKVList *SyKVListNew () {
  return calloc(1, sizeof(TSyKVList));
}


void SyKVListFree (TSyKVList *lst) {
  if (lst) { SyKVListClear(lst); free(lst); }
}


void SyKVListClearItem (TSyKVListItem *item) {
  if (!item) return;
  SyStrFree(item->key); SyStrFree(item->value); SyStrFree(item->ustr);
  memset(item, 0, sizeof(TSyKVListItem));
}


void SyKVListFreeItem (TSyKVListItem *item) {
  if (!item) return;
  SyStrFree(item->key); SyStrFree(item->value); SyStrFree(item->ustr);
  free(item);
}


void SyKVListClear (TSyKVList *lst) {
  TSyKVListItem *item, *next;

  if (!lst) return;
  item = lst->first;
  while (item) {
    next = item->next;
    SyKVListFreeItem(item);
    item = next;
  }
  lst->first = lst->last = NULL;
  lst->count = 0;
}


static TSyKVListItem *SyKVListFindInternal (const TSyKVList *lst, const char *key, TSyKVListItem **prev) {
  TSyKVListItem *item;

  if (prev) *prev = NULL;
  if (!lst || !key) return NULL;
  item = lst->first;
  while (item) {
    if ((lst->casesens && !strcmp(item->key, key)) || !strcasecmp(item->key, key)) break;
    if (prev) *prev = item;
    item = item->next;
  }
  return item;
}


TSyKVListItem *SyKVListFind (const TSyKVList *lst, const char *key) {
  return SyKVListFindInternal(lst, key, NULL);
}


TSyResult SyKVListDelete (TSyKVList *lst, const char *key) {
  TSyKVListItem *item, *prev;

  item = SyKVListFindInternal(lst, key, &prev);
  if (!item) return SY_ERROR;
  if (prev) prev->next = item->next; else lst->first = item->next;
  if (!item->next) lst->last = prev;
  lst->count--;
  SyKVListFreeItem(item);

  return SY_TRUE;
}


TSyKVListItem *SyKVListSet (TSyKVList *lst, const char *key, const char *value, int *newKey) {
  TSyKVListItem *item;
  char *sk, *sv;

  if (newKey) *newKey = 0;
  if (!lst || !key) return NULL;
  sk = SyStrDup(key); if (!sk) return NULL;
  sv = SyStrDup(value); if (!sv) { SyStrFree(sk); return NULL; }
  item = SyKVListFind(lst, key);
  if (item) { free(sk); SyStrFree(item->value); }
  else {
    if (newKey) *newKey = 1;
    item = calloc(1, sizeof(TSyKVListItem));
    if (!item) { free(sk); free(sv); return NULL; }
    item->key = sk;
    if (lst->count) lst->last->next = item; else lst->first = item;
    lst->last = item; lst->count++;
  }
  item->value = sv;
  return item;
}


/* trim [dynamic] string "in-place" */
void SyStrTrim (char *s) {
  int f = 0, p = 0;

  while (s[f] && (unsigned char)(s[f]) <= ' ') f++;
  if (f) { while (s[f]) s[p++] = s[f++]; s[p] = '\0'; }
  f = strlen(s);
  while (f >= 0 && (unsigned char)(s[f]) <= ' ') f--;
  s[f+1] = '\0';
}


/* return NULL or new string */
char *SyURLDecode (const char *s) {
  char *d, *dest;
  int destSize;

  if (!s) return SyStrNewEmpty();
  destSize = strlen(s)+1;
  dest = chCAlloc(1, destSize); if (!dest) return NULL;
  d = dest;
  while (*s) {
    char ch = *(s++);
    if (ch == '%' && *s) {
      char h0 = *s, h1 = s[1];
      if (isxdigit(h0) && isxdigit(h1)) {
        if (h0 >= 'a' && h0 <= 'f') h0 -= 32;
        if (h1 >= 'a' && h1 <= 'f') h1 -= 32;
        if (h0 > '9') h0 -= 7; if (h1 > '9') h1 -= 7;
        h0 -= '0'; h1 -= '0';
        ch = ((int)h0*16)+((int)h1); s += 2;
      }
    }
    *(d++) = ch;
  }
  return dest;
}


/* return NULL or new string */
char *SyURLEncode (const char *s) {
  char *d, *dest;
  int len;

  if (!s || !(*s)) return SyStrNewEmpty();
  len = strlen(s);
  dest = chAlloc(len*3+1); /* max size */
  if (!dest) { free(dest); return SyStrNewEmpty(); }
  memset(dest, 0, len*3+1); d = dest;
  while (*s) {
    char ch = *(s++);
    if (ch <= ' ' || ch >= '\x7F' || ch == '%') { sprintf(d, "%%%02X", (int)((unsigned char)ch)); d += 3; }
    else *(d++) = ch;
  }
  return dest;
}


/* return NULL or new string */
char *SyBuildAuthStr (const char *user, const char *pass) {
  static const char b64Alph[64] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
  int f;
  char *auth, *dest;

  if ((!user || !(*user)) && (!pass || !(*pass))) return SyStrNewEmpty();
  f = (user?strlen(user):0)+(pass?strlen(pass):0)+16;
  auth = chCAlloc(1, f); if (!auth) return NULL;
  snprintf(auth, f-1, "%s:%s", user?user:"", pass?pass:"");
  f = (strlen(auth)+2)/3*4+1; /* length of encoded string+EOS */
  dest = chAlloc(f); if (!dest) { free(auth); return NULL; }
  memset(dest, 0, f);
  for (f = 0; auth[f*3]; f++) {
    dest[f*4+0] = b64Alph[(auth[f*3]>>2)];
    dest[f*4+1] = b64Alph[((auth[f*3]&3)<<4)|(auth[f*3+1]>>4)];
    dest[f*4+2] = b64Alph[((auth[f*3+1]&15)<<2)|(auth[f*3+2]>>6)];
    dest[f*4+3] = b64Alph[auth[f*3+2]&63];
    if (!auth[f*3+2]) dest[f*4+3] = '=';
    if (!auth[f*3+1]) dest[f*4+2] = '=';
  }
  free(auth);
  return dest;
}


TSyURL *SyURLNew (void) {
  TSyURL *url = calloc(1, sizeof(TSyURL));
  if (url) url->proto = SY_PROTO_UNKNOWN;
  return url;
}


void SyURLFree (TSyURL *url) {
  if (url) { SyURLClear(url); free(url); }
}


void SyURLClear (TSyURL *url) {
  if (!url) return;
  SyStrFree(url->protostr);
  SyStrFree(url->user);
  SyStrFree(url->pass);
  SyStrFree(url->host);
  SyStrFree(url->dir);
  SyStrFree(url->file);
  SyStrFree(url->query);
  SyStrFree(url->anchor);
  memset(url, 0, sizeof(TSyURL));
  url->proto = SY_PROTO_UNKNOWN;
}


TSyURL *SyURLClone (const TSyURL *src) {
  TSyURL *res;

  if (!src) return NULL;
  res = SyURLNew();
  if (!res) return NULL;

  res->proto = src->proto;
  res->port = src->port;
  res->defaultPort = src->defaultPort;
  do {
    res->protostr = SyStrDup(src->protostr); if (!res->protostr) break;
    res->user = SyStrDup(src->user); if (!res->user) break;
    res->pass = SyStrDup(src->pass); if (!res->pass) break;
    res->host = SyStrDup(src->host); if (!res->host) break;
    res->dir = SyStrDup(src->dir); if (!res->dir) break;
    res->file = SyStrDup(src->file); if (!res->file) break;
    res->query = SyStrDup(src->query); if (!res->query) break;
    res->anchor = SyStrDup(src->anchor); if (!res->anchor) break;
    return res;
  } while (0);

  SyURLFree(res);
  return NULL;
}


TSyResult SyURLParse (TSyURL *dest, const char *urlstr) {
  char *url, *buf;
  char *s, *t, *q, *dn, ch;
  /* */
  if (dest == NULL) return SY_ERROR;
  SyURLClear(dest);
  if (!urlstr || !urlstr[0]) return SY_ERROR;
  if ((buf = SyStrNew(urlstr, -1)) == NULL) return SY_ERROR;
  url = buf;
  SyStrTrim(url);
  if (!url[0]) { free(buf); return SY_ERROR; }
  /* protocol:// */
  s = strstr(url, "://");
  if (s != NULL) {
    /* there should be only alhas before "://" */
    for (const char *t = url; t < s && s != NULL; ++t) if (!isalpha(*t)) s = NULL;
  }
  dest->port = 80;
  dest->defaultPort = SY_TRUE; /* for now*/
  if (s == NULL) {
    /* no protocol specified */
    dest->proto = SY_PROTO_DEFAULT;
    dest->protostr = SyStrNew(SY_PROTO_DEFAULT_STR, -1);
  } else {
    *s = '\0'; /* for easy comparisons, etc */
    dest->protostr = SyStrNew(url, -1); /* copy proto string */
    if (strcasecmp(url, "ftp") == 0) { dest->proto = SY_PROTO_FTP; dest->port = 21; }
    else if (strcasecmp(url, "http") == 0) { dest->proto = SY_PROTO_HTTP; dest->port = 80; }
    else if (strcasecmp(url, "https") == 0) { dest->proto = SY_PROTO_HTTPS; dest->port = 443; }
    else dest->proto = SY_PROTO_UNKNOWN;
    url = s+3; /* skip proto definition */
  }
  /* split to host and dir/name */
  dn = strchr(url, '/');
  if (dn != NULL) *dn = '\0'; /* cut path for now (will be restored later) */
  /* check for username in host field */
  if ((t = strrchr(url, '@')) != NULL) {
    s = url;
    *t = '\0';
    url = t+1; /* what's left is host[:port] */
    /* user:password? */
    t = strchr(s, ':');
    if (t) *(t++) = '\0';
    dest->pass = SyStrNew(t, -1);
    dest->user = SyStrNew(s, -1);
  } else {
    /* no user/password */
    dest->user = SyStrNew((dest->proto == SY_PROTO_FTP ? "anonymous" : NULL), -1);
    dest->pass = SyStrNew((dest->proto == SY_PROTO_FTP ? "syren@nowhere.net" : NULL), -1);
  }
  /* port number? */
  if ((s = strchr(url, ':')) != NULL) {
    int f = 0;
    *(s++) = '\0';
    while (*s) {
      char ch = *(s++);
      if (ch < '0' || ch > '9') { free(buf); SyURLClear(dest); return SY_ERROR; }
      f = (f*10)+(ch-'0');
      if (f > 65535) { free(buf); SyURLClear(dest); return SY_ERROR; }
    }
    if (f < 0) { free(buf); SyURLClear(dest); return SY_ERROR; }
    if (f > 0) { dest->port = f; dest->defaultPort = SY_FALSE; }
  }
  /* save host */
  if (!url[0]) dest->host = SyStrNewEmpty(); else dest->host = SyStrNew(url, -1);
  /* */
  if (dn == NULL) {
    if (dest->proto == SY_PROTO_FTP) { free(buf); SyURLClear(dest); return SY_ERROR; }
    dest->dir = SyStrNew("/", -1);
    dest->file = SyStrNewEmpty();
    dest->query = SyStrNewEmpty();
    dest->anchor = SyStrNewEmpty();
  } else {
    if (dest->proto == SY_PROTO_FTP) {
      /* FTP, no queries/anchors */
      /* split to dir and file */
      *dn = '/';
      t = strrchr(dn, '/')+1;
      dest->dir = SyStrNew(dn, t-dn);
      dest->file = SyStrNew(t, -1);
      dest->query = SyStrNewEmpty();
      dest->anchor = SyStrNewEmpty();
    } else {
      /* HTTP, possible queries/anchors */
      *dn = '/';
      /* split to dir and file */
      s = dn; t = dn+1; q = NULL;
      while (*s) {
        char ch = *(s++);
        if (ch == '?' || ch == '#') { q = s-1; break; }
        if (ch == '/') t = s;
      }
      dest->dir = SyStrNew(dn, t-dn);
      if (q) {
        ch = *q; *q = '\0'; dest->file = SyStrNew(t, -1); *q = ch;
        if (*q == '?') {
          s = strchr(q, '#'); if (s) *s = '\0';
          dest->query = SyStrNew(q, -1);
          if (s) { *s = '#'; q = s; } else q = NULL;
        } else dest->query = SyStrNewEmpty();
        dest->anchor = SyStrNew(q, -1);
      } else {
        dest->file = SyStrNew(t, -1);
        dest->query = SyStrNewEmpty();
        dest->anchor = SyStrNewEmpty();
      }
    }
  }
  free(buf);
  return SY_OK;
}


#ifdef SY_STR_URL2STR
/* return NULL or new string */
char *SyURL2StrEx (const TSyURL *url, TSyBool userpass, TSyBool hidepass) {
  char *s, *t;

  if (!url) return NULL;
  if (userpass == SY_TRUE && ((url->user && *url->user) || (url->pass && *url->pass))) {
    if (hidepass == SY_TRUE) t = SySPrintf("%s:********@", url->user?url->user:"");
    else t = SySPrintf("%s:%s@", url->user?url->user:"", url->pass?url->pass:"");
  } else t = SyStrNewEmpty();
  if (!t) return NULL;
  if (url->protostr[0] == 'h' && url->port == 80)
    s = SySPrintf("%s://%s%s%s%s%s%s",  url->protostr, t, url->host, url->dir, url->file, url->query, url->anchor);
  else
    s = SySPrintf("%s://%s%s:%i%s%s%s%s",  url->protostr, t, url->host, url->port, url->dir, url->file, url->query, url->anchor);
  free(t);
  return s;
}


/* return NULL or new string */
char *SyURL2Str (const TSyURL *url) {
  return SyURL2StrEx(url, SY_TRUE, SY_TRUE);
}
#endif


/* -1: error */
int64_t SyStr2LongEx (const char *s, int *ep) {
  int64_t n = 0;

  if (!s) return -1;
  const char *sp = s;
  while (*s && (unsigned char)(*s) <= ' ') s++;
  if (!isdigit(*s)) return -1;
  while (isdigit(*s)) {
    n *= 10; if (n < 0) return -1;
    n += (*(s++))-'0';
  }
  if (ep) *ep = (s-sp);
  return n;
}


int64_t SyStr2Long (const char *s) {
  int64_t n = 0;

  if (!s) return -1;
  while (*s && (unsigned char)(*s) <= ' ') s++;
  if (!isdigit(*s)) return -1;
  while (isdigit(*s)) {
    n *= 10; if (n < 0) return -1;
    n += (*(s++))-'0';
  }
  while (*s && (unsigned char)(*s) <= ' ') s++;
  if (*s) return -1;
  return n;
}


int64_t SyHexStr2Long (const char *s) {
  int64_t n = 0;

  if (!s) return -1;
  while (*s && (unsigned char)(*s) <= ' ') s++;
  if (!isxdigit(*s)) return -1;
  while (isxdigit(*s)) {
    char ch = (*(s++));
    if (ch >= 'a' && ch <= 'f') ch -= 32; /* to lower case */
    ch -= '0'; if (ch > 9) ch -= 7; /* hex convert */
    n *= 16; if (n < 0) return -1;
    n += ch;
  }
  while (*s && (unsigned char)(*s) <= ' ') s++;
  if (*s) return -1;
  return n;
}


/* -1: error */
int SyStr2Int (const char *s) {
  int64_t res = SyStr2Long(s);

  if (res > 0x7fffffff) res = -1;
  return res;
}


TSyResult SyLong2Str (char *dest, int64_t num) {
  char tmp[64];
  int pp;

  /* fuck msvcrt! */
  if (!dest) return SY_ERROR;
  *dest = '\0';
  if (num < 0) return SY_ERROR;
  pp = sizeof(tmp)/sizeof(char); tmp[--pp] = '\0';
  do {
    if (!pp) return SY_ERROR;
    tmp[--pp] = (num%10)+'0'; num /= 10;
  } while (num);
  strcpy(dest, &(tmp[pp]));
  return SY_OK;
}


#ifdef SY_STR_ADDON
/* dest: at least 26 chars */
/* returns len */
int SyLong2StrComma (char *dest, int64_t num) {
  char tmp[64];
  int len, ccnt, t, res, pp;

  if (!dest) return 0;
  *dest = '\0';
  /* fuck msvcrt! */
  /*ccnt = snprintf(tmp, 24, "%lli", num);
  if (ccnt < 0 || ccnt > 23) return 0;*/ /* the thing that should not be */
  if (num < 0) { strcpy(dest, "---"); return strlen(dest); }
  pp = sizeof(tmp)/sizeof(char); tmp[--pp] = '\0';
  do {
    if (!pp) { strcpy(dest, "BAD"); return strlen(dest); }
    tmp[--pp] = (num%10)+'0'; num /= 10;
  } while (num);
  len = strlen(&(tmp[pp])); ccnt = (len-1)/3;
  res = len+ccnt;
  dest += res; *(dest--) = '\0';
  t = 3; while (len) {
    len--; *(dest--) = tmp[pp+len];
    if (len && !--t) { t = 3; *(dest--) = ','; }
  }
  return res;
}


/* dest: at least 10 bytes */
void SySize2Str (char *dest, int64_t size) {
  if (size < (int64_t)0) { strcpy(dest, "--------"); return; }
  if (size < (int64_t)1024L) { sprintf(dest, "%7iB", (int)size); return; }
  if (size < (int64_t)1024*1024L) { sprintf(dest, "%6.1fKB", (double)size/1024); return; }
  if (size < (int64_t)1024*1024*1024L) { sprintf(dest, "%6.1fMB", (double)size/(1024*1024)); return; }
  if (size < (int64_t)1024*1024*1024*1024L) { sprintf(dest, "%6.1fGB", (double)size/(double)(1024*1024*1024)); return; }
  sprintf(dest, "%6.1fTB", (double)size/(double)(1024*1024)/(double)(1024*1024));
  return;
}


/* dest: at least 10 bytes */
void SyTime2Str (char *dest, int value) {
  int h;

  if (value < 0) { strcpy(dest, "--:--"); return; }
  if (value < 3600) { sprintf(dest, "%02i:%02i", value/60, value%60); return; }
  h = value/3600; if (h > 999) h = 999;
  sprintf(dest, "%i:%02i:%02i", value/3600, (value/60)%60, value%60);
  return;
}
#endif


#endif
