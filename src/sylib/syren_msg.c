/*
  Syren -- a lightweight downloader for Linux/BSD/MacOSX
  inspired by Axel Copyright 2001-2002 Wilmer van der Gaast
  version 0.0.6 (atomic alien)
  coded by Ketmar // Vampire Avalon

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License with
  the Debian GNU/Linux distribution in file /usr/doc/copyright/GPL;
  if not, write to the Free Software Foundation, Inc., 59 Temple Place,
  Suite 330, Boston, MA  02111-1307  USA
*/
/*
  Syren UI message service
 */
#ifndef _SYREN_MSG_C
#define _SYREN_MSG_C

#include "syren_msg.h"


void SyMessage (const TSyPrintStr *pfn, TSyMsgType msgtype, const char *fmt, ...) {
  int n, size = 256;
  va_list ap;
  char *p, *np;

  if (!pfn || !pfn->print) return;
  if ((p = malloc(size)) == NULL) return;
  while (1) {
    memset(p, 0, size);
    va_start(ap, fmt);
    n = vsnprintf(p, size, fmt?fmt:"", ap);
    va_end(ap);
    if (n > -1 && n < size) break;
    if (n > -1) size = n+1; else size *= 2;
    if ((np = realloc(p, size)) == NULL) { free(p); return; }
    p = np;
  }
  np = p+strlen(p)-1;
  while (np >= p && (unsigned char)(*np) <= ' ') np--;
  *(np+1) = '\0';
  pfn->print(pfn->udata, msgtype, p);
  free(p);
}


#endif
