/*
  Syren -- a lightweight downloader for Linux/BSD/MacOSX
  inspired by Axel Copyright 2001-2002 Wilmer van der Gaast
  version 0.0.6 (atomic alien)
  coded by Ketmar // Vampire Avalon

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License with
  the Debian GNU/Linux distribution in file /usr/doc/copyright/GPL;
  if not, write to the Free Software Foundation, Inc., 59 Temple Place,
  Suite 330, Boston, MA  02111-1307  USA
*/
/*
  Syren tcp utilities
 */
#ifndef _SYREN_TCP_H
#define _SYREN_TCP_H

#ifdef __cplusplus
extern "C" {
#endif


#include "syren_os.h"
#include "syren_common.h"
#include "syren_msg.h"


typedef struct {
#ifdef SYOPT_ALLOW_HTTPS
  int usessl;
  void *sslinfo;
#endif
  int fd;
  int errCode; /* 0: no error */
} TSySocket;


#define SY_TCP_DONT_ALLOWPARTIAL  0
#define SY_TCP_ALLOWPARTIAL       1


/* returns allocated string or NULL */
char *SyTCPGetLastErrorMsg (TSySocket *fd);

/* put interface IP address to `ip' */
TSyResult SyGetIFIP (char *ip, const char *iface);

/* initialize socket */
TSyResult SyTCPInitSocket (TSySocket *fd);
#ifdef SYOPT_ALLOW_HTTPS
/* timeout in seconds */
TSyResult SyTCPInitSSL (TSySocket *fd, const char *hostname, int timeout, const TSyPrintStr *pfn);
#endif
TSyResult SyTCPCloseSocket (TSySocket *fd);

/* create a TCP connection; fd must not be initialized; timeout in seconds */
TSyResult SyTCPConnect (TSySocket *fd, char *hostname, int port, const char *iface, int timeout, const TSyPrintStr *pfn);
TSyResult SyTCPSend (TSySocket *fd, const void *buf, int bufSize);
TSyResult SyTCPSendStr (TSySocket *fd, const char *str);
/* <0: received (-res) bytes; read error */
int SyTCPReceiveEx (TSySocket *fd, void *buf, int bufSize, int allowPartial);
/* <0: received (-res) bytes; read error */
int SyTCPReceive (TSySocket *fd, void *buf, int bufSize);

char *SyTCPReceiveStrEx (TSySocket *fd, int maxSize, char *dest);
/* return NULL if string was too big */
char *SyTCPReceiveStr (TSySocket *fd, int maxSize);

TSyResult SyTCPSendLine (const TSyPrintStr *pfn, int printLine, TSySocket *fd, const char *fmt, ...);

/* return NULL if string was too big or on error;
   tries to receive all headers
 */
char *SyTCPReceiveHdrs (TSySocket *fd, int maxSize);


#ifdef __cplusplus
}
#endif

#endif
