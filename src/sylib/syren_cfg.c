/*
  Syren -- a lightweight downloader for Linux/BSD/MacOSX
  inspired by Axel Copyright 2001-2002 Wilmer van der Gaast
  version 0.0.6 (atomic alien)
  coded by Ketmar // Vampire Avalon

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License with
  the Debian GNU/Linux distribution in file /usr/doc/copyright/GPL;
  if not, write to the Free Software Foundation, Inc., 59 Temple Place,
  Suite 330, Boston, MA  02111-1307  USA
*/
/*
  Syren config engine
 */
#ifndef _SYREN_CFG_C
#define _SYREN_CFG_C

#include "syren_cfg.h"


TSyCfg *SyCfgNew (void) {
  TSyCfg *cfg = calloc(1, sizeof(TSyCfg));
  if (!cfg) return NULL;
  cfg->opts = SyKVListNew();
  if (!cfg->opts) { free(cfg); return NULL; }
  return cfg;
}


void SyCfgFree (TSyCfg *cfg) {
  if (cfg) { SyKVListFree(cfg->opts); free(cfg); }
}


void SyCfgClear (TSyCfg *cfg) {
  SyKVListClear(cfg->opts);
}


static TSyKVListItem *SyCfgAddKey (TSyCfg *cfg, int ktype, const char *kname, const char *kdescr, void *var) {
  TSyKVListItem *item;
  char *s;

  if (!cfg || !cfg->opts) return NULL;
  item = SyKVListFind(cfg->opts, kname); if (item) return NULL;
  s = SyStrDup(kdescr); if (!s) return NULL;
  item = SyKVListSet(cfg->opts, kname, NULL, NULL);
  if (!item) { SyStrFree(s); return NULL; }
  item->ustr = s;
  item->uidata = ktype;
  item->udata = var;
  return item;
}


TSyResult SyCfgAddStrKey (TSyCfg *cfg, const char *kname, const char *kdescr, char **var) {
  TSyKVListItem *item;

  if (!cfg || !cfg->opts || !var) return SY_ERROR;
  item = SyCfgAddKey(cfg, SY_CI_STRING, kname, kdescr, var);
  if (!item) return SY_ERROR;
  return SY_OK;
}


TSyResult SyCfgAddBoolKey (TSyCfg *cfg, const char *kname, const char *kdescr, TSyBool *var) {
  TSyKVListItem *item;

  if (!cfg || !cfg->opts || !(var)) return SY_ERROR;
  item = SyCfgAddKey(cfg, SY_CI_BOOL, kname, kdescr, var);
  if (!item) return SY_ERROR;
  return SY_OK;
}


TSyResult SyCfgAddIntKey (TSyCfg *cfg, const char *kname, const char *kdescr, int *var) {
  TSyKVListItem *item;

  if (!cfg || !cfg->opts || !var) return SY_ERROR;
  item = SyCfgAddKey(cfg, SY_CI_INT, kname, kdescr, var);
  if (!item) return SY_ERROR;
  return SY_OK;
}


int SyCfgGetKeyType (const TSyCfg *cfg, const char *kname) {
  TSyKVListItem *item;

  if (!cfg || !cfg->opts) return SY_CI_ERROR;
  item = SyKVListFind(cfg->opts, kname);
  if (!item) return SY_CI_ERROR;
  return item->uidata;
}


char *SyCfgGetKeyValue (const TSyCfg *cfg, const char *kname) {
  TSyKVListItem *item;

  item = SyKVListFind(cfg->opts, kname);
  if (!item) return NULL;
  switch (item->uidata) {
    case SY_CI_STRING:
      return SyStrNew(*((char **)(item->udata)), -1);
      break;
    case SY_CI_INT:
      return SySPrintf("%i", *((int *)(item->udata)));
      break;
    case SY_CI_BOOL:
      return (*((TSyBool *)(item->udata))!=SY_FALSE)?SyStrNew("tan", -1):SyStrNew("ona", -1);
      break;
    default: ;
  }
  return NULL;
}


TSyResult SyCfgSetKeyValue (TSyCfg *cfg, const char *kname, const char *value) {
  int n;
  char *s, **v;
  int *i; TSyBool *b;
  TSyKVListItem *item;

  item = SyKVListFind(cfg->opts, kname);
  if (!item) return SY_ERROR;
  if (item->uidata == SY_CI_STRING) {
      s = SyStrNew(value, -1); if (!s) return SY_ERROR;
      v = (char **)item->udata;
      if (*v) free(*v);
      *v = s;
  } else {
    n = SyStr2Int(value);
    if (item->uidata == SY_CI_INT) {
      if (n < 0) return SY_ERROR;
      i = (int *)item->udata; *i = n;
    } else { /* SY_CI_BOOL */
      b = (TSyBool *)item->udata;
      if (n >= 0) *b = n?SY_TRUE:SY_FALSE;
      else {
        s = SyStrNew(value, -1); if (!s) return SY_ERROR;
        SyStrTrim(s);
        *b = (*s && strchr("tTyYsS", *s))?SY_TRUE:SY_FALSE;
        SyStrFree(s);
      }
    }
  }
  return SY_OK;
}


TSyResult SyCfgSet (TSyCfg *cfg, const char *optline) {
  char *s, *buf, *c, ch;
  TSyKVListItem *item;

  if (!cfg) return SY_ERROR;
  if (!optline || !optline[0]) return SY_OK;
  s = SyStrNew(optline, -1); if (!s) return SY_ERROR;
  buf = s; SyStrTrim(s); c = s;
  while ((unsigned char)*c > ' ' && *c != '=') c++;
  ch = *c; *c = '\0';
  item = SyKVListFind(cfg->opts, s);
  if (item) {
    if (ch) {
      c++; while (*c && (unsigned char)*c <= ' ') c++;
      if (*c == '=') { c++; while (*c && (unsigned char)*c <= ' ') c++; }
    }
    SyCfgSetKeyValue(cfg, s, c);
    free(buf);
    return SY_OK;
  } else { free(buf); return SY_ERROR; }
}


TSyResult SyCfgLoadFile (TSyCfg *cfg, FILE *fl, const TSyPrintStr *pfn) {
  char s[1024];
  int lineno = 0;
  //
  if (cfg == NULL || cfg->opts == NULL || fl == NULL) return SY_ERROR;
  while (fgets(s, sizeof(s), fl)) {
    ++lineno;
    SyStrTrim(s);
    if (!s[0] || *s == '#' || *s == ';') continue;
    if (SyCfgSet(cfg, s) != SY_OK) SyMessage(pfn, SY_MSG_WARNING, "unknown config option at line %i: %s", lineno, s);
  }
  return SY_OK;
}


TSyResult SyCfgSaveFile (const TSyCfg *cfg, FILE *fl, const TSyPrintStr *pfn) {
  //
  if (cfg == NULL || cfg->opts == NULL || fl == NULL) return SY_ERROR;
  fprintf(fl, "%s\n", "# configuration file for Syren -- a lightweight downloader for GNU/Linux");
  for (const TSyKVListItem *item = cfg->opts->first; item != NULL; item = item->next) {
    char *s = SyCfgGetKeyValue(cfg, item->key);
    //
    if (s != NULL) { fprintf(fl, "\n# %s\n%s = %s\n", item->ustr, item->key, s); SyStrFree(s); }
  }
  return SY_OK;
}


TSyResult SyCfgLoad (TSyCfg *cfg, const char *filename, const TSyPrintStr *pfn) {
  FILE *fl;
  TSyResult res;
  //
  if (cfg == NULL || cfg->opts == NULL || filename == NULL || !filename[0]) return SY_ERROR;
  if ((fl = fopen(filename, "r")) == NULL) return SY_ERROR;
  res = SyCfgLoadFile(cfg, fl, pfn);
  fclose(fl);
  return res;
}


TSyResult SyCfgSave (const TSyCfg *cfg, const char *filename, const TSyPrintStr *pfn) {
  FILE *fl;
  TSyResult res;
  //
  if (cfg == NULL || cfg->opts == NULL || filename == NULL || !filename[0]) return SY_ERROR;
  if ((fl = fopen(filename, "w")) == NULL) return SY_ERROR;
  res = SyCfgSaveFile(cfg, fl, pfn);
  fclose(fl);
  return res;
}


#endif
