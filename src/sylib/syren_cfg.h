/*
  Syren -- a lightweight downloader for Linux/BSD/MacOSX
  inspired by Axel Copyright 2001-2002 Wilmer van der Gaast
  version 0.0.6 (atomic alien)
  coded by Ketmar // Vampire Avalon

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License with
  the Debian GNU/Linux distribution in file /usr/doc/copyright/GPL;
  if not, write to the Free Software Foundation, Inc., 59 Temple Place,
  Suite 330, Boston, MA  02111-1307  USA
*/
/*
  Syren config engine
 */
#ifndef _SYREN_CFG_H
#define _SYREN_CFG_H

#ifdef __cplusplus
extern "C" {
#endif

#ifndef SY_DNT
# define SY_DNT
#endif

#include "syren_common.h"
#include "syren_str.h"
#include "syren_msg.h"


#define SY_CI_ERROR  -1
#define SY_CI_STRING 0
#define SY_CI_INT    1  /* only positive */
#define SY_CI_BOOL   2


typedef struct {
  TSyKVList *opts;
} TSyCfg;



TSyCfg *SyCfgNew (void);
void SyCfgFree (TSyCfg *cfg);
void SyCfgClear (TSyCfg *cfg);

TSyResult SyCfgAddStrKey (TSyCfg *cfg, const char *kname, const char *kdescr, char **var);
TSyResult SyCfgAddBoolKey (TSyCfg *cfg, const char *kname, const char *kdescr, TSyBool *var);
TSyResult SyCfgAddIntKey (TSyCfg *cfg, const char *kname, const char *kdescr, int *var);

int SyCfgGetKeyType (const TSyCfg *cfg, const char *kname);

/* converts any key value to string */
char *SyCfgGetKeyValue (const TSyCfg *cfg, const char *kname);
/* converts string to any key value */
TSyResult SyCfgSetKeyValue (TSyCfg *cfg, const char *kname, const char *value);

TSyResult SyCfgSet (TSyCfg *cfg, const char *optline);
TSyResult SyCfgLoad (TSyCfg *cfg, const char *filename, const TSyPrintStr *pfn);
TSyResult SyCfgSave (const TSyCfg *cfg, const char *filename, const TSyPrintStr *pfn);

TSyResult SyCfgLoadFile (TSyCfg *cfg, FILE *fl, const TSyPrintStr *pfn);
TSyResult SyCfgSaveFile (const TSyCfg *cfg, FILE *fl, const TSyPrintStr *pfn);


#ifdef __cplusplus
}
#endif

#endif
