/*
  Syren -- a lightweight downloader for Linux/BSD/MacOSX
  inspired by Axel Copyright 2001-2002 Wilmer van der Gaast
  version 0.0.6 (atomic alien)
  coded by Ketmar // Vampire Avalon

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License with
  the Debian GNU/Linux distribution in file /usr/doc/copyright/GPL;
  if not, write to the Free Software Foundation, Inc., 59 Temple Place,
  Suite 330, Boston, MA  02111-1307  USA
*/
/*
  Syren HTTP headers list
 */
#ifndef _SYREN_HDRS_C
#define _SYREN_HDRS_C


#include "syren_hdrs.h"


static void SyHdrClearItem (TSyKVListItem *key) {
  TSyKVList *cc;

  if (!key) return;
  cc = key->udata;
  if (cc) { SyKVListFree(cc); key->udata = NULL; }
}


static char *SyHdrCookieGetName (const char *cval) {
  const char *s = cval;

  if (!cval) return NULL;
  while (*s && (unsigned char)(*s) > ' ' && (unsigned char)(*s) != '=') s++;
  if (s == cval) return NULL;
  return SyStrNew(cval, s-cval);
}


static TSyKVListItem *SyHdrFindCookieField (const TSyHdrs *hdr) {
  TSyKVListItem *item;

  if (!hdr || !hdr->fields || hdr->type == SY_HDR_INVALID) return NULL;
  item = hdr->fields->first; while (item && !item->udata) item = item->next;
  return item;
}


TSyHdrs *SyHdrNew (void) {
  TSyHdrs *hdr = calloc(1, sizeof(TSyHdrs));

  if (!hdr) return NULL;
  hdr->fields = SyKVListNew();
  if (!hdr->fields) { free(hdr); return NULL; }
  hdr->code = -1; hdr->type = SY_HDR_INVALID;
  return hdr;
}


void SyHdrFree (TSyHdrs *hdr) {
  if (!hdr) return;
  SyHdrClear(hdr, SY_HDR_INVALID); SyKVListFree(hdr->fields);
  free(hdr);
}


void SyHdrClear (TSyHdrs *hdr, TSyHdrType type) {
  TSyKVListItem *key;
  if (!hdr) return;
  if (hdr->fields) {
    key = hdr->fields->first;
    while (key) { SyHdrClearItem(key); key = key->next; }
    SyKVListClear(hdr->fields);
  }
  if (hdr->firstLine) { free(hdr->firstLine); hdr->firstLine = NULL; }
  hdr->code = -1; hdr->type = type;
}


static TSyKVListItem *SyHdrAddField (TSyHdrs *hdr, const char *kname, const char *value, int *newKey) {
  TSyKVListItem *item;

  if (!hdr || !hdr->fields || hdr->type == SY_HDR_INVALID) return NULL;
  if (newKey) *newKey = 0;
  if (!strcasecmp(kname, "Set-Cookie") || !strcasecmp(kname, "Set-Cookie2") ||
      !strcasecmp(kname, "Cookie") || !strcasecmp(kname, "Cookie2")) {
    item = SyKVListSet(hdr->fields, kname, NULL, newKey); if (!item) return NULL;
    if (!item->udata) item->udata = SyKVListNew();
    if (!item->udata) { SyKVListDelete(hdr->fields, kname); return NULL; }
    ((TSyKVList *)(item->udata))->casesens = 1;
  } else item = SyKVListSet(hdr->fields, kname, value, newKey); if (!item) return NULL;
  return item;
}


TSyResult SyHdrDeleteField (TSyHdrs *hdr, const char *name) {
  TSyKVListItem *item;

  if (!hdr || !hdr->fields || hdr->type == SY_HDR_INVALID || !name || !name[0]) return SY_ERROR;
  item = SyKVListFind(hdr->fields, name); if (!item) return SY_ERROR;
  SyHdrClearItem(item);
  return SyKVListDelete(hdr->fields, name);
}


TSyBool SyHdrHasField (const TSyHdrs *hdr, const char *name) {
  TSyKVListItem *item;

  if (!hdr || !hdr->fields || hdr->type == SY_HDR_INVALID || !name || !name[0]) return SY_FALSE;
  item = SyKVListFind(hdr->fields, name); if (!item) return SY_FALSE;
  return SY_TRUE;
}


char *SyHdrGetFieldValue (const TSyHdrs *hdr, const char *name) {
  TSyKVListItem *item;

  if (!hdr || !hdr->fields || hdr->type == SY_HDR_INVALID || !name || !name[0]) return NULL;
  item = SyKVListFind(hdr->fields, name); if (!item) return NULL;
  if (item->udata) return NULL; /* see cookie functions */
  return SyStrDup(item->value);
}


/* set field; correctly processes "Set-Cookie" and "Cookie" */
TSyResult SyHdrSetFieldValue (TSyHdrs *hdr, const char *name, const char *value) {
  char *s; int newKey;
  TSyKVList *cc;
  TSyKVListItem *item;

  if (!hdr || !hdr->fields || hdr->type == SY_HDR_INVALID || !name || !name[0] || !value) return SY_ERROR;
  while (*value && (unsigned char)(*value) <= ' ') value++; /*if (!value[0]) return SY_ERROR;*/
  item = SyHdrAddField(hdr, name, value, &newKey); if (!item) return SY_ERROR;
  if (item->udata) {
    s = SyHdrCookieGetName(value);
    if (!s) { if (newKey) SyHdrDeleteField(hdr, name); return SY_ERROR; }
    cc = item->udata;
    item = SyKVListSet(cc, s, value, NULL);
    free(s);
    if (!item) { if (newKey) SyHdrDeleteField(hdr, name); return SY_ERROR; }
  }
  return SY_OK;
}


TSyKVList *SyHdrFindCookieList (const TSyHdrs *hdr) {
  TSyKVListItem *item = SyHdrFindCookieField(hdr);

  if (item) return (TSyKVList *)item->udata;
  return NULL;
}


void SyHdrDeleteCookie (TSyHdrs *hdr, const char *name) {
  SyKVListDelete(SyHdrFindCookieList(hdr), name);
}


/* value must be full value (with 'path', 'domain', etc) */
TSyResult SyHdrSetCookie (TSyHdrs *hdr, const char *value) {
  TSyKVListItem *item = SyHdrFindCookieField(hdr);
  return SyHdrSetFieldValue(hdr,
    item?item->key:(hdr->type==SY_HDR_REQUEST?"Cookie":"Set-Cookie"),
    value);
}


/* add line, parse it, set "code" if necessary */
TSyResult SyHdrAddLine (TSyHdrs *hdr, const char *line) {
  char *s, *n;
  TSyKVListItem *item;
  TSyKVList *cc;
  TSyResult res = SY_OK;

  if (!hdr || !hdr->fields || hdr->type == SY_HDR_INVALID || !line) return SY_ERROR;
  if (!line[0]) return SY_OK; /* ignore empty lines */
  if (hdr->firstLine) {
    /* "name: value" pair or multiline */
    if ((unsigned char)(*line) <= ' ') {
      /* multiline header continues */
      item = hdr->fields->last; if (!item) return SY_ERROR; /* nothing to continue */
      if (item->udata) {
        /* cookie... */
        cc = item->udata;
        item = cc->last; if (!item) return SY_ERROR; /* nothing to continue */
      }
      s = SySPrintf("%s%s", item->value?item->value:"", line); if (!s) return SY_ERROR;
      if (item->value) free(item->value);
      item->value = s;
    } else {
      /* "name: value" pair */
      s = strchr(line, ':');
      if (!s) return SY_ERROR;
      n = SyStrNew(line, s-line); if (!n) return SY_ERROR;
      SyStrTrim(n); if (!n[0]) { free(n); return SY_ERROR; }
      s++; s = SyStrNew(s, -1); if (!s) { free(n); return SY_ERROR; }
      SyStrTrim(s); if (!s[0]) { free(s); free(n); return SY_OK; }
      res = SyHdrSetFieldValue(hdr, n, s);
      free(s); free(n);
    }
  } else {
    /* first line */
    s = SyStrNew(line, -1); if (!s) return SY_ERROR;
    hdr->firstLine = s;
    if (hdr->type == SY_HDR_REPLY) {
      /* parse reply code; bad code is error! */
      /* hack for brain-damaged 'bandit' server */
#if 0
      while (*s) {
        s = strstr(s, "HTTP/"); if (!s) break;
        s += 5;
        if (isdigit(*s) && s[1] == '.' && isdigit(s[2])) { s += 3; break; }
      }
#else
      /* i don't care about first word now, just skip it and be happy */
      while (*s && (unsigned char)(*s) > ' ') ++s;
#endif
      if (!s) return SY_ERROR;
      while (*s && (unsigned char)(*s) > ' ') s++;
      while (*s && (unsigned char)(*s) <= ' ') s++;
      if (!isdigit(*s) || !isdigit(s[1]) || !isdigit(s[2])) return SY_ERROR;
      if ((unsigned char)(s[3]) > ' ') return SY_ERROR;
      hdr->code = (*s-'0')*100+(s[1]-'0')*10+(s[2]-'0');
    }
  }
  return res;
}


#endif
