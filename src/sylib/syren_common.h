/*
  Syren -- a lightweight downloader for Linux/BSD/MacOSX
  inspired by Axel Copyright 2001-2002 Wilmer van der Gaast
  version 0.0.6 (atomic alien)
  coded by Ketmar // Vampire Avalon

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License with
  the Debian GNU/Linux distribution in file /usr/doc/copyright/GPL;
  if not, write to the Free Software Foundation, Inc., 59 Temple Place,
  Suite 330, Boston, MA  02111-1307  USA
*/
/*
  Syren commons
 */
#ifndef _SYREN_COMMON_H
#define _SYREN_COMMON_H

#ifdef __cplusplus
extern "C" {
#endif


#ifndef _GNU_SOURCE
# define _GNU_SOURCE
#endif


#include <stdint.h>
#include <ctype.h>
#include <time.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>

/*#include "syren_os.h"*/
/* see os-specific includes in syren_os.h */


#ifdef __linux
#define SYREN_OS_NAME "Linux"
#else
#ifdef __APPLE__
#define SYREN_OS_NAME "Darwin"
#else
#define SYREN_OS_NAME "BSD"
#endif
#endif


#define SYREN_VERSION_CODENAME "atomic alien"
#define SYREN_VERSION_STRING "0.0.6"
#define SYREN_CDATETIME_STRING __DATE__ " " __TIME__
#define SYREN_VERSION_DATETIME_STRING "Syren v" SYREN_VERSION_STRING \
  " (" SYREN_VERSION_CODENAME ")  compile date: " SYREN_CDATETIME_STRING
#define SYREN_VDHEADER_STRING SYREN_VERSION_DATETIME_STRING "\ncoded by Ketmar // Vampire Avalon\nGNU GPL v3\n" \
  "tnx to:\n  silver (!!)\n  pingw33n\n  amber\n"
#define SYREN_DEFAULT_USER_AGENT "Mozilla/5.0 (X11; " SYREN_OS_NAME ") Syren v" SYREN_VERSION_STRING \
  " (" SYREN_VERSION_CODENAME ")"


typedef enum {
  SY_FALSE=0,
  SY_TRUE=-1
} TSyBool;


typedef enum {
  SY_OK=0,
  SY_ERROR=-1  /* MUST be -1! */
} TSyResult;


/*#define SYREN_USE_SCRIPT*/

#ifdef __cplusplus
}
#endif

#endif
