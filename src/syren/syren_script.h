/*
  Syren -- a lightweight downloader for Linux/BSD/MacOSX
  inspired by Axel Copyright 2001-2002 Wilmer van der Gaast
  version 0.0.6 (atomic alien)
  coded by Ketmar // Vampire Avalon

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License with
  the Debian GNU/Linux distribution in file /usr/doc/copyright/GPL;
  if not, write to the Free Software Foundation, Inc., 59 Temple Place,
  Suite 330, Boston, MA  02111-1307  USA
*/
/*
  Syren scripting engine
 */
#ifndef _SYREN_SCRIPT_H
#define _SYREN_SCRIPT_H


#include "syren_os.h"
#include "syren_common.h"
#include "syren_str.h"
#include "syren_msg.h"
#include "syren_hdrs.h"


#ifdef SYREN_USE_SCRIPT

#include "syren_dloader.h"

extern TSyState *state; /* dl state */

#define KLISP_ONE_ENGINE
#include "klisp/klisp.h"

#endif


char *SyLoadWholeFile (const char *fname);


#ifdef SYREN_USE_SCRIPT
extern TSyBool syScriptOK;
extern TSyBool syScriptEnabled;

extern int ssOptWriteData;


void KLispPrintF (const char *fmt, ...);


TSyResult SyScriptInit (const TSyPrintStr *pfn);
void SyScriptDeinit (void);

TSyResult SyScriptLoad (const char *fname, TSyBool showNoFile, const TSyPrintStr *pfn);
void SyScriptLoadInit (const char *argv0, const TSyPrintStr *pfn);

TSyResult SyScriptCallback (const char *event, const TSyPrintStr *pfn);
#endif


#endif
