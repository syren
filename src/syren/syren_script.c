/*
  Syren -- a lightweight downloader for Linux/BSD/MacOSX
  inspired by Axel Copyright 2001-2002 Wilmer van der Gaast
  version 0.0.6 (atomic alien)
  coded by Ketmar // Vampire Avalon

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License with
  the Debian GNU/Linux distribution in file /usr/doc/copyright/GPL;
  if not, write to the Free Software Foundation, Inc., 59 Temple Place,
  Suite 330, Boston, MA  02111-1307  USA
*/
/*
  Syren scripting engine
 */
#ifndef _SYREN_SCRIPT_C
#define _SYREN_SCRIPT_C


#include "syren_os.h"
#include "syren_common.h"
#include "syren_str.h"
#include "syren_msg.h"
#include "syren_hdrs.h"
#include "syren_script.h"


char *SyLoadWholeFile (const char *fname) {
  int fd;
  char *p;
  int left, rd;
  int64_t sz;
  char *res = NULL;

  if (!fname || !fname[0]) return NULL;
  if (!strcmp(fname, "-")) {
    fd = 1;
    sz = 1024; res = malloc(sz); if (!res) return NULL;
    *res = '\0'; p = res; left = sz-1;
    while (1) {
      rd = read(fd, p, left);
      if (rd <= 0) return res;
      p[rd] = '\0';
      sz = sz*2;
      if (sz > 2*1024*1024) p = NULL; else p = realloc(res, sz);
      if (!p) { free(res); return NULL; }
      res = p; p = p+strlen(p); left = sz-(p-res)-1;
    }
  } else {
    int fd = SyOpenFile(fname, SY_FMODE_READ, SY_FALSE);
    if (fd < 0) return NULL;
    sz = SyFileSize(fd);
    if (sz > 0 || sz <= 4*1024*1024) {
      res = calloc(1, sz+1);
      if (res) {
        if (SyReadFile(fd, res, sz) != SY_OK) { free(res); res = NULL; }
      }
    }
    SyCloseFile(fd);
  }

  return res;
}


#ifdef SYREN_USE_SCRIPT
TSyBool syScriptOK = SY_FALSE;
TSyBool syScriptEnabled = SY_TRUE;

int ssOptWriteData;

/***************************************************************/
extern TSyBool
  cfgNoOutput,
  cfgQuiet,
  cfgNoIndicator,
  cfgUseProxyHTTP,
  cfgUseProxyFTP,
  cfgDecodeFTPURL,
  cfgFTPUseConnect,
  cfgProgressWriteCR;
extern int
  cfgBufferSize,
  cfgMaxBufferSize,
  cfgSaveInterval,
  cfgReconnectDelay,
  cfgReconnectAttempts,
  cfgTimeout,
  cfgMaxRedirects,
  cfgRefererType;
extern char
  *cfgDefaultName,
  *cfgIFace,
  *cfgProxyHTTP,
  *cfgProxyFTP,
  *cfgRefererStr,
  *cfgUAStr;

extern char
  *oOutFileName,
  *oStateFileName,
  *oURL,
  *oPostData,
  *oSendCookies,
  *oCookieDump,
  *oAddonHeaders;
extern TSyBool oResume;
extern TSyBool oCanRename;
extern TSyBool oCanRemoveState;
extern TSyBool oIgnoreStatePos;
extern TSyBool oNoStateFile;
extern TSyBool oForceRetry;
extern TSyBool writeCfg;
extern TSyBool dumpState;
extern TSyBool replyOnly;
extern int uEncodeDecode;
extern TSyBool doRXVT;
/***************************************************************/

static const TSyPrintStr *xpfn = NULL;


static char *resstr = NULL;

void KLispPrintF (const char *fmt, ...) {
  int n, size = 256;
  va_list ap;
  char *p, *np;

  if ((p = malloc(size)) == NULL) return;
  while (1) {
    memset(p, 0, size);
    va_start(ap, fmt);
    n = vsnprintf(p, size, fmt?fmt:"", ap);
    va_end(ap);
    if (n > -1 && n < size) break;
    if (n > -1) size = n+1; else size *= 2;
    if ((np = realloc(p, size)) == NULL) { free(p); return; }
    p = np;
  }

  np = SySPrintf("%s%s", resstr?resstr:"", p);
  free(p);
  if (!np) return;
  free(resstr);
  resstr = np;

  /*fprintf(stderr, "!!!%s!!!\n", resstr);*/
  while ((p = strchr(resstr, '\n'))) {
    *p = '\0';
    if (xpfn) SyMessage(xpfn, SY_MSG_MSG, "%s", resstr);
    p++; np = resstr; while ((*np++ = *p++)) ;
  }
  if (!*resstr) { free(resstr); resstr = NULL; }
}


static void KLispFlush (void) {
  if (xpfn && resstr && *resstr) SyMessage(xpfn, SY_MSG_MSG, "%s", resstr);
  if (resstr) { free(resstr); resstr = NULL; }
}


static int KLPrim_Load (KLISP_POOL_DEF int args) {
  int cell;
  char *t, *str;
  const char *sexpr, *s;

  /*KLispPrintCell(KLISP_POOL_ARG args); KLispPrintF("\n");*/
  if (!KLispCheckArgs(KLISP_POOL_ARG args, 1, "$", "load", 1)) return 0;
  s = KLISP_CELL(KLISP_CELL(args).car).str;
  if (!*s) return KLispError(KLISP_POOL_ARG "load: invalid file name");

  t = SySPrintf("%s.lsp", s);
  if (!t) goto errquit;
  str = SyLoadWholeFile(t);
  if (str) goto loaded;
  free(t);

  str = getenv("HOME");
  if (str != NULL) {
    t = SySPrintf("%s/.syren/%s.lsp", str, s);
    if (!t) goto errquit;
    str = SyLoadWholeFile(t);
    if (str) goto loaded;
    free(t);
  }

  t = SySPrintf("/etc/syren/%s.lsp", s);
  if (!t) goto errquit;
  str = SyLoadWholeFile(t);
  if (str) goto loaded;
  free(t);

  return KLispError(KLISP_POOL_ARG "file not found: %s.lsp", s);
loaded:
  KLispPrintF("file: %s\n", t);
  sexpr = str; cell = KLispParseSExpr(KLISP_POOL_ARG &sexpr);
  free(str);
  if (KLISP_POOL->error) {
    KLispPrintF("%s: PARSE ERROR: %s\n", t, KLISP_POOL->error);
    free(t);
    return 0;
  }
  free(t);
  return cell;
errquit:
  return KLispErrorMem(KLISP_POOL_ARG0);
}


static int KLPrim_Sleep (KLISP_POOL_DEF int args) {
  TKLispNumber n;

  if (!KLispCheckArgs(KLISP_POOL_ARG args, 1, "n", "load", 1)) return 0;
  n = KLISP_CELL(KLISP_CELL(args).car).num;
  // 1sec=1000000
  int sec = (int)(n*1000000); // nseconds
  while (sec > 0) {
    if (sec >= 1000000/2) {
      usleep(1000000/2);
      sec -= 1000000/2;
    } else {
      usleep(sec);
      sec = 0;
    }
  }
  return 0;
}


typedef enum {
  UDATA_URL
} TUDataType;


static void *KLCheckUType (KLISP_POOL_DEF int args, TUDataType ctype) {
  void *ptr;

  if (!args) goto error;
  args = KLISP_CELL(args).car;
  if (!args) goto error;
  if (KLISP_CELL(args).ctype != KLISP_TYPE_UDATA) goto error;
  ptr = KLISP_CELL(args).udata;
  if (*((TUDataType *)ptr) != ctype) goto error;

  return ptr;
error:
  KLispError(KLISP_POOL_ARG "invalid udata type");
  return NULL;
}


typedef struct {
  TUDataType utype;
  TSyURL *url;
} TUDataURL;


/* URL object */
static int KURLFinalizer (KLISP_POOL_DEF void *ptr, int udatacell) {
  /*fprintf(stderr, "finalizer for <%s>\n", ((TUDataURL *)ptr)->url);*/
  free(ptr);
  return 0;
}


/*
typedef struct {
  TSyProto proto;
  char *protostr;
  char *user, *pass;
  char *host;
  int port; TSyBool defaultPort;
  char *dir, *file, *query, *anchor;
} TSyURL;
*/
#define XURL_MENTRY(name) \
  {1, KURL_Get##name, "Get" #name}, \
  {1, KURL_Set##name, "Set" #name},

#define XURL_SFIELD(name,fldname) \
static int KURL_Get##name (KLISP_POOL_DEF int args) {\
  TUDataURL *uurl; \
 \
  if (!KLispCheckArgs(KLISP_POOL_ARG args, 1, "u", "URL:Get" #name, 1)) return 0; \
  if (!(uurl = KLCheckUType(KLISP_POOL_ARG args, UDATA_URL))) return 0; \
 \
  return KLispNewSym(KLISP_POOL_ARG uurl->url->fldname); \
} \
static int KURL_Set##name (KLISP_POOL_DEF int args) { \
  TUDataURL *uurl; \
  char *s; \
 \
  if (!KLispCheckArgs(KLISP_POOL_ARG args, 1, "u$", "URL:Set" #name, 1)) return 0; \
  if (!(uurl = KLCheckUType(KLISP_POOL_ARG args, UDATA_URL))) return 0; \
  args = KLISP_CELL(KLISP_CELL(args).cdr).car; \
  s = SyStrDup(KLISP_CELL(args).str); \
  if (s) { \
    SyStrFree(uurl->url->fldname); \
    uurl->url->fldname = s; \
 \
    return 1; \
  } \
 \
  return 0; \
}
#define XURL_NFIELD(name,fldname) \
static int KURL_Get##name (KLISP_POOL_DEF int args) {\
  TUDataURL *uurl; \
 \
  if (!KLispCheckArgs(KLISP_POOL_ARG args, 1, "u", "URL:Get" #name, 1)) return 0; \
  if (!(uurl = KLCheckUType(KLISP_POOL_ARG args, UDATA_URL))) return 0; \
 \
  return KLispNewNum(KLISP_POOL_ARG uurl->url->fldname); \
} \
static int KURL_Set##name (KLISP_POOL_DEF int args) { \
  TUDataURL *uurl; \
 \
  if (!KLispCheckArgs(KLISP_POOL_ARG args, 1, "un", "URL:Set" #name, 1)) return 0; \
  if (!(uurl = KLCheckUType(KLISP_POOL_ARG args, UDATA_URL))) return 0; \
  args = KLISP_CELL(KLISP_CELL(args).cdr).car; \
  uurl->url->fldname = (int)KLISP_CELL(args).num; \
 \
  return 1; \
}


XURL_SFIELD(Proto, protostr)
XURL_SFIELD(User, user)
XURL_SFIELD(Pass, pass)
XURL_SFIELD(Host, host)
XURL_SFIELD(Dir, dir)
XURL_SFIELD(File, file)
XURL_SFIELD(Query, query)
XURL_SFIELD(Anchor, anchor)
XURL_NFIELD(Port, port)

struct _TKLispPrimItem uURLMethods[] = {
  XURL_MENTRY(Proto)
  XURL_MENTRY(User)
  XURL_MENTRY(Pass)
  XURL_MENTRY(Host)
  XURL_MENTRY(Dir)
  XURL_MENTRY(File)
  XURL_MENTRY(Query)
  XURL_MENTRY(Anchor)
  XURL_MENTRY(Port)
  {0, NULL, NULL}
};

#undef XURL_MENTRY
#undef XURL_SFIELD


static int KLPrim_URL (KLISP_POOL_DEF int args) {
  TUDataURL *uurl;
  int coURL;

  uurl = calloc(1, sizeof(TUDataURL));
  uurl->url = state->url;
  coURL = KLispNewUData(KLISP_POOL_ARG uurl, KURLFinalizer);
  if (coURL < 0) { free(uurl); return -1; }
  if (!KLispRegisterMethods(KLISP_POOL_ARG coURL, uURLMethods)) {
    free(uurl);
    return -1;
  }

  return coURL;
}


/*
/ * return NULL or new string * /
char *SyHdrGetCookie (const TSyHdrs *hdr, const char *name);
void SyHdrDeleteCookie (TSyHdrs *hdr, const char *name);
*/

extern TSyHdrs *scHdrs;

static int KLPrim_HdrGetField (KLISP_POOL_DEF int args) {
  char *val;

  if (!KLispCheckArgs(KLISP_POOL_ARG args, 1, "$", "HdrGetField", 1)) return 0;
  if (!scHdrs) return 0; /* no headers here */
  val = SyHdrGetFieldValue(scHdrs, KLISP_CELL(KLISP_CELL(args).car).str);
  if (!val) return 0; /* no such field */
  args = KLispNewSym(KLISP_POOL_ARG val);
  SyStrFree(val);

  return args;
}


static int KLPrim_HdrSetField (KLISP_POOL_DEF int args) {
  int arg1, arg2;

  if (!KLispCheckArgs(KLISP_POOL_ARG args, 1, "$[$N]", "HdrSetField", 1)) return 0;
  if (!scHdrs) return 0; /* no headers here */
  arg1 = KLISP_CELL(args).car;
  arg2 = KLISP_CAR_EX(KLISP_CDR_EX(args));
  if (arg2) {
    if (SyHdrSetFieldValue(scHdrs, KLISP_CELL(arg1).str, KLISP_CELL(arg2).str) != SY_OK) return 0;
  } else {
    if (SyHdrDeleteField(scHdrs, KLISP_CELL(arg1).str) != SY_OK) return 0;
  }

  return 1;
}


static int KLPrim_HdrGetCode (KLISP_POOL_DEF int args) {
  if (!KLispCheckArgs(KLISP_POOL_ARG args, 1, "", "HdrGetCode", 1)) return 0;
  if (!scHdrs) return 0; /* no headers here */

  return KLispNewNum(KLISP_POOL_ARG scHdrs->code);
}


static int KLPrim_HdrSetCode (KLISP_POOL_DEF int args) {
  if (!KLispCheckArgs(KLISP_POOL_ARG args, 1, "n", "HdrSetCode", 1)) return 0;
  if (!scHdrs) return 0; /* no headers here */
  scHdrs->code = (int)(KLISP_CELL(KLISP_CAR_EX(args)).num);

  return 1;
}


#define SYSC_INTVAR(name,var) \
static int KLPrim_Get##name (KLISP_POOL_DEF int args) { \
  if (!KLispCheckArgs(KLISP_POOL_ARG args, 1, "", "Get" #name, 1)) return 0; \
 \
  return KLispNewNum(KLISP_POOL_ARG var); \
} \
 \
static int KLPrim_Set##name (KLISP_POOL_DEF int args) { \
  if (!KLispCheckArgs(KLISP_POOL_ARG args, 1, "n", "Set" #name, 1)) return 0; \
  var = (int)(KLISP_CELL(KLISP_CAR_EX(args)).num); \
 \
  return 1; \
}

#define SYSC_STRVAR(name,var) \
static int KLPrim_Get##name (KLISP_POOL_DEF int args) { \
  if (!KLispCheckArgs(KLISP_POOL_ARG args, 1, "", "Get" #name, 1)) return 0; \
 \
  return KLispNewSym(KLISP_POOL_ARG var); \
} \
 \
static int KLPrim_Set##name (KLISP_POOL_DEF int args) { \
  char *s; \
 \
  if (!KLispCheckArgs(KLISP_POOL_ARG args, 1, "$", "Set" #name, 1)) return 0; \
  s = SyStrDup(KLISP_CELL(KLISP_CAR_EX(args)).str); \
  if (s) { SyStrFree(var); var = s; } \
 \
  return 1; \
}

/*
SYSC_INTVAR(WriteData, ssOptWriteData);
*/
SYSC_STRVAR(OutFileName, oOutFileName);


extern TSyKVList *oURLList;
static int KLPrim_AddURL (KLISP_POOL_DEF int args) {
  int arg1, arg2;

  if (!KLispCheckArgs(KLISP_POOL_ARG args, 1, "$[$N]", "AddURL", 1)) return 0;
  /*if (!scHdrs) return 0; no headers here */
  arg1 = KLISP_CELL(args).car;
  arg2 = KLISP_CAR_EX(KLISP_CDR_EX(args));

  SyKVListSet(oURLList, KLISP_CELL(arg1).str, arg2?KLISP_CELL(arg2).str:"", NULL);

  return 1;
}


struct _TKLispPrimItem syKLispPrimList[] = {
  {1, KLPrim_Load, "load"},
  {1, KLPrim_URL, "URL"},
  {1, KLPrim_HdrGetField, "HdrGetField"},
  {1, KLPrim_HdrSetField, "HdrSetField"},
  {1, KLPrim_HdrGetCode, "HdrGetCode"},
  {1, KLPrim_HdrSetCode, "HdrSetCode"},

  {1, KLPrim_AddURL, "AddURL"},

  {1, KLPrim_GetOutFileName, "GetOutFileName"},
  {1, KLPrim_SetOutFileName, "SetOutFileName"},

  {1, KLPrim_Sleep, "sleep"},
/*
  {1, KLPrim_GetWriteData, "GetWriteData"},
  {1, KLPrim_SetWriteData, "SetWriteData"},
*/

  {0, NULL, NULL}
};


static TKLispPool *pool = NULL;


TSyResult SyScriptInit (const TSyPrintStr *pfn) {
  syScriptOK = SY_FALSE;
  if (!(pool = KLispNewPool())) {
    SyMessage(pfn, SY_MSG_WARNING, "can't initialize scripting engine");
    return SY_ERROR;
  }
  if (!KLispRegisterPrims(KLISP_POOL_ARG klispPrimList)) {
    SyMessage(pfn, SY_MSG_WARNING, "can't initialize scripting engine primitives");
    KLispFreePool(KLISP_POOL_ARG0);
    return SY_ERROR;
  }
  if (!KLispRegisterPrims(KLISP_POOL_ARG syKLispPrimList)) {
    SyMessage(pfn, SY_MSG_WARNING, "can't initialize scripting engine primitives (1)");
    KLispFreePool(KLISP_POOL_ARG0);
    return SY_ERROR;
  }
  syScriptOK = SY_TRUE;

  return SY_OK;
}


void SyScriptDeinit (void) {
  if (syScriptOK != SY_TRUE) return;
  KLispFreePool(KLISP_POOL_ARG0);
  KLispRBTCleanup();
  syScriptOK = SY_FALSE;
}


static int Eval (KLISP_POOL_DEF const char *str, const TSyPrintStr *pfn) {
  int optPrintParsed = 0, optPrintResult = 0, optPrintError = 0;

  xpfn = pfn;
  KLispEvalStringEx(KLISP_POOL_ARG str, &optPrintParsed, &optPrintResult, &optPrintError);
  KLispFlush();
  xpfn = NULL;
  if (KLISP_POOL->error) {
    SyMessage(pfn, SY_MSG_WARNING, "%i: script error: %s", KLISP_POOL->errorLine, KLISP_POOL->error);
    return 0;
  }

  return 1;
}


TSyResult SyScriptLoad (const char *fname, TSyBool showNoFile, const TSyPrintStr *pfn) {
  char *str;

  if (syScriptOK != SY_TRUE) return SY_ERROR;
  if (syScriptEnabled != SY_TRUE) return SY_OK;
  str = SyLoadWholeFile(fname);
  if (!str) {
    if (showNoFile == SY_TRUE) SyMessage(pfn, SY_MSG_WARNING, "can't load script: %s", fname?fname:"()");
    return SY_ERROR;
  }
  KLISP_POOL->lineNo = 1;
  if (!Eval(KLISP_POOL_ARG str, pfn)) return SY_ERROR;

  return SY_OK;
}


void SyScriptLoadInit (const char *argv0, const TSyPrintStr *pfn) {
  char *t, *s, *l;

  if (syScriptOK != SY_TRUE) return;
  if (syScriptEnabled != SY_TRUE) return;
  SyScriptLoad("/etc/syren/syren.lsp", SY_FALSE, pfn);
  t = getenv("HOME");
  if (t != NULL) {
    s = SySPrintf("%s/.syren/syren.lsp", t);
    SyScriptLoad(s, SY_FALSE, pfn);
    SyStrFree(s);
    s = SySPrintf("%s/syren.lsp", t);
    SyScriptLoad(s, SY_FALSE, pfn);
    SyStrFree(s);
  }
  if (argv0) {
    s = SyStrDup(argv0);
    if (s) {
      t = s; l = NULL;
      while (*t) {
        if (*t == '/') l = t;
        t++;
      }
      if (l) {
        *l = '\0';
        t = SySPrintf("%s/syren.lsp", s);
        if (t) {
          SyScriptLoad(t, SY_FALSE, pfn);
          SyStrFree(t);
        }
      }
      SyStrFree(s);
    }
  }
  SyScriptLoad("syren.lsp", SY_FALSE, pfn);
}


TSyResult SyScriptCallback (const char *event, const TSyPrintStr *pfn) {
  int cb;

  if (syScriptEnabled != SY_TRUE) return SY_OK;
  cb = KLispGetSymbol(KLISP_POOL_ARG event);
  if (cb <= 0) return SY_OK;

  ssOptWriteData = 1;
  xpfn = pfn;
  /*KLispPrintCell(KLISP_POOL_ARG cb);
  KLispPrintF("\n");*/
  KLispFreeError(KLISP_POOL_ARG0);
  cb = KLispNewCons(KLISP_POOL_ARG KLISP_CELL(cb).cdr, 0);
  /*KLispPrintCell(KLISP_POOL_ARG cb);
  KLispPrintF("\n");*/
  if (cb >= 0) KLispEval(KLISP_POOL_ARG cb, NULL, NULL);
  KLispFlush();
  xpfn = NULL;
  if (KLISP_POOL->error) {
    SyMessage(pfn, SY_MSG_WARNING, "%i: script error: %s", KLISP_POOL->errorLine, KLISP_POOL->error);
    return SY_ERROR;
  }

  return SY_OK;
}
#endif


#endif
