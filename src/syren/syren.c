/*
  Syren -- a lightweight downloader for Linux/BSD/MacOSX
  inspired by Axel Copyright 2001-2002 Wilmer van der Gaast
  version 0.0.6 (atomic alien)
  coded by Ketmar // Avalon Group

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License with
  the Debian GNU/Linux distribution in file /usr/doc/copyright/GPL;
  if not, write to the Free Software Foundation, Inc., 59 Temple Place,
  Suite 330, Boston, MA  02111-1307  USA
*/
/*
 * Syren main program
 */
#ifdef __linux
# ifndef _GNU_SOURCE
#  define _GNU_SOURCE
# endif
#endif


#define USE_NEW_PREALLOC  0


#include "syren_common.h"
#include "syren_os.h"
#include "syren_str.h"
#include "syren_msg.h"
#include "syren_cfg.h"
#include "syren_hdrs.h"
#include "syren_tcp.h"
#include "syren_http.h"
#include "syren_proxy.h"
#include "syren_ftp.h"
#include "syren_dloader.h"

#include "syren_script.h"


#include <signal.h>

/* posix_fallocate */
#include <fcntl.h>
/*
#ifdef __linux
# include <linux/fallocate.h>
#endif
*/

/* uncomment this to use Linux TTY ioctls */
/*
#ifdef __linux
#define LINUX_TTY
#endif
*/

/* uncomment this to use ANSI/VT100 TTY codes */
#define ANSI_TTY

/* uncomment this to use "DEC private" TTY codes (ESC [ ? n)*/
#define DEC_TTY



TSyBool
  cfgNoOutput = SY_FALSE,
  cfgQuiet = SY_FALSE,
  cfgNoIndicator = SY_FALSE,
  cfgUseProxyHTTP = SY_TRUE,
  cfgUseProxyFTP = SY_TRUE,
  cfgDecodeFTPURL = SY_TRUE,
  cfgFTPUseConnect = SY_FALSE,
  cfgProgressWriteCR = SY_FALSE,
  optAnyCode = SY_FALSE;
int
  cfgBufferSize = 8192,
  cfgMaxBufferSize = 1024*1024,
  cfgSaveInterval = 10,
  cfgReconnectDelay = 10,
  cfgReconnectAttempts = 666,
  cfgTimeout = 60,
  cfgMaxRedirects = 10,
  cfgRefererType = 0;
char
  *cfgDefaultName = NULL,
  *cfgIFace = NULL,
  *cfgProxyHTTP = NULL,
  *cfgProxyFTP = NULL,
  *cfgRefererStr = NULL,
  *cfgUAStr = NULL;

char
  *oOutFileName = NULL, /* -o/-O */
  *oStateFileName = NULL, /* -s */
  *oURL = NULL,
  *oPostData = NULL,
  *oSendCookies = NULL,
  *oCookieDump = NULL,
  *oAddonHeaders = NULL;
TSyBool
  oResume = SY_FALSE,
  oCanRename = SY_TRUE,
  oCanRemoveState = SY_TRUE,
  oIgnoreStatePos = SY_FALSE,
  oNoStateFile = SY_FALSE,
  oForceRetry = SY_FALSE,
  writeCfg = SY_FALSE,
  dumpState = SY_FALSE,
  replyOnly = SY_FALSE,
  oPreallocSpace = SY_TRUE;
int uEncodeDecode = 0;
TSyBool doRXVT = SY_FALSE;
int optWOS = 0;


TSyKVList *oURLList = NULL;

double lastSSave = 0;

TSyState *state = NULL; /* dl state */
#ifdef SYREN_USE_SCRIPT
TSyBool scAbort = SY_FALSE;
#endif

#ifdef LINUX_TTY
static int GetTTYWidth (void) {
  struct winsize ws;
  if (!isatty(fileno(stderr))) return -1;
  if (ioctl(fileno(stderr), TIOCGWINSZ, &ws)) return -1;
  return ws.ws_col;
}
#endif


static void SetTermTitle (const char *tit) {
  if (!tit || !isatty(fileno(stdout)) || doRXVT != SY_TRUE) return;
  fprintf(stdout, "\x1b]2;%s\7", tit);
  fflush(stdout);
}


static TSyBool ctrlCPressed = SY_FALSE;

static void DoWait (int seconds) {
  while (seconds > 0) {
    char *tit = SySPrintf("waiting: %i seconds left", seconds);
    SetTermTitle(tit);
    SyStrFree(tit);
    SySleep(1);
    seconds--;
    if (ctrlCPressed != SY_FALSE) return;
  }
}


static void PrintProgress (TSyState *state, int64_t bytesDone, int64_t bytesTotal, TSyBool done) {
  char tmp0[128], tmp1[128], tmp2[128], tmp3[128], tmp4[128], fmt[128], str[1024];
  int len; int64_t fb = state->firstByte;
  TSyStats *stats = &state->stats;

  if (cfgNoIndicator) return;
  fprintf(stderr, "\r");
  if (isatty(fileno(stderr))) {
#ifdef DEC_TTY
    fputs("\x1b[?7l", stderr);
#endif
  }
  if (bytesTotal > 0) {
    len = SyLong2StrComma(tmp1, (bytesTotal+fb));
    SyLong2StrComma(tmp0, (bytesDone+fb));
    sprintf(fmt, "[%%%is/%%s] ", len);
    sprintf(tmp2, fmt, tmp0, tmp1); /*!*/
    sprintf(tmp3, "[%3i%%] ", (int)((double)100*(bytesDone+fb)/(bytesTotal+fb))); /*!*/
    /* tmp3: res */
    SySize2Str(tmp0, (int64_t)stats->bps);
    sprintf(tmp4, "[SPD:%s] ", tmp0); /*!*/
    SyTime2Str(tmp0, (int)(SyGetTimeD()-stats->sttime));
    SyTime2Str(tmp1, (int)stats->eta);
    sprintf(fmt, "TIME:%s ETA:%s", tmp0, tmp1); /*!*/
    fprintf(stderr, "%s%s%s%s", tmp2, tmp3, tmp4, fmt);
    sprintf(str, "%s%s%s/%s", tmp2, tmp3, tmp0, tmp1);
  } else {
    SyLong2StrComma(tmp0, bytesDone+fb);
    SyTime2Str(tmp1, (int)(SyGetTimeD()-stats->sttime));
    SySize2Str(fmt, (int64_t)stats->bps);
    sprintf(str, "[%s] [SPD:%s] TIME:%s", tmp0, fmt, tmp1);
    fprintf(stderr, "%s", str);
  }
  if (isatty(fileno(stdout)) && doRXVT == SY_TRUE) {
    fprintf(stdout, "\x1b]2;%s\7", str);
    fflush(stdout);
  }
  if (isatty(fileno(stderr))) {
#ifdef ANSI_TTY
    fprintf(stderr, "\x1b[K");
#endif
#ifdef DEC_TTY
    fputs("\x1b[?7h", stderr);
#endif
  }
  if (done == SY_TRUE) fputs("\n", stderr);
  else if (cfgProgressWriteCR == SY_TRUE) fprintf(stderr, "\n");
  fflush(stderr);
}


static void SyPrintStr (void *udata, TSyMsgType msgtype, const char *msg) {
#ifdef LINUX_TTY
  int wdt;
#endif
  static const char *ltr = "NMWE?";
  if (cfgNoOutput) return;
  if (cfgQuiet && msgtype < SY_MSG_MSG) return;
  fflush(stderr);
  if (isatty(fileno(stderr))) {
#ifndef LINUX_TTY
# ifdef DEC_TTY
    fputs("\x1b[?7l", stderr);
# endif
#else
    wdt = GetTTYWidth();
    if (wdt < 0) wdt = strlen(msg)+16;
    if (wdt < 2) return;
#endif
  }
  if (msgtype < SY_MSG_NOTICE || msgtype > SY_MSG_ERROR) msgtype = SY_MSG_ERROR+1;
  fputc(ltr[msgtype], stderr);
#ifdef LINUX_TTY
  if (wdt > 4) {
    fputs(": ", stderr);
    wdt -= 4;
    if (wdt >= strlen(msg)) fputs(msg, stderr);
    else while (wdt--) fputc(*(msg++), stderr);
  }
#else
  fputs(": ", stderr);
  fputs(msg, stderr);
#endif
  if (isatty(fileno(stderr))) {
#ifdef ANSI_TTY
    fputs("\x1b[K", stderr);
#endif
#ifndef LINUX_TTY
# ifdef DEC_TTY
    fputs("\x1b[?7h", stderr);
# endif
#endif
  }
  fputs("\n", stderr);
  fflush(stderr);
  if (isatty(fileno(stdout)) && doRXVT == SY_TRUE && msgtype > SY_MSG_NOTICE) {
    fprintf(stdout, "\x1b]2;%s\7", msg);
    fflush(stdout);
  }
}


static void SyDefaultCfg (TSyCfg *cfg) {
  SyCfgAddIntKey(cfg, "reconnect_delay", "number of seconds to wait before reconnecting to server", &cfgReconnectDelay);
  SyCfgAddIntKey(cfg, "reconnect_attempts", "number of reconnection attempts (0: do not reconnect)", &cfgReconnectAttempts);
  SyCfgAddIntKey(cfg, "io_timeout", "i/o timeout in seconds", &cfgTimeout);
  SyCfgAddIntKey(cfg, "save_state_interval", "save state every x seconds (0: disable)", &cfgSaveInterval);
  cfgDefaultName = SyStrNew("index.html", -1);
  SyCfgAddStrKey(cfg, "default_file_name", "default file name for http", &cfgDefaultName);
  SyCfgAddIntKey(cfg, "max_redirects", "maximum number of redirects/symlinks", &cfgMaxRedirects);
  SyCfgAddIntKey(cfg, "init_buffer_size", "initial buffer size", &cfgBufferSize);
  SyCfgAddIntKey(cfg, "max_buffer_size", "maximum amount of bytes read buffer can grow to", &cfgMaxBufferSize);
  SyCfgAddBoolKey(cfg, "quiet", "tan: do not show notices", &cfgQuiet);
  SyCfgAddBoolKey(cfg, "no_output", "tan: disable any output", &cfgNoOutput);
  SyCfgAddBoolKey(cfg, "no_indicator", "tan: hide download indicator", &cfgNoIndicator);

  cfgIFace = SyStrNew("", -1);
  SyCfgAddStrKey(cfg, "interface", "net interface to use for connection (name or IP)", &cfgIFace);

  cfgProxyHTTP = SyStrNew(getenv("http_proxy"), -1);
  SyCfgAddStrKey(cfg, "http_proxy", "proxy for http connections", &cfgProxyHTTP);
  cfgProxyFTP = SyStrNew(getenv("ftp_proxy"), -1);
  SyCfgAddStrKey(cfg, "ftp_proxy", "proxy for ftp connections", &cfgProxyFTP);
  SyCfgAddBoolKey(cfg, "use_http_proxy", "use http proxy (if specified)?", &cfgUseProxyHTTP);
  SyCfgAddBoolKey(cfg, "use_ftp_proxy", "use ftp proxy (if specified)?", &cfgUseProxyFTP);
  SyCfgAddBoolKey(cfg, "ftp_proxy_use_connect", "use CONNECT method for proxied ftp connections", &cfgFTPUseConnect);

  SyCfgAddIntKey(cfg, "referer_type", "set referer type (0:none; 1:orig URL; 2:follow URL; 3:user)", &cfgRefererType);
  cfgRefererStr = SyStrNew(NULL, -1);
  SyCfgAddStrKey(cfg, "referer_str", "referer for mode 3", &cfgRefererStr);
  SyCfgAddBoolKey(cfg, "decode_ftp_url", "decode URLs for FTP", &cfgDecodeFTPURL);

  cfgUAStr = SyStrNew(SYREN_DEFAULT_USER_AGENT, -1);
  SyCfgAddStrKey(cfg, "ua_str", "user agent (set to empty string if no UA should be sent)", &cfgUAStr);
}



TSyResult SySaveInt (int fd, int64_t v, int size) {
  uint8_t b;

  if (fd < 0 || size < 1 || size > 8) return SY_ERROR;
  while (size--) {
    b = (v>>(size*8))&0xff;
    if (SyWriteFile(fd, &b, 1) != SY_OK) return SY_ERROR;
  }

  return SY_OK;
}


TSyResult SyLoadInt (int fd, int64_t *v, int size) {
  uint8_t b; int64_t res = 0;

  if (v) *v = 0;
  if (!fd || size < 1 || size > 8) return SY_ERROR;
  while (size--) {
    if (SyReadFile(fd, &b, 1) != SY_OK) return SY_ERROR;
    res = (res<<8)+b;
  }
  if (v) *v = res;

  return SY_OK;
}


TSyResult SySaveStr (int fd, const char *v) {
  int len = 0;

  if (!fd) return SY_ERROR;
  if (v) len = strlen(v);
  if (len > 65535) return SY_ERROR;
  if (SySaveInt(fd, len, 2) != SY_OK) return SY_ERROR;
  if (len) { if (SyWriteFile(fd, v, len) != SY_OK) return SY_ERROR; }

  return SY_OK;
}


char *SyLoadStr (int fd) {
  char *v;
  int64_t len = 0;

  if (!fd) return NULL;
  if (SyLoadInt(fd, &len, 2) != SY_OK) return NULL;
  if (!len) return SyStrNewEmpty();
  v = calloc(1, len+8); if (!v) return NULL;
  if (SyReadFile(fd, v, len) != SY_OK) { free(v); return NULL; }

  return v;
}


static const char *SF_STR = "SYREN STATE FILE v6\r\n\x1a";
TSyResult SySaveState (const char *destfile, TSyState *state) {
  TSyResult res = SY_ERROR;
  TSyURL *url;
  char *s, *t;
  int fd;

  if (!state) return SY_ERROR;
  url = state->url;
  s = SySPrintf("%s://%s:%s@%s:%i%s%s%s%s",
     url->protostr, url->user, url->pass, url->host, url->port,
     url->dir, url->file, url->query, url->anchor);
  if (!s) return SY_ERROR;
  fd = SyOpenFile(destfile, SY_FMODE_WRITE, SY_TRUE);
  if (fd >= 0) {
    while (1) {
      /*if (SySaveStr(fd, "SYREN STATE FILE v5") != SY_OK) break;*/
      if (SyWriteFile(fd, SF_STR, strlen(SF_STR)) != SY_OK) break;
      if (SySaveInt(fd, state->fileSize, 8) != SY_OK) break;
      if (SySaveInt(fd, state->currentByte+state->firstByte, 8) != SY_OK) break;
      if (SySaveInt(fd, state->lastByte, 8) != SY_OK) break;
      if (SySaveStr(fd, s) != SY_OK) break;
      t = strrchr(oOutFileName, '/'); if (!t) t = oOutFileName; else t++;
      if (SySaveStr(fd, t) != SY_OK) break;
      if (SySaveInt(fd, cfgRefererType, 1) != SY_OK) break;
      if (SySaveStr(fd, cfgRefererStr) != SY_OK) break;
      if (SySaveStr(fd, oPostData) != SY_OK) break;
      res = SY_OK; break;
    }
    SyCloseFile(fd);
  }
  if (res != SY_OK) SyDeleteFile(destfile);
  free(s);

  return res;
}


char *SyLoadStateV5 (int fd, TSyState *state) {
  char *destfile = NULL, *s = NULL;
  int64_t i;

  while (1) {
    s = SyLoadStr(fd); if (!s) break;
    if (strcmp(s, "SYREN STATE FILE v5")) break;
    if (SyLoadInt(fd, &state->fileSize, 8) != SY_OK) break;
    if (SyLoadInt(fd, &state->firstByte, 8) != SY_OK) break;
    if (oIgnoreStatePos != SY_FALSE) state->firstByte = 0;
    if (state->firstByte < 0 || (state->fileSize >= 0 && state->firstByte > state->fileSize)) break;
    if (SyLoadInt(fd, &state->lastByte, 8) != SY_OK) break;
    if (state->lastByte < -1 || (state->lastByte >= 0 && state->firstByte > state->lastByte)) break;
    free(s); s = SyLoadStr(fd); if (!s) break;
    if (oURL) free(oURL); oURL = s;
    s = SyLoadStr(fd); if (!s) break;
    destfile = s; s = NULL;
    if (SyLoadInt(fd, &i, 1) != SY_OK) break;
    cfgRefererType = (int)i;
    s = SyLoadStr(fd); if (!s) break;
    if (cfgRefererStr) free(cfgRefererStr);
    cfgRefererStr = s;
    s = SyLoadStr(fd); if (!s) { free(destfile); destfile = NULL; break; }
    if (state->postData) free(state->postData);
    state->postData = s; s = NULL;
    break;
  }
  if (s) free(s);

  return destfile;
}


char *SyLoadStateV6 (int fd, TSyState *state) {
  char *destfile = NULL, *s = NULL, buf[128];
  int64_t i;
  int l = strlen(SF_STR);

  while (1) {
    memset(buf, 0, sizeof(buf));
    if (SyReadFile(fd, buf, l) != SY_OK) break;
    if (strcmp(buf, SF_STR)) break;
    if (SyLoadInt(fd, &state->fileSize, 8) != SY_OK) break;
    if (SyLoadInt(fd, &state->firstByte, 8) != SY_OK) break;
    if (oIgnoreStatePos != SY_FALSE) state->firstByte = 0;
    if (state->firstByte < 0 || (state->fileSize >= 0 && state->firstByte > state->fileSize)) break;
    if (SyLoadInt(fd, &state->lastByte, 8) != SY_OK) break;
    if (state->lastByte < -1 || (state->lastByte >= 0 && state->firstByte > state->lastByte)) break;
    free(s); s = SyLoadStr(fd); if (!s) break;
    if (oURL) free(oURL); oURL = s;
    s = SyLoadStr(fd); if (!s) break;
    destfile = s; s = NULL;
    if (SyLoadInt(fd, &i, 1) != SY_OK) break;
    cfgRefererType = (int)i;
    s = SyLoadStr(fd); if (!s) break;
    if (cfgRefererStr) free(cfgRefererStr);
    cfgRefererStr = s;
    s = SyLoadStr(fd); if (!s) { free(destfile); destfile = NULL; break; }
    if (state->postData) free(state->postData);
    state->postData = s; s = NULL;
    break;
  }
  if (s) free(s);

  return destfile;
}


/* return destfile */
char *SyLoadState (const char *statefile, TSyState *state) {
  char *destfile = NULL;
  int fd;
  uint8_t b;

  fd = SyOpenFile(statefile, SY_FMODE_READ, SY_FALSE);
  if (fd < 0) return NULL;
  if (SyReadFile(fd, &b, 1) == SY_OK) {
    if (SySeekFile(fd, 0) == SY_OK) {
      /*fprintf(stderr, "state v%i\n", b?6:5);*/
      if (!b) destfile = SyLoadStateV5(fd, state);
      else destfile = SyLoadStateV6(fd, state);
    }
  }
  SyCloseFile(fd);

  return destfile;
}


void DumpState (TSyState *state) {
  char t0[32], t1[32], t2[32];
  SyLong2StrComma(t0, state->fileSize);
  SyLong2StrComma(t1, state->firstByte);
  SyLong2StrComma(t2, state->lastByte);
  printf("state dump\n==========\nURL: %s\nfile name: %s\n"
      "file size: %s\nfirst byte: %s\nlast byte: %s\n",
    oURL, oOutFileName, t0, t1, t2
  );
}


static void ShowHelp (void) {
  fprintf(stderr, "%s",
    "usage: syren [options] url [+url] [+url] [@urllistfile]\n"
    "options:\n"
    "  -h            this help\n"
    "  -h all        show 'long' configuration options\n"
    "  -h <longopt>  show 'long' configuration option description\n"
    "  -l filename   same as @urllistfile\n"
    "  -o filename   output to specified file\n"
    "  -O filename   output to specified file (same as '-o')\n"
    "  -r filename   resume download (do not use saved state, use file)\n"
    "  -s filename   restore state\n"
    "  -S filename   redownload file using state (ignore starting position, recreate)\n"
    "  -D filename   dump state file and exit\n"
    "  -k            keep state file\n"
    "  -P <str|@fn>  make POST request\n"
    "  -c <str|@fn>  send cookies\n"
    "  -C filename   dump cookies\n"
    "  -H <str|@fn>  additional headers\n"
    "  -i            download server reply and stop\n"
    "  -I name_or_ip use specified net interface (\"-\" any)\n"
    "  -X            do not use proxies\n"
    "  -A            don't preallocate disk space\n"
    "  -T            turn off terminal window title changing\n"
    "  -Z            write CR after progress string\n"
    "  -w            generate .syrenrc file with the current config\n"
    "  -N            don't write state file\n"
    "  -F            force retry on any download error\n"
    "  -e            encode URL\n"
    "  -E            decode URL\n"
    "  -q            be quiet\n"
    "  -Q            be VERY quiet\n"
#ifdef SYREN_USE_SCRIPT
    "  -L            disable scripting\n"
#endif
    "  -V            version\n"
    "  -W            accept any HTTP reply code\n"
    "  -9            WOS delay\n"
  );
}


static char *GetStrOpt (TSyPrintStr *prs, int argc, char *argv[], int *f, const char *optName,
    char **value, TSyBool allowEmpty, TSyBool exclusive) {
  //
  char *res/*, *s*/;
  //
  if (*f >= argc || !argv[*f]) { SyMessage(prs, SY_MSG_ERROR, "no value for '%s'", optName); return NULL; }
  if (exclusive == SY_TRUE && (value && *value)) { SyMessage(prs, SY_MSG_ERROR, "duplicate '%s'", optName); return NULL; }
  res = SyStrNew(argv[*f], -1); (*f)++;
  if (!res) { SyMessage(prs, SY_MSG_ERROR, "memory error"); return NULL; }
  if (allowEmpty != SY_TRUE && !res[0]) {
    free(res);
    SyMessage(prs, SY_MSG_ERROR, "empty value for '%s'", optName);
    return NULL;
  }
  if (value) {
    if (*value) free(*value);
    *value = res;
  }

  return res;
}


static TSyResult GetStrOptWithAt (TSyPrintStr *prs, int argc, char *argv[], int *f, const char *optName,
    char **value, TSyBool allowEmpty, TSyBool exclusive, TSyBool ishdr) {
//
  //printf("[%s]: %p\n", optName, ishdr?NULL:value);
  char *res = GetStrOpt(prs, argc, argv, f, optName, ishdr?NULL:value, allowEmpty, exclusive);
  if (!res) return SY_ERROR;
  if (*res == '@') {
    res = SyLoadWholeFile(res+1);
    if (!res) { SyMessage(prs, SY_MSG_ERROR, "can't load data from '%s'", (*value)+1); return SY_ERROR; }
    //free(*value); *value = res;
  }

  if (ishdr) {
    if (*value) {
      if (*res) {
        char *t = SySPrintf("%s\n%s", *value, res);
        free(res);
        free(*value);
        *value = t;
      } else {
        free(res);
      }
    } else {
      *value = res;
    }
  } else {
    if (value != NULL) *value = res;
  }

  return SY_OK;
}


static void SyAddURLFromFile (const char *fname) {
  char *fl, *s, *e;

  fl = SyLoadWholeFile(fname);
  if (!fl) return;
  s = fl; while (*s) {
    e = s; while (*e && *e != '\n') e++;
    if (*e) *e++ = '\0';
    SyStrTrim(s);
    if (*s && s[0] != '#' && s[0] != ';') {
      SyKVListDelete(oURLList, s);
      SyKVListSet(oURLList, s, "", NULL);
    }
    s = e;
  }
  free(fl);
}


static TSyResult ParseCmdLine (TSyPrintStr *prs, TSyCfg *cfg, int argc, char *argv[]) {
  int f, nomoreopt = 0;
  char *s, *t, *sopt, soptN[2];
  TSyKVListItem *key;

  f = 1; while (f < argc) {
    s = argv[f++];
    if (!s) break; if (!(*s)) continue;
    if (!nomoreopt && *s == '-') {
      if (s[1] == '-') {
        /* long option (config) */
        if (!s[3]) nomoreopt = 1;
        else {
          s += 2;
          if (SyCfgSet(cfg, s) != SY_OK) { SyMessage(prs, SY_MSG_ERROR, "invalid option: %s", (s-2)); return SY_ERROR; }
        }
      } else {
        sopt = s;
        while (*(++sopt)) {
          soptN[0] = *sopt; soptN[1] = '\0';
          switch (*sopt) {
            case 'V':
              printf("%s", SYREN_VDHEADER_STRING);
              return SY_ERROR;
            case '9': optWOS = 15; break;
            case 'W': optAnyCode = SY_TRUE; break;
            case 'e': uEncodeDecode = 1; break;
            case 'E': uEncodeDecode = -1; break;
            case 'w': writeCfg = SY_TRUE; break;
            case 'X': cfgUseProxyHTTP = cfgUseProxyFTP = SY_FALSE; break;
            case 'T': doRXVT = SY_FALSE; break;
            case 'q': cfgQuiet = SY_TRUE; break;
            case 'Q': cfgQuiet = cfgNoOutput = SY_TRUE; break;
            case 'i': replyOnly = SY_TRUE; break;
            case 'N': oNoStateFile = SY_TRUE; break;
            case 'F': oForceRetry = SY_TRUE; break;
            case 'k': oCanRemoveState = SY_FALSE; break;
            case 'Z': cfgProgressWriteCR = SY_TRUE; break;
            case 'A': oPreallocSpace = SY_FALSE; break;
            case 'L':
#ifdef SYREN_USE_SCRIPT
              syScriptEnabled = SY_FALSE;
#endif
              break;
            case 'I':
              if (!argv[f]) {
                SyMessage(prs, SY_MSG_ERROR, "no argument for -I");
                return SY_ERROR;
              }
              SyStrFree(cfgIFace);
              cfgIFace = SyStrDup(argv[f++]);
              /*SyStrTrim(cfgIFace);*/
              /*if (!cfgIFace[0] || !strcmp(cfgIFace, "-")) { SyStrFree(cfgIFace); cfgIFace = NULL; }*/
              break;
            case 'o': case 'O': case 'r':
              if (*sopt == 'r') oResume = SY_TRUE;
              s = GetStrOpt(prs, argc, argv, &f, soptN, &oOutFileName, SY_FALSE, SY_TRUE);
              if (!s) return SY_ERROR;
              oCanRename = SY_FALSE;
              break;
            case 's': case 'S': case 'D':
              if (*sopt == 'S') oIgnoreStatePos = SY_TRUE;
              else if (*sopt == 'D') dumpState = SY_TRUE;
              s = GetStrOpt(prs, argc, argv, &f, soptN, &oStateFileName, SY_FALSE, SY_TRUE);
              if (!s) return SY_ERROR;
              oCanRename = SY_FALSE;
              break;
            case 'P':
              if (GetStrOptWithAt(prs, argc, argv, &f, soptN, &oPostData, SY_FALSE, SY_TRUE, SY_FALSE) != SY_OK) return SY_ERROR;
              break;
            case 'c':
              if (GetStrOptWithAt(prs, argc, argv, &f, soptN, &oSendCookies, SY_FALSE, SY_TRUE, SY_FALSE) != SY_OK) return SY_ERROR;
              break;
            case 'C':
              if (!GetStrOpt(prs, argc, argv, &f, soptN, &oCookieDump, SY_FALSE, SY_TRUE)) return SY_ERROR;
              break;
            case 'H':
              if (GetStrOptWithAt(prs, argc, argv, &f, soptN, &oAddonHeaders, SY_FALSE, SY_TRUE, SY_TRUE)) return SY_ERROR;
              break;
            case 'l':
              if (f >= argc) {
                fprintf(stderr, "E: \"-l\" without file name!\n");
                return SY_ERROR;
              }
              SyAddURLFromFile(argv[f++]);
              break;
            case 'h':
              fprintf(stderr, "%s\n", SYREN_VERSION_DATETIME_STRING);
              key = cfg->opts->first;
              if (f < argc && !strcmp(argv[f], "all")) {
                fprintf(stderr, "long options:\n");
                while (key) { fprintf(stderr, "  --%s\n", key->key); key = key->next; }
              } else {
                if (f < argc && argv[f]) {
                  while (key) {
                    if (!strcasecmp(key->key, argv[f])) {
                      fprintf(stderr, "--%s %s\n  %s\n", key->key,
                        key->uidata==SY_CI_STRING?"string":(key->uidata==SY_CI_INT?"integer":(key->uidata==SY_CI_BOOL?"boolean":"?")),
                        key->ustr);
                      break;
                    }
                    key = key->next;
                  }
                } else key = NULL;
                if (!key) ShowHelp();
              }
              return SY_ERROR;
            default:
              SyMessage(prs, SY_MSG_ERROR, "unknown option: %s", soptN);
              return SY_ERROR;
          } /* switch */
        } /* while */
      } /* if */
    } else {
      /* new URL */
      t = SyStrNew(s, -1);
      if (!t) { SyMessage(prs, SY_MSG_ERROR, "memory error"); return SY_ERROR; }
      SyStrTrim(t);
      if (*t == '@') SyAddURLFromFile(t+1);
      else if (*t) {
        if (t[0] == '+') { strcpy(t, &(t[1])); SyStrTrim(t); }
        if (*t) {
          SyKVListDelete(oURLList, t);
          SyKVListSet(oURLList, t, "", NULL);
        }
      }
      SyStrFree(t);
    }
  }
  if (oResume && oStateFileName) { SyMessage(prs, SY_MSG_ERROR, "'-r'/'-s' conflict"); return SY_ERROR; }

  return SY_OK;
}


#ifdef SYREN_USE_SCRIPT
TSyHdrs *scHdrs = NULL;
#endif

TSyResult SyDrvAddHeaders (TSyState *state, TSyHdrs *hdrs) {
  char *s = NULL, *t, *cs;
  TSyURL *url = state->url;
  char port[32];

  // fuck you, sourceshit
  if (strstr(url->host, "sourceforge.net")) {
    if (SyHdrAddLine(hdrs, "User-Agent: HereToPissOffSourceCrapSniffer") != SY_OK) { SyMessage(state->pfn, SY_MSG_ERROR, "hdr error"); return SY_ERROR; }
  }

  if (cfgRefererType) {
    switch (cfgRefererType) {
      case 1: s = SyStrNew(oURL, -1); break;
      case 2:
        if (url->proto == SY_PROTO_FTP && url->port != 80) sprintf(port, ":%i", url->port); else port[0] = '\0';
        s = SySPrintf("%s://%s%s%s%s%s%s", url->protostr, url->host, port,
          url->dir, url->file, url->query, url->anchor);
        break;
      default: s = SyStrNew(cfgRefererStr, -1); break;
    }
    if (!s) { SyMessage(state->pfn, SY_MSG_ERROR, "memory error"); return SY_ERROR; }
    t = SySPrintf("Referer: %s", s); free(s);
    if (!t) { SyMessage(state->pfn, SY_MSG_ERROR, "memory error"); return SY_ERROR; }
    if (SyHdrAddLine(hdrs, t) != SY_OK) { free(t); SyMessage(state->pfn, SY_MSG_ERROR, "hdr error"); return SY_ERROR; }
    free(t);
  }
  if (oAddonHeaders && *oAddonHeaders) {
    s = oAddonHeaders;
    while (*s) {
      t = s; while (*t && *t != '\n') t++;
      cs = SyStrNew(s, t-s); if (!cs) return SY_ERROR;
      SyStrTrim(cs);
      if (*cs) {
        if (SyHdrAddLine(hdrs, cs) != SY_OK) { free(cs); SyMessage(state->pfn, SY_MSG_ERROR, "hdr error"); return SY_ERROR; }
      }
      free(cs); if (!s[0]) break;
      s = t; if (*s) s++; /* don't skip latest 0 */
    }
  }
  if (oSendCookies && *oSendCookies) {
    char *newstr = calloc(strlen(oSendCookies)*3, 1); /* this should be enough */
    s = oSendCookies;
    while (*s) {
      t = s; while (*t && *t != '\n') t++;
      cs = SyStrNew(s, t-s); if (!cs) return SY_ERROR;
      SyStrTrim(cs);
      if (*cs) {
        if (newstr[0]) strcat(newstr, "; ");
        //if (SyHdrSetCookie(hdrs, cs) != SY_OK) { free(cs); return SY_ERROR; }
        strcat(newstr, cs);
      }
      free(cs); if (!s[0]) break;
      s = t; if (*s) s++; /* don't skip latest 0 */
    }
    if (newstr[0]) {
      SyHdrSetCookie(hdrs, newstr);
      SyHdrAddLine(hdrs, "Cookie2: $Version=1");
    }
    free(newstr);
  }
#ifdef SYREN_USE_SCRIPT
  scHdrs = hdrs;
  SyScriptCallback("Event.AddHeaders", state->pfn);
  scHdrs = NULL;
  if (scAbort == SY_TRUE) state->breakNow = SY_TRUE;
#endif

  return SY_OK;
}


TSyResult SyDrvGotHeaders (TSyState *state, TSyHdrs *hdrs) {
  TSyResult res = SY_OK;
  TSyKVList *cc; TSyKVListItem *item;
  int fd;
  static char *crlf = "\r\n";

#ifdef SYREN_USE_SCRIPT
  scHdrs = hdrs;
  SyScriptCallback("Event.GotHeaders", state->pfn);
  /*fprintf(stderr, "\rGotHeaders: return\n");*/
  scHdrs = NULL;
  if (scAbort == SY_TRUE) state->breakNow = SY_TRUE;
#endif
  if (oCookieDump && *oCookieDump) {
    cc = SyHdrFindCookieList(hdrs);
    if (cc && cc->count) {
      if (strcmp(oCookieDump, "-")) {
        fd = SyOpenFile(oCookieDump, SY_FMODE_WRITE, SY_TRUE);
        if (fd < 0) { SyMessage(state->pfn, SY_MSG_ERROR, "can't dump cookies to '%s'", oCookieDump); return SY_ERROR; }
      } else fd = 0;
      item = cc->first;
      while (item) {
        if (SyWriteFile(fd, item->value, strlen(item->value)) != SY_OK) { res = SY_ERROR; break; }
        if (SyWriteFile(fd, crlf, strlen(crlf)) != SY_OK) { res = SY_ERROR; break; }
        item = item->next;
      }
      if (fd) SyCloseFile(fd);
    }
  }

  return res;
}


static TSyState *curState = NULL;

static void BreakSignal (int signo) {
  lastSSave = 0;
  if (curState) curState->breakNow = SY_TRUE;
  ctrlCPressed = SY_TRUE;
}


static void SaveStateSignal (int signo) {
  /*fprintf(stderr, "\nsave state\n");*/
  lastSSave = 0;
}


TSyResult SyXPrealloc (int fd, int64_t fileSize) {
#if !USE_NEW_PREALLOC
  char tmp0[256];
  char *buf;
  int bufSz, wr;
  double sttime = SyGetTimeD(), ctime;
  int wasoutput = 0;

  if (fileSize < 1) return SY_ERROR;

  bufSz = 256*1024;
  if (bufSz > fileSize) bufSz = fileSize;
  buf = calloc(1, bufSz);
  if (!buf) return SY_ERROR;

  while (fileSize > 0) {
    if (cfgQuiet != SY_TRUE || cfgNoOutput != SY_TRUE) {
      ctime = SyGetTimeD()-sttime;
      if (ctime >= 0.666) {
        SyLong2StrComma(tmp0, fileSize);
        fprintf(stderr, "\rN: preallocating: [%s] bytes left\x1b[K", tmp0);
        fflush(stderr);
        wasoutput = 1;
        sttime = SyGetTimeD();
      }
    }
    wr = fileSize>(int64_t)bufSz?bufSz:fileSize;
    if (SyWriteFile(fd, buf, wr) != SY_OK) {
      free(buf);
      if (isatty(fileno(stderr))) {
        if (wasoutput) { fprintf(stderr, "\r\x1b[K"); fflush(stderr); }
      }
      return SY_ERROR;
    }
    fileSize -= (int64_t)wr;
    if (ctrlCPressed == SY_TRUE) {
      free(buf);
      if (isatty(fileno(stderr))) {
        if (wasoutput) { fprintf(stderr, "\r\x1b[Kaborted!\n"); fflush(stderr); }
      }
      return SY_ERROR;
    }
  }
  free(buf);
  if (isatty(fileno(stderr))) {
    if (wasoutput) { fprintf(stderr, "\r\x1b[K"); fflush(stderr); }
  }
#else
  if (fileSize > 0) {
# ifdef __linux
    /*if (fallocate(fd, FALLOC_FL_KEEP_SIZE, 0, fileSize)) return SY_ERROR;*/
    /*if (fallocate(fd, 0, 0, fileSize)) return SY_ERROR;*/
    if (posix_fallocate(fd, 0, fileSize)) return SY_ERROR;
# else
    if (posix_fallocate(fd, 0, fileSize)) return SY_ERROR;
# endif
  }
#endif

  return SySeekFile(fd, 0);
}


TSyResult SyDrvOpenFile (TSyState *state) {
  int fd, sto = 0; int64_t len = -1;
  char *s, tmp[32];

  if (replyOnly != SY_FALSE) return SY_OK;

  if (oCanRename) {
    if (!oOutFileName || !oOutFileName[0]) {
      if (oOutFileName) free(oOutFileName);
      if (state->httpFName && *state->httpFName) {
        char *t;
        //
        if ((t = strrchr(state->httpFName, '/')) != NULL) {
          oOutFileName = SyStrDup(t[1]?t+1:"unnamed_file.bin");
        } else {
          oOutFileName = SyStrDup(state->httpFName);
        }
      } else {
        oOutFileName = SyStrDup(state->url->file);
      }
#ifdef SYREN_USE_SCRIPT
      SyScriptCallback("Event.CheckFileName", state->pfn);
#endif
      /*if (scAbort == SY_TRUE) goto done;*/
      if (!oOutFileName || !oOutFileName[0]) {
        if (oOutFileName) free(oOutFileName);
        oOutFileName = SyStrNew((cfgDefaultName&&*cfgDefaultName)?cfgDefaultName:"index.html", -1);
      }
      if (!oOutFileName) { SyMessage(state->pfn, SY_MSG_ERROR, "memory error"); return SY_ERROR; }
    }
    /* check for duplicates */
    fd = SyOpenFile(oOutFileName, SY_FMODE_READ, SY_FALSE);
    if (fd >= 0) {
      int f = -1; s = NULL;
      do { f++;
        SyCloseFile(fd); if (s) free(s);
        s = SySPrintf("%s.%i", oOutFileName, f);
        if (!s) { SyMessage(state->pfn, SY_MSG_ERROR, "memory error"); return SY_ERROR; }
        fd = SyOpenFile(s, SY_FMODE_READ, SY_FALSE);
      } while (fd >= 0);
      free(oOutFileName); oOutFileName = s;
    }
  }
  /* open/create file */
  if (!oOutFileName || !oOutFileName[0]) { SyMessage(state->pfn, SY_MSG_ERROR, "invalid file name"); return SY_ERROR; }
  if (state->firstByte > 0) {
    if (!strcmp(oOutFileName, "-")) { SyMessage(state->pfn, SY_MSG_ERROR, "can't resume stdout"); return SY_ERROR; }
    fd = SyOpenFile(oOutFileName, SY_FMODE_WRITE, SY_FALSE);
    if (fd >= 0) {
      len = SyFileSize(fd);
      if (len < 0) { SyCloseFile(fd); SyMessage(state->pfn, SY_MSG_ERROR, "can't resume: file error"); return SY_ERROR; }
      if (len < state->firstByte) { SyCloseFile(fd); SyMessage(state->pfn, SY_MSG_ERROR, "can't resume: corrupted file"); return SY_ERROR; }
      if (SySeekFile(fd, state->firstByte) != SY_OK) {
        SyCloseFile(fd);
        SyMessage(state->pfn, SY_MSG_ERROR, "can't resume: file error");
        return SY_ERROR;
      }
      SyLong2StrComma(tmp, state->firstByte);
      SyMessage(state->pfn, SY_MSG_MSG, "resuming %s at %s", oOutFileName, tmp);
    }
  } else {
    if (!strcmp(oOutFileName, "-")) { fd = 0; sto = 1; oNoStateFile = SY_TRUE; }
    else {
      SyDeleteFile(oOutFileName);
      fd = SyOpenFile(oOutFileName, SY_FMODE_WRITE, SY_TRUE);
      /* preallocating w/o state file means no resumes, so disable it */
      if (fd > 0 && oPreallocSpace != SY_FALSE && state->fileSize > 0 && oNoStateFile != SY_TRUE) {
        SyLong2StrComma(tmp, state->fileSize);
        SyMessage(state->pfn, SY_MSG_MSG, "preallocating %s bytes", tmp);
        if (SyXPrealloc(fd, state->fileSize) != SY_OK) {
          SyMessage(state->pfn, SY_MSG_ERROR, "preallocating failed");
          SyCloseFile(fd);
          SyDeleteFile(oOutFileName);
          return SY_ERROR;
        }
      }
    }
  }
  if (fd < 0) { SyMessage(state->pfn, SY_MSG_ERROR, "can't open file %s", oOutFileName); return SY_ERROR; }
  if (state->firstByte <= 0 || sto) SyMessage(state->pfn, SY_MSG_MSG, "writing to %s", sto?"stdout":oOutFileName);
  state->udatai = sto?-2:fd;
  if (!oStateFileName || !oStateFileName[0]) {
    if (oStateFileName) free(oStateFileName);
    oStateFileName = SySPrintf("%s.syren", oOutFileName);
    if (!oStateFileName) SyMessage(state->pfn, SY_MSG_WARNING, "can't create state file name");
    else if (oNoStateFile != SY_TRUE) SySaveState(oStateFileName, state);
  }

  return SY_OK;
}


TSyResult SyDrvCloseFile (TSyState *state) {
  if (replyOnly != SY_FALSE) return SY_OK;
  if (state->udatai >= 0) {
    SyMessage(state->pfn, SY_MSG_MSG, "closing %s%s", oOutFileName,
      (state->error==SY_FALSE?"":" (incomplete file)"));
    SyCloseFile(state->udatai);
    state->udatai = -1;
    if (oStateFileName && *oStateFileName && oNoStateFile != SY_TRUE) SySaveState(oStateFileName, state);
  }

  return SY_OK;
}


TSyResult SyDrvWriteFile (TSyState *state, void *buf, int bufSize) {
  int fd;

  if (replyOnly != SY_FALSE) return SY_OK;
  fd = state->udatai; if (fd == -2) fd = 1; /* stdout */
  if (fd >= 0) {
    if (SyWriteFile(fd, buf, bufSize) != SY_OK) return SY_ERROR;
  }
  if (oStateFileName && *oStateFileName && oNoStateFile != SY_TRUE && state->udatai >= 0) {
    if (cfgSaveInterval >= 1.0 && SyGetTimeD()-lastSSave >= cfgSaveInterval) {
      SySaveState(oStateFileName, state);
      lastSSave = SyGetTimeD();
      fsync(state->udatai);
    }
  }

  return SY_OK;
}


TSyResult SyDrvNoResume (TSyState *state) {
  if (replyOnly != SY_FALSE) {
    SyMessage(state->pfn, SY_MSG_WARNING, "can't resume");
    return SY_OK;
  }
  if (state->udatai >= 0) {
    if (state->udatai) SyCloseFile(state->udatai);
    SyDeleteFile(oOutFileName);
    state->udatai = SyOpenFile(oOutFileName, SY_FMODE_WRITE, SY_TRUE);
    if (state->udatai < 0) {
      SyMessage(state->pfn, SY_MSG_WARNING, "can't create file %s", oOutFileName);
      return SY_ERROR;
    }
  }
  SyMessage(state->pfn, SY_MSG_WARNING, "can't resume file %s, file truncated", oOutFileName);

  return SY_OK;
}


TSyResult CheckResume (TSyState *state) {
  int fd; int64_t len;

  SyMessage(state->pfn, SY_MSG_NOTICE, "checking file for resuming");
  if (!strcmp(oOutFileName, "-")) { SyMessage(state->pfn, SY_MSG_ERROR, "can't resume stdout"); return SY_ERROR; }
  fd = SyOpenFile(oOutFileName, SY_FMODE_WRITE, SY_FALSE);
  if (fd < 0) { SyMessage(state->pfn, SY_MSG_ERROR, "resume: can't open file %s", oOutFileName); return SY_ERROR; }
  len = SyFileSize(fd);
  SyCloseFile(fd);
  if (len < 0) { SyMessage(state->pfn, SY_MSG_ERROR, "can't determine file size for %s", oOutFileName); return SY_ERROR; }
  state->firstByte = len;
  SyMessage(state->pfn, SY_MSG_NOTICE, "resume check passed");

  return SY_OK;
}


static TSyCfg *curcfg = NULL;
static char *cfgSaveBuf = NULL;
static size_t cfgSaveBufSize = 0;
static TSyPrintStr prs;


static void loadDefaultConfig (void) {
  const char *home;
  char *s = NULL;
  //
  SyCfgClear(curcfg);
  SyDefaultCfg(curcfg);
  if ((home = getenv("HOME")) != NULL) {
    s = SySPrintf("%s/.syren/.syrenrc", home);
    if (access(s, R_OK) == 0) {
      SyCfgLoad(curcfg, s, &prs);
      goto quit;
    }
    free(s);
    s = SySPrintf("%s/.syrenrc", home);
    if (access(s, R_OK) == 0) {
      SyCfgLoad(curcfg, s, &prs);
      goto quit;
    }
  }
  if (access(".syrenrc", R_OK) == 0) SyCfgLoad(curcfg, ".syrenrc", &prs);
  else if (access("/etc/syren/.syrenrc", R_OK) == 0) SyCfgLoad(curcfg, "/etc/syren/.syrenrc", &prs);
quit:
  if (s != NULL) free(s);
}


static void storeConfig (void) {
  FILE *fl = open_memstream(&cfgSaveBuf, &cfgSaveBufSize);
  SyCfgSaveFile(curcfg, fl, &prs);
  fclose(fl);
}


static void restoreConfig (void) {
  if (cfgSaveBuf != NULL) {
    FILE *fl;
    //
    fl = fmemopen(cfgSaveBuf, cfgSaveBufSize, "r");
    SyCfgLoadFile(curcfg, fl, &prs);
    fclose(fl);
  }
}


static char *getPerSiteConfigFileName (const char *host) {
  const char *t = getenv("HOME");
  //
  while (*host) {
    char *cfgname = SySPrintf("%s/%s/%s.rc", (t != NULL ? t : "/etc"), (t != NULL ? ".syren" : "syren"), host), *dot;
    //
    if (access(cfgname, R_OK) == 0) return cfgname;
    free(cfgname);
    if ((dot = strchr(host, '.')) == NULL) break;
    host = dot+1;
  }
  //
  return NULL;
}


static void loadPerSiteConfig (void) {
  TSyURL *url = SyURLNew();
  //
  if (SyURLParse(url, oURL) == SY_OK) {
    char *cfgname = getPerSiteConfigFileName(url->host);
    //
    if (cfgname != NULL) {
      SyCfgLoad(curcfg, cfgname, &prs);
      free(cfgname);
    }
  }
  //
  SyURLFree(url);
}


int main (int argc, char *argv[]) {
  int mainres = 1;
  char *s, *t;
  TSyResult res;
  int retCnt;
  int userBreak = 0;

  prs.print = &SyPrintStr; prs.udata = NULL;

  t = getenv("TERM");
  if (t && (!strcmp(t, "rxvt") || !strcmp(t, "mrxvt") || !strcmp(t, "xterm") || !strcmp(t, "eterm"))) doRXVT = SY_TRUE;

  curcfg = SyCfgNew();
  if (!curcfg) { SyMessage(&prs, SY_MSG_ERROR, "memory error"); }

  state = SyNew();
  if (!state) { SyMessage(&prs, SY_MSG_ERROR, "memory error"); }
  prs.udata = (void *)state;

  oURLList = SyKVListNew();
  if (!oURLList) goto done;
  oURLList->casesens = 1;

  loadDefaultConfig();

  if (ParseCmdLine(&prs, curcfg, argc, argv) != SY_OK) goto done;

  SyMessage(&prs, SY_MSG_MSG, "%s", SYREN_VERSION_DATETIME_STRING);

#ifdef SYREN_USE_SCRIPT
  SyScriptInit(&prs);
  SyScriptLoadInit(argv[0], &prs);
#endif

  if (!oStateFileName) {
    if (!oURLList->first) { SyMessage(&prs, SY_MSG_ERROR, "URL?"); goto done; }
  }

  if (cfgMaxBufferSize < 1) { SyMessage(&prs, SY_MSG_ERROR, "buffer size too small"); goto done; }
  if (cfgTimeout < 1) { SyMessage(&prs, SY_MSG_ERROR, "timeout too small"); goto done; }

  if (writeCfg == SY_TRUE) {
    SyMessage(&prs, SY_MSG_MSG, "generating sample config");
    SyCfgSave(curcfg, ".syrenrc", &prs);
    mainres = 0;
    goto done;
  }

  if (SySocketInit() != SY_OK) goto done;

  storeConfig();

  state->udatai = -1;

  state->ftpUseConnect = cfgFTPUseConnect;

  state->pfn = &prs;
  state->fnprog = PrintProgress;
  state->fnprephdrs = SyDrvAddHeaders;
  state->fngothdrs = SyDrvGotHeaders;

  state->fnopen = SyDrvOpenFile;
  state->fnclose = SyDrvCloseFile;
  state->fnwrite = SyDrvWriteFile;
  state->fnnoresume = SyDrvNoResume;

  state->postData = oPostData; oPostData = NULL;

do {
  static int wosDo = 0;
  res = SY_ERROR;
  userBreak = 0;

  restoreConfig();

  if (!oStateFileName) {
    if (!oURLList->first) break;
    oURL = SyStrDup(oURLList->first->key);
    SyKVListDelete(oURLList, oURLList->first->key);
    if (!oURL) {
      SyMessage(&prs, SY_MSG_ERROR, "memory error");
      goto done;
    }
    /*if (!oURL) { SyMessage(&prs, SY_MSG_ERROR, "URL?"); goto done; }*/
  } else {
    SyKVListClear(oURLList);
    SyMessage(&prs, SY_MSG_MSG, "restoring state from file %s", oStateFileName);
    if (oStateFileName && *oStateFileName) {
      s = SyLoadState(oStateFileName, state);
      if (!s || !s[0]) {
        if (s) free(s);
        SyMessage(&prs, SY_MSG_ERROR, "can't restore state");
        goto done;
      }
      if (oOutFileName) free(oOutFileName);
      oOutFileName = s;
    }
    if (dumpState == SY_TRUE) { DumpState(state); goto done; }
  }

  loadPerSiteConfig();

  if (replyOnly == SY_FALSE && oResume == SY_TRUE && CheckResume(state) != SY_OK) goto done;

  //if (strcasestr(oURL, "worldofspectrum.org")) optWOS = 15; // hehe
  if (wosDo && optWOS > 0) { SyMessage(&prs, SY_MSG_MSG, "WOS waiting: %d", optWOS); sleep(optWOS); }
  wosDo = 1;

#ifdef SYREN_USE_SCRIPT
/*
  if (!(state->url = SyURLNew())) {
    SyMessage(&prs, SY_MSG_ERROR, "memory error!");
    goto done;
  }
  if (SyURLParse(state->url, oURL) != SY_OK) {
    SyMessage(state->pfn, SY_MSG_ERROR, "invalid URL");
    return SY_ERROR;
  }
  SyScriptCallback("Event.BeforePrepare", &prs);
  s = SyURL2StrEx(state->url, SY_TRUE, SY_FALSE);
  if (!s) {
    SyMessage(&prs, SY_MSG_ERROR, "memory error!");
    goto done;
  }
  SyStrFree(oURL); oURL = s; s = NULL;
  SyURLFree(state->url); state->url = NULL;
  fprintf(stderr, "URL: <%s>\n", oURL);
*/
#endif
  if (SyPrepare(state, oURL,
    cfgUseProxyHTTP?cfgProxyHTTP:NULL,
    cfgUseProxyFTP?cfgProxyFTP:NULL, cfgIFace) != SY_OK) { SyMessage(&prs, SY_MSG_ERROR, "SyPrepare() failed"); goto done; }
#ifdef SYREN_USE_SCRIPT
  SyScriptCallback("Event.AfterPrepare", &prs);
  if (scAbort == SY_TRUE) goto done;
#endif

  if (uEncodeDecode < 0 ||
     (cfgDecodeFTPURL == SY_TRUE && state->url->proto == SY_PROTO_FTP)) {
    s = SyURLDecode(state->url->dir);
    if (!s) { SyMessage(&prs, SY_MSG_ERROR, "SyURLDecode() failed"); goto done; }
    SyStrFree(state->url->dir); state->url->dir = s;
    s = SyURLDecode(state->url->file);
    if (!s) { SyMessage(&prs, SY_MSG_ERROR, "SyURLDecode() failed"); goto done; }
    SyStrFree(state->url->file); state->url->file = s;
  } else if (uEncodeDecode > 0) {
    s = SyURLEncode(state->url->dir);
    if (!s) { SyMessage(&prs, SY_MSG_ERROR, "SyURLEncode() failed"); goto done; }
    SyStrFree(state->url->dir); state->url->dir = s;
    s = SyURLEncode(state->url->file);
    if (!s) { SyMessage(&prs, SY_MSG_ERROR, "SyURLEncode() failed"); goto done; }
    SyStrFree(state->url->file); state->url->file = s;
  }

  signal(SIGHUP, SaveStateSignal);
  signal(SIGUSR1, SaveStateSignal);

  signal(SIGINT, BreakSignal);
  signal(SIGQUIT, BreakSignal);
  signal(SIGTERM, BreakSignal);

  retCnt = cfgReconnectAttempts;

  state->initBufferSize = cfgBufferSize;
  state->maxBufferSize = cfgMaxBufferSize;
  state->ioTimeout = cfgTimeout;
  state->maxRedirects = cfgMaxRedirects;
  if (cfgUAStr && *cfgUAStr) {
    SyStrFree(state->userAgent);
    state->userAgent = SyStrNew(cfgUAStr, -1);
  }
  while (1) {
    ctrlCPressed = SY_FALSE;
    curState = state;
    state->allowOnly2XX = optAnyCode=SY_TRUE ? SY_FALSE : SY_TRUE;
    res = SyBegin(state);
    if (replyOnly != SY_FALSE) break;
    if (res == SY_OK) res = SyRun(state);
    curState = NULL;
    if (res == SY_OK) break;
    if (state->interrupted == SY_TRUE) {
      userBreak = 1;
      if (oStateFileName && *oStateFileName && state->status == SY_STATUS_DOWNLOADING && oNoStateFile != SY_TRUE) {
        SyMessage(&prs, SY_MSG_WARNING, "user break, saving state");
        SySaveState(oStateFileName, state);
      } else SyMessage(&prs, SY_MSG_WARNING, "user break");
      break;
    }
    if (retCnt < 1) break;
    if (state->status != SY_STATUS_DOWNLOADING && oForceRetry != SY_TRUE) break;
    SyMessage(&prs, SY_MSG_ERROR, "download failed");
    if (cfgReconnectDelay > 0) {
      SyMessage(&prs, SY_MSG_MSG, "waiting %i seconds", cfgReconnectDelay);
      //SySleep(cfgReconnectDelay);
      DoWait(cfgReconnectDelay);
      if (ctrlCPressed != SY_FALSE) {
        SyMessage(&prs, SY_MSG_WARNING, "user break");
        res = SY_ERROR;
        break;
      }
    }
    SyReset(state);
    SyMessage(&prs, SY_MSG_MSG, "restarting");
    state->firstByte += state->currentByte; /* start from here */
    oCanRename = SY_FALSE;
    retCnt--;
  }
#ifdef SYREN_USE_SCRIPT
  if (res == SY_OK) SyScriptCallback("Event.DownloadComplete", &prs);
  else SyScriptCallback("Event.DownloadFailed", &prs);
  if (scAbort == SY_TRUE) goto done;
#endif
  SyEnd(state);
  if (res == SY_OK) {
    SyMessage(&prs, SY_MSG_MSG, "download complete");
    if (oCanRemoveState && oStateFileName && *oStateFileName) {
      if (SyDeleteFile(oStateFileName) == SY_OK) SyMessage(&prs, SY_MSG_NOTICE, "state file removed");
    }
  } else SyMessage(&prs, SY_MSG_ERROR, "download failed");

  mainres = (res==SY_OK)?0:1;

done:
  SyStrFree(oURL); oURL = NULL;
  SyStrFree(oOutFileName); oOutFileName = NULL;
  SyStrFree(oStateFileName); oStateFileName = NULL;
  oCanRename = SY_TRUE;
} while (!userBreak);

  if (cfgSaveBuf != NULL) free(cfgSaveBuf);
  SyFree(state);
  SyCfgFree(curcfg);
  if (oURLList) SyKVListFree(oURLList);
  if (cfgDefaultName) SyStrFree(cfgDefaultName);
  if (cfgIFace) SyStrFree(cfgIFace);
  if (cfgProxyHTTP) SyStrFree(cfgProxyHTTP);
  if (cfgProxyFTP) SyStrFree(cfgProxyFTP);
  if (cfgRefererStr) SyStrFree(cfgRefererStr);
  if (cfgUAStr) SyStrFree(cfgUAStr);
  SyStrFree(oURL);
  SyStrFree(oOutFileName);
  SyStrFree(oStateFileName);
  if (oPostData) SyStrFree(oPostData);
  if (oSendCookies) SyStrFree(oSendCookies);
  if (oCookieDump) SyStrFree(oCookieDump);
  if (oAddonHeaders) SyStrFree(oAddonHeaders);

#ifdef SYREN_USE_SCRIPT
  SyScriptDeinit();
#endif

  SySocketShutdown();
  return mainres;
}
